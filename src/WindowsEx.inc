(*******************************************************************************
  功  能: 扩展 Windows 单元，为早期 delphi 版本补充相应数据类型。
  作  者: JXQ
  日  期: 2018.12.15
  用  法: 在 interface 部分 uses 区之后加入 {$I WindowsEx.inc} 指令。
          需要先引用 Windows 单元。
*******************************************************************************)

{$IFDEF VER180 | VER185}  //delphi 2007
type
  PLPCTSTR = PPAnsiChar;
  PLPTSTR = PPAnsiChar;

  LPBYTE = PByte;
  LPVOID = Pointer;
  LPCVOID = Pointer;
  ULONG32 = LongWord;
  LONG = Longint;
  PVOID = Pointer;
  PPVOID = ^PVOID;
  LONG64 = Int64;
  ULONG64 = UInt64;
  DWORD64 = UInt64;
  PLONG64 = ^LONG64;
  PULONG64 = ^ULONG64;
  PDWORD64 = ^DWORD64;

  USHORT = Word;
  PSHORT = PSmallint;
  PUSHORT = PWord;

  HANDLE_PTR = type NativeUInt;
  SIZE_T = ULONG_PTR;
  SSIZE_T = LONG_PTR;

  PINT_PTR = ^INT_PTR;
  PUINT_PTR = ^UINT_PTR;
  PLONG_PTR = ^LONG_PTR;
  PULONG_PTR = ^ULONG_PTR;
  PDWORD_PTR = ^DWORD_PTR;
  PSIZE_T = ^SIZE_T;
  PSSIZE_T = ^SSIZE_T;

  DWORDLONG = UInt64;
  PULONGLONG = ^UInt64;
{$ENDIF}


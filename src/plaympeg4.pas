(*******************************************************************************
  名  称: 海康威视播放库 SDK
  版  本: V7.3.6.60_Build20180831_win32
  原文件: plaympeg4.h
  改  写: JXQ
  日  期: 2018.12.24
  方  式: 静态调用
*******************************************************************************)

unit plaympeg4;

interface

uses
  Windows;

{$I WindowsEx.inc}      //Windows类型扩展

{======== 常量定义 ============================================================}

const
{-------- 播放库错误码 --------------------------------------------------------}
  PLAYM4_NOERROR = 0;                   //没有错误
  PLAYM4_PARA_OVER = 1;                 //输入参数非法
  PLAYM4_ORDER_ERROR = 2;               //调用顺序不对
  PLAYM4_TIMER_ERROR = 3;               //多媒体时钟设置失败
  PLAYM4_DEC_VIDEO_ERROR = 4;           //视频解码失败
  PLAYM4_DEC_AUDIO_ERROR = 5;           //音频解码失败
  PLAYM4_ALLOC_MEMORY_ERROR = 6;        //分配内存失败
  PLAYM4_OPEN_FILE_ERROR = 7;           //文件操作失败
  PLAYM4_CREATE_OBJ_ERROR = 8;          //创建线程事件等失败
  PLAYM4_CREATE_DDRAW_ERROR = 9;        //创建DierctDraw失败
  PLAYM4_CREATE_OFFSCREEN_ERROR = 10;   //创建后端缓存失败
  PLAYM4_BUF_OVER = 11;                 //缓冲区满，输入流失败
  PLAYM4_CREATE_SOUND_ERROR = 12;       //创建音频设备失败
  PLAYM4_SET_VOLUME_ERROR = 13;         //设置音量失败
  PLAYM4_SUPPORT_FILE_ONLY = 14;        //只能在播放文件时才能使用此接口
  PLAYM4_SUPPORT_STREAM_ONLY = 15;      //只能在播放流时才能使用此接口
  PLAYM4_SYS_NOT_SUPPORT = 16;          //不支持
  PLAYM4_FILEHEADER_UNKNOWN = 17;       //没有文件头
  PLAYM4_VERSION_INCORRECT = 18;        //解码器和编码器版本不对应
  PALYM4_INIT_DECODER_ERROR = 19;       //初始化解码器失败
  PLAYM4_CHECK_FILE_ERROR = 20;         //文件太短或码流无法识别
  PLAYM4_INIT_TIMER_ERROR = 21;         //初始化多媒体时钟失败
  PLAYM4_BLT_ERROR = 22;                //位拷贝失败
  PLAYM4_UPDATE_ERROR = 23;             //显示overlay失败
  PLAYM4_OPEN_FILE_ERROR_MULTI = 24;    //打开文件失败，流类型是复合流
  PLAYM4_OPEN_FILE_ERROR_VIDEO = 25;    //打开文件失败，流类型是视频流
  PLAYM4_JPEG_COMPRESS_ERROR = 26;      //JPEG压缩失败
  PLAYM4_EXTRACT_NOT_SUPPORT = 27;      //文件不支持
  PLAYM4_EXTRACT_DATA_ERROR = 28;       //数据错误
  PLAYM4_SECRET_KEY_ERROR = 29;         //解码密钥错误
  PLAYM4_DECODE_KEYFRAME_ERROR = 30;    //解码关键帧失败
  PLAYM4_NEED_MORE_DATA = 31;           //数据不足
  PLAYM4_INVALID_PORT = 32;             //无效端口号
  PLAYM4_NOT_FIND = 33;                 //查找失败
  PLAYM4_NEED_LARGER_BUFFER = 34;       //需要更大的缓冲区
  PLAYM4_FAIL_UNKNOWN = 99;             //未知的错误
{-------- 鱼眼部分错误码 ------------------------------------------------------}
  PLAYM4_FEC_ERR_ENABLEFAIL = 100;          //鱼眼模块加载失败
  PLAYM4_FEC_ERR_NOTENABLE = 101;           //鱼眼模块没有加载
  PLAYM4_FEC_ERR_NOSUBPORT = 102;           //子端口没有分配
  PLAYM4_FEC_ERR_PARAMNOTINIT = 103;        //没有初始化对应端口的参数
  PLAYM4_FEC_ERR_SUBPORTOVER = 104;         //子端口已经用完
  PLAYM4_FEC_ERR_EFFECTNOTSUPPORT = 105;    //该安装方式下这种效果不支持
  PLAYM4_FEC_ERR_INVALIDWND = 106;          //非法的窗口
  PLAYM4_FEC_ERR_PTZOVERFLOW = 107;         //PTZ位置越界
  PLAYM4_FEC_ERR_RADIUSINVALID = 108;       //圆心参数非法
  PLAYM4_FEC_ERR_UPDATENOTSUPPORT = 109;    //指定的安装方式和矫正效果，该参数更新不支持
  PLAYM4_FEC_ERR_NOPLAYPORT = 110;          //播放库端口没有启用
  PLAYM4_FEC_ERR_PARAMVALID = 111;          //参数为空
  PLAYM4_FEC_ERR_INVALIDPORT = 112;         //非法子窗口
  PLAYM4_FEC_ERR_PTZZOOMOVER = 113;         //PTZ矫正范围越界
  PLAYM4_FEC_ERR_OVERMAXPORT = 114;         //矫正通道饱和，最大支持的矫正通道为四个
  PLAYM4_FEC_ERR_ENABLED = 115;             //该端口已经启用了鱼眼模块
  PLAYM4_FEC_ERR_D3DACCENOTENABLE = 116;    //D3D加速没有开启
  PLAYM4_FEC_ERR_PLACETYPE = 117;           //安装方式不正确；一个播放库port需要对应一种安装方式
  PLAYM4_FEC_ERR_CorrectType = 118;         //矫正方式错误：如矫正方式已有，则不能开多个；比如一个播放库port设定除了PTZ和鱼眼半球矫正方式外，其他的矫正方式则只能开一路；设置180度矫正不能和PTZ矫正仪器开，半球矫正与其他矫正则无关联性。
  PLAYM4_FEC_ERR_NULLWND = 119;             //鱼眼窗口为NULL

{-------- 流播放模式 ----------------------------------------------------------}
  STREAME_REALTIME = 0;   //此模式（默认）下, 会尽量保正实时性, 防止数据阻塞; 而且数据检查严格
  STREAME_FILE = 1;       //此模式下按时间戳播放
{-------- 缓冲数据流大小 ------------------------------------------------------}
  SOURCE_BUF_MIN = 1024 * 50;       //缓冲数据流缓冲最小值
  SOURCE_BUF_MAX = 1024 * 100000;   //缓冲数据流缓冲最大值

{======== 结构体定义 ==========================================================}

type

  {-------- 交互信息结构 --------}
  PLAYM4_SESSION_INFO = packed record
    nSessionInfoType: Integer;
    nSessionInfoLen: Integer;
    pSessionInfoData: Pointer;
  end;

/// <summary>索引回调函数</summary>
/// <param name="nPort" IO="Out">播放器通道号</param>
/// <param name="nUser" IO="Out">用户数据</param>
  FileRefDoneCB = procedure(nPort: DWORD; nUser: DWORD); stdcall;

{======== 接口函数 ============================================================}

{-------- 错误码 --------------------------------------------------------------}

/// <summary>获取错误号</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>错误码</returns>
function PlayM4_GetLastError(nPort: LONG): DWORD; stdcall; external 'PlayCtrl.dll';

{-------- 端口号 --------------------------------------------------------------}

/// <summary>获取未使用的通道号</summary>
/// <param name="nPort" IO="Out">播放通道号，指向用于获取端口号的LONG型变量指针</param>
/// <returns>True成功，False失败</returns>
function PlayM4_GetPort(nPort: PLongint): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>释放已使用的通道号</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_FreePort(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

{-------- 文件操作 ------------------------------------------------------------}

/// <summary>打开文件</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="sFileName" IO="In">文件名</param>
/// <returns>True成功，False失败</returns>
function PlayM4_OpenFile(nPort: LONG; sFileName: LPSTR): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>关闭文件</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_CloseFile(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

{-------- 流操作 --------------------------------------------------------------}

/// <summary>设置流播放模式</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="nMode" IO="In">流播放模式</param>
/// <returns>True成功，False失败</returns>
/// <remarks>必须在播放之前设置</remarks>
function PlayM4_SetStreamOpenMode(nPort: LONG; nMode: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>获取流播放模式</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>STREAME_REALTIME或STREAME_FILE</returns>
function PlayM4_GetStreamOpenMode(nPort: LONG): LONG; stdcall; external 'PlayCtrl.dll';

/// <summary>打开流</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="pFileHeadBuf" IO="In">文件头数据</param>
/// <param name="nSize" IO="In">文件头长度</param>
/// <param name="nBufPoolSize" IO="In">设置播放器中存放数据流的缓冲区大小。范围是SOURCE_BUF_MIN~ SOURCE_BUF_MAX</param>
/// <returns>True成功，False失败</returns>
/// <remarks>流开关接口和文件开关接口不能交叉使用</remarks>
function PlayM4_OpenStream(nPort: LONG; pFileHeadBuf: PByte; nSize, nBufPoolSize: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>关闭流</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_CloseStream(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>输入流数据</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="pBuf" IO="In">流数据缓冲区地址</param>
/// <param name="nSize" IO="In">流数据缓冲区大小</param>
/// <returns>True表示已经输入数据。False表示失败</returns>
function PlayM4_InputData(nPort: LONG; pBuf: PByte; nSize: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>打开流（以协议的方式）</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="nProtocolType" IO="In">协议类型，若取值为PLAYM4_PROTOCOL_RTSP，表示RTSP协议</param>
/// <param name="pstSessionInfo" IO="In">协议信息结构体，若取值为PLAYM4_SESSION_INFO_SDP，表示nSessionInfoTypeSDP信息</param>
/// <param name="nBufPoolSize" IO="In">协议数据大小</param>
/// <returns>True成功，False失败</returns>
function PlayM4_OpenStreamAdvanced(nPort: LONG; nProtocolType: Integer; pstSessionInfo: PLAYM4_SESSION_INFO; nBufPoolSize: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

{-------- 播放控制 ------------------------------------------------------------}

/// <summary>开启播放</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="hWnd" IO="In">播放视频的窗口句柄</param>
/// <returns>True成功，False失败</returns>
function PlayM4_Play(nPort: LONG; HWND: HWND): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>关闭播放</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_Stop(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>暂停/恢复播放</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="nPause" IO="In">1：暂停，0：恢复</param>
/// <returns>True成功，False失败</returns>
function PlayM4_Pause(nPort: LONG; nPause: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>慢速播放</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>每次调用将使当前播放速度慢一倍；最多调用4次；要恢复正常播放调用PlayM4_Play()，从当前位置开始正常播放</remarks>
function PlayM4_Slow(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>快速播放</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>每次调用将使当前播放速度加快一倍，正常速度调用此接口最多调用4次；要恢复正常播放调用PlayM4_Play(),从当前位置开始正常播放；高清码流在高倍速播放时，由于受到解码和显示的限制，可能达不到所设置的速度</remarks>
function PlayM4_Fast(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>单帧前进</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>恢复正常播放需要调用PlayM4_Play()</remarks>
function PlayM4_OneByOne(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>单帧回退</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>只支持文件播放，且必须在文件索引生成之后才能调用，暂时不支持TS封装文件，且需要文件帧号是连续的</remarks>
function PlayM4_OneByOneBack(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>以独占方式打开声音</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_PlaySound(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>关闭声音（独占方式）</summary>
/// <returns>True成功，False失败</returns>
function PlayM4_StopSound: BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>以共享方式打开声音</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>建议PlayM4_PlaySound，PlayM4_PlaySoundShare不要混用。播放低帧率文件时，必须先开启声音再播放，否则可能造成声音播放不正常</remarks>
function PlayM4_PlaySoundShare(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>关闭声音（共享方式）</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>True成功，False失败</returns>
function PlayM4_StopSoundShare(nPort: LONG): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>设置音量</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="nVolume" IO="In">音量值，范围0-$FFFF</param>
/// <returns>True成功，False失败</returns>
function PlayM4_SetVolume(nPort: LONG; nVolume: Word): BOOL; stdcall; external 'PlayCtrl.dll';

/// <summary>获取音量</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>音量值。这里的音量是指声卡输出的主音量，会影响到其他的声音应用</returns>
function PlayM4_GetVolume(nPort: LONG): Word; stdcall; external 'PlayCtrl.dll';

{-------- 文件定位 ------------------------------------------------------------}

/// <summary>获取文件当前播放位置（百分比）</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <returns>范围0~100%</returns>
function PlayM4_GetPlayPos(nPort: LONG): Single; stdcall; external 'PlayCtrl.dll';

/// <summary>设置文件当前播放位置（百分比）</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="fRelativePos" IO="In">播放位置，范围：0~100%</param>
/// <returns>True成功，False失败</returns>
function PlayM4_SetPlayPos(nPort: LONG; fRelativePos: Single): BOOL; stdcall; external 'PlayCtrl.dll';

{-------- 文件索引 ------------------------------------------------------------}

/// <summary>设置建立索引回调</summary>
/// <param name="nPort" IO="In">播放通道号</param>
/// <param name="pFileRefDone" IO="In">索引回调函数</param>
/// <param name="nUser" IO="In">用户指针</param>
/// <returns>True成功，False失败</returns>
function PlayM4_SetFileRefCallBack(nPort: LONG; pFileRefDone: FileRefDoneCB; nUser: DWORD): BOOL; stdcall; external 'PlayCtrl.dll';

implementation

end.


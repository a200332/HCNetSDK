(*******************************************************************************
  名  称: 海康威视设备网络 SDK
  版  本: V5.3.6.30_build20180816_Win32
  原文件: HCNetSDK.h
  改  写: JXQ
  日  期: 2018.12.15
  方  式: 静态调用
*******************************************************************************)

unit HCNetSDK;

interface

uses
  SysUtils, Windows;

{$I WindowsEx.inc}      //Windows类型扩展

{======== 常量定义 ============================================================}

const
{-------- 一般常量 ------------------------------------------------------------}
  NET_DVR_DEV_ADDRESS_MAX_LEN = 129;
  NET_DVR_LOGIN_USERNAME_MAX_LEN = 64;
  NET_DVR_LOGIN_PASSWD_MAX_LEN = 64;
  SERIALNO_LEN = 48;
  STREAM_ID_LEN = 32;
  NAME_LEN = 32;
  MACADDR_LEN = 6;
  MAX_LICENSE_LEN = 16;
  MAX_ILLEGAL_LEN = 64;
  CARDNUM_LEN_OUT = 32;
  GUID_LEN = 16;
  DEV_TYPE_NAME_LEN = 24;
  MAX_EVENTID_LEN = 64;
  MAX_INTERVAL_NUM = 4;

{-------- 错误码 --------------------------------------------------------------}
  NET_DVR_NOERROR = 0;                  //没有错误
  NET_DVR_PASSWORD_ERROR = 1;           //用户名密码错误
  NET_DVR_NOENOUGHPRI = 2;              //权限不足
  NET_DVR_NOINIT = 3;                   //SDK未初始化
  NET_DVR_CHANNEL_ERROR = 4;            //通道号错误
  NET_DVR_OVER_MAXLINK = 5;             //连接到设备的用户个数超过最大
  NET_DVR_VERSIONNOMATCH = 6;           //版本不匹配
  NET_DVR_NETWORK_FAIL_CONNECT = 7;     //连接设备失败
  NET_DVR_NETWORK_SEND_ERROR = 8;       //向设备发送失败
  NET_DVR_NETWORK_RECV_ERROR = 9;       //从设备接收数据失败
  NET_DVR_NETWORK_RECV_TIMEOUT = 10;    //从设备接收数据超时
  NET_DVR_NETWORK_ERRORDATA = 11;       //传送的数据有误
  NET_DVR_ORDER_ERROR = 12;             //调用次序错误
  NET_DVR_COMMANDTIMEOUT = 14;          //设备命令执行超时
  NET_DVR_PARAMETER_ERROR = 17;         //参数错误
  NET_DVR_NODISK = 19;                  //设备无硬盘
  NET_DVR_NOSUPPORT = 23;               //设备不支持
  NET_DVR_DVRNORESOURCE = 28;           //设备资源不足
  NET_DVR_NOSPECFILE = 33;              //回放时设备没有指定的文件
  NET_DVR_CREATEFILE_ERROR = 34;        //创建文件出错。本地录像、保存图片、获取配置文件和远程下载录像时创建文件失败
  NET_DVR_ALLOC_RESOURCE_ERROR = 41;    //SDK资源分配错误
  NET_DVR_AUDIO_MODE_ERROR = 42;        //声卡模式错误
  NET_DVR_NOENOUGH_BUF = 43;            //缓冲区太小
  NET_DVR_CREATESOCKET_ERROR = 44;      //创建SOCKET出错
  NET_DVR_MAX_NUM = 46;                 //个数达到最大
  NET_DVR_USERNOTEXIST = 47;            //用户不存在。注册的用户ID已注销或不可用
  NET_DVR_PLAYERFAILED = 51;            //调用播放库中某个函数失败
  NET_DVR_MAX_USERNUM = 52;             //登录设备的用户数达到最大
  NET_DVR_GETLOCALIPANDMACFAIL = 53;    //获得本地PC的IP地址或物理地址失败
  NET_DVR_LOADPLAYERSDKFAILED = 64;     //载入当前目录下Player Sdk出错
  NET_DVR_LOADPLAYERSDKPROC_ERROR = 65; //找不到Player Sdk中某个函数入口
  NET_DVR_LOADDSSDKFAILED = 66;         //载入当前目录下DSsdk出错
  NET_DVR_LOADDSSDKPROC_ERROR = 67;     //找不到DsSdk中某个函数入口
  NET_DVR_BINDSOCKET_ERROR = 72;        //绑定套接字失败
  NET_DVR_SOCKETCLOSE_ERROR = 73;       //socket连接中断，此错误通常是由于连接中断或目的地不可达
  NET_DVR_SOCKETLISTEN_ERROR = 75;      //监听失败
  NET_DVR_PROGRAM_EXCEPTION = 76;       //程序异常
  NET_DVR_IPCHAN_NOTALIVE = 83;         //预览时外接IP通道不在线
  NET_DVR_RTSP_SDK_ERROR = 84;          //加载标准协议通讯库StreamTransClient失败
  NET_DVR_CONVERT_SDK_ERROR = 85;       //加载转封装库失败

{-------- 异常消息 ------------------------------------------------------------}
  EXCEPTION_EXCHANGE = $8000;                         //用户交互时异常(注册心跳超时，心跳间隔为2分钟)
  EXCEPTION_AUDIOEXCHANGE = $8001;                    //语音对讲异常
  EXCEPTION_ALARM = $8002;                            //报警异常
  EXCEPTION_PREVIEW = $8003;                          //网络预览异常
  EXCEPTION_SERIAL = $8004;                           //透明通道异常
  EXCEPTION_RECONNECT = $8005;                        //预览时重连
  EXCEPTION_ALARMRECONNECT = $8006;                   //报警时重连
  EXCEPTION_SERIALRECONNECT = $8007;                  //透明通道重连
  SERIAL_RECONNECTSUCCESS = $8008;                    //透明通道重连成功
  EXCEPTION_PLAYBACK = $8010;                         //回放异常
  EXCEPTION_DISKFMT = $8011;                          //硬盘格式化
  EXCEPTION_PASSIVEDECODE = $8012;                    //被动解码异常
  EXCEPTION_EMAILTEST = $8013;                        //邮件测试异常
  EXCEPTION_BACKUP = $8014;                           //备份异常
  PREVIEW_RECONNECTSUCCESS = $8015;                   //预览时重连成功
  ALARM_RECONNECTSUCCESS = $8016;                     //报警时重连成功
  RESUME_EXCHANGE = $8017;                            //用户交互恢复
  NETWORK_FLOWTEST_EXCEPTION = $8018;                 //网络流量检测异常
  EXCEPTION_PICPREVIEWRECONNECT = $8019;              //图片预览重连
  PICPREVIEW_RECONNECTSUCCESS = $8020;                //图片预览重连成功
  EXCEPTION_PICPREVIEW = $8021;                       //图片预览异常
  EXCEPTION_MAX_ALARM_INFO = $8022;                   //报警信息缓存已达上限
  EXCEPTION_LOST_ALARM = $8023;                       //报警丢失
  EXCEPTION_PASSIVETRANSRECONNECT = $8024;            //被动转码重连
  PASSIVETRANS_RECONNECTSUCCESS = $8025;              //被动转码重连成功
  EXCEPTION_PASSIVETRANS = $8026;                     //被动转码异常
  EXCEPTION_RELOGIN = $8040;                          //用户重登录
  RELOGIN_SUCCESS = $8041;                            //用户重登录成功
  EXCEPTION_PASSIVEDECODE_RECONNNECT = $8042;         //被动解码重连
  EXCEPTION_RELOGIN_FAILED = $8044;                   //重登录失败，停止重登录
  EXCEPTION_PREVIEW_RECONNECT_CLOSED = $8045;         //关闭预览重连功能
  EXCEPTION_ALARM_RECONNECT_CLOSED = $8046;           //关闭报警重连功能
  EXCEPTION_SERIAL_RECONNECT_CLOSED = $8047;          //关闭透明通道重连功能
  EXCEPTION_PIC_RECONNECT_CLOSED = $8048;             //关闭回显重连功能
  EXCEPTION_PASSIVE_DECODE_RECONNECT_CLOSED = $8049;  //关闭被动解码重连功能
  EXCEPTION_PASSIVE_TRANS_RECONNECT_CLOSED = $804a;   //关闭被动转码重连功能

{-------- 设备配置命令 --------------------------------------------------------}
  NET_DVR_GET_DEVICECFG_V40 = 1100;     //获取设备参数(扩展)
  NET_DVR_GET_TIMECFG = 118;            //获取时间参数
  NET_DVR_GET_DECODERCFG_V40 = 6319;    //获取RS485(云台解码器)参数
  NET_DVR_GET_RS232CFG_V30 = 1036;      //获取RS232串口参数
  NET_DVR_GET_EXCEPTIONCFG_V40 = 6177;  //获取异常参数
  NET_DVR_GET_ZONEANDDST = 128;         //获取时区和夏时制参数
  NET_DVR_GET_DEVSERVER_CFG = 3257;     //获取模块服务配置
  NET_DVR_GET_HOLIDAY_PARAM_CFG = 1240; //获取节假日参数


{-------- 设备类型/型号 -------------------------------------------------------}
  DVR = 1;                            //尚未定义的DVR类型
  ATMDVR = 2;                         //ATM DVR
  DVS = 3;                            //DVS
  Dec = 4;                            //6001D
  ENC_DEC = 5;                        //6001F
  DVR_HC = 6;                         //8000HC
  DVR_HT = 7;                         //8000HT
  DVR_HF = 8;                         //8000HF
  DVR_HS = 9;                         //8000HS DVR(no audio)
  DVR_HTS = 10;                       //8016HTS DVR(no audio)
  DVR_HB = 11;                        //HB DVR(SATA HD)
  DVR_HCS = 12;                       //8000HCS DVR
  DVS_A = 13;                         //带ATA硬盘的DVS
  DVR_HC_S = 14;                      //8000HC-S
  DVR_HT_S = 15;                      //8000HT-S
  DVR_HF_S = 16;                      //8000HF-S
  DVR_HS_S = 17;                      //8000HS-S
  ATMDVR_S = 18;                      //ATM-S
  DVR_7000H = 19;                     //7000H系列
  DEC_MAT = 20;                       //多路解码器
  DVR_MOBILE = 21;                    //mobile DVR
  DVR_HD_S = 22;                      //8000HD-S
  DVR_HD_SL = 23;                     //8000HD-SL
  DVR_HC_SL = 24;                     //8000HC-SL
  DVR_HS_ST = 25;                     //8000HS_ST
  DVS_HW = 26;                        //6000HW
  DS630X_D = 27;                      //多路解码器
  DS640X_HD = 28;                     //640X高清解码器
  DS610X_D = 29;                      //610X解码器
  IPCAM = 30;                         //标清网络摄像机
  MEGA_IPCAM = 31;                    //高清网络摄像机
  IPCAM_X62MF = 32;                   //X62MF系列摄像机
  ITCCAM = 35;                        //智能交通摄像机
  IVS_IPCAM = 36;                     //智能分析高清网络摄像机(人脸抓拍机)
  ZOOMCAM = 38;                       //一体机
  IPDOME = 40;                        //IP标清快球
  IPDOME_MEGA200 = 41;                //IP 200万高清快球
  IPDOME_MEGA130 = 42;                //IP 130万高清快球
  IPDOME_AI = 43;                     //IP 高清智能快球
  TII_IPCAM = 44;                     //红外热成像摄像机
  IPTC_DOME = 45;                     //红外热成像双目球机
  DS_2DP_Z = 46;                      //球型鹰眼(大)
  DS_2DP = 47;                        //非球型鹰眼(小)
  ITS_WMS = 48;                       //称重数据管理服务器
  IPMOD = 50;                         //IP 模块
  TRAFFIC_YTDOME = 51;                //交通智能云台(不带雷达测速)
  TRAFFIC_RDDOME = 52;                //交通智能云台(带雷达测速)
  IDS6501_HF_P = 60;                  //6501 车牌识别
  IDS6101_HF_A = 61;                  //智能ATM
  IDS6002_HF_B = 62;                  //双摄像机跟踪：DS6002-HF/B
  IDS6101_HF_B = 63;                  //行为分析：DS6101-HF/B
  IDS52XX = 64;                       //智能分析仪
  IDS90XX = 65;                       //9000智能
  IDS8104_AHL_S_HX = 66;              //海鑫人脸识别 ATM
  IDS8104_AHL_S_H = 67;               //私有人脸识别 ATM
  IDS91XX = 68;                       //9100智能
  IIP_CAM_B = 69;                     //智能行为IP摄像机
  IIP_CAM_F = 70;                     //智能人脸IP摄像机
  DS71XX_H = 71;                      //DS71XXH_S
  DS72XX_H_S = 72;                    //DS72XXH_S
  DS73XX_H_S = 73;                    //DS73XXH_S
  DS72XX_HF_S = 74;                   //DS72XX_HF_S
  DS73XX_HFI_S = 75;                  //DS73XX_HFI_S
  DS76XX_H_S = 76;                    //DS76XX_H_S
  DS76XX_N_S = 77;                    //DS76XX_N_S
  DS_TP3200_EC = 78;                  //机柜智能检测仪
  DS81XX_HS_S = 81;                   //DS81XX_HS_S
  DS81XX_HL_S = 82;                   //DS81XX_HL_S
  DS81XX_HC_S = 83;                   //DS81XX_HC_S
  DS81XX_HD_S = 84;                   //DS81XX_HD_S
  DS81XX_HE_S = 85;                   //DS81XX_HE_S
  DS81XX_HF_S = 86;                   //DS81XX_HF_S
  DS81XX_AH_S = 87;                   //DS81XX_AH_S
  DS81XX_AHF_S = 88;                  //DS81XX_AHF_S
  DS90XX_HF_S = 90;                   //DS90XX_HF_S
  DS91XX_HF_S = 91;                   //DS91XX_HF_S
  DS91XX_HD_S = 92;                   //91XXHD-S(MD)
  IDS90XX_A = 93;                     //9000智能 ATM
  IDS91XX_A = 94;                     //9100智能 ATM
  DS95XX_N_S = 95;                    //DS95XXN-S NVR
  DS96XX_N_SH = 96;                   //DS96XXN-SH NVR
  DS90XX_HF_SH = 97;                  //DS90XX_HF_SH
  DS91XX_HF_SH = 98;                  //DS91XX_HF_SH
  DS_B10_XY = 100;                    //视频综合平台设备型号(X:编码板片数，Y:解码板片数)
  DS_6504HF_B10 = 101;                //视频综合平台内部编码器
  DS_6504D_B10 = 102;                 //视频综合平台内部解码器
  DS_1832_B10 = 103;                  //视频综合平台内部码分器
  DS_6401HFH_B10 = 104;               //视频综合平台内部光纤板
  DS_65XXHC = 105;                    //65XXHC DVS
  DS_65XXHC_S = 106;                  //65XXHC-SATA DVS
  DS_65XXHF = 107;                    //65XXHF DVS
  DS_65XXHF_S = 108;                  //65XXHF-SATA DVS
  DS_6500HF_B = 109;                  //65 rack DVS
  IVMS_6200_C = 110;                  //iVMS-6200(/C) 客流量统计
  IVMS_6200_B = 111;                  //IVMS_6200_B 行为分析
  DS_72XXHV_ST15 = 112;               //72XXHV_ST15 DVR
  DS_72XXHV_ST20 = 113;               //72XXHV_ST20 DVR
  IVMS_6200_T = 114;                  //IVMS-6200(/T)
  IVMS_6200_BP = 115;                 //IVMS-6200(/BP)
  DS_81XXHC_ST = 116;                 //DS_81XXHC_ST
  DS_81XXHS_ST = 117;                 //DS_81XXHS_ST
  DS_81XXAH_ST = 118;                 //DS_81XXAH_ST
  DS_81XXAHF_ST = 119;                //DS_81XXAHF_ST
  DS_66XXDVS = 120;                   //DS_66XXDVS
  DS_1964_B10 = 121;                  //视频综合平台内部报警器
  DS_B10N04_IN = 122;                 //视频综合平台内部级联输入
  DS_B10N04_OUT = 123;                //视频综合平台内部级联输出
  DS_B10N04_INTEL = 124;              //视频综合平台内部智能
  DS_6408HFH_B10E_RM = 125;           //V6高清
  DS_B10N64F1_RTM = 126;              //V6级联不带DSP
  DS_B10N64F1D_RTM = 127;             //V6级联带DSP
  DS_B10_SDS = 128;                   //视频综合平台子域控制器
  DS_B10_DS = 129;                    //视频综合平台域控制器
  DS_6401HFH_B10V = 130;              //VGA高清编码器
  DS_6504D_B10B = 131;                //视频综合平台内部标清解码器
  DS_6504D_B10H = 132;                //视频综合平台内部高清解码器
  DS_6504D_B10V = 133;                //视频综合平台内部VGA解码器
  DS_6408HFH_B10S = 134;              //视频综合平台SDI子板
  DS_18XX_N = 135;                    //矩阵接入网关
  DS_6504HF_B10F_CLASS = 136;         //光端机SD
  DS_18XX_PTZ = 141;                  //网络码分类产品
  DS_19AXX = 142;                     //通用报警主机类产品
  DS_19BXX = 143;                     //家用报警主机
  DS_19CXX = 144;                     //自助银行报警主机
  DS_19DXX = 145;                     //动环监控报警主机
  DS_19XX = 146;                      //1900系列报警主机
  DS_19SXX = 147;                     //视频报警主机
  DS_1HXX = 148;                      //ATM防护舱控制器
  DS_PEAXX = 149;                     //紧急求助报警产品
  DS_PWXX = 150;                      //无线报警主机产品
  DS_PMXX = 151;                      //4G网络模块
  DS_19DXX_S = 152;                   //视频动环监控主机
  DS_C10H = 161;                      //多屏控制器
  DS_C10N_BI = 162;                   //BNC处理器
  DS_C10N_DI = 163;                   //RGB处理器
  DS_C10N_SI = 164;                   //码流处理器
  DS_C10N_DO = 165;                   //显示处理器
  DS_C10N_SERVER = 166;               //分布式服务器
  IDS_8104_AHFL_S_H = 171;            //8104ATM
  IDS_65XX_HF_A = 172;                //65 ATM
  IDS90XX_HF_RH = 173;                //9000 智能RH
  IDS91XX_HF_RH = 174;                //9100 智能RH设备
  IDS_65XX_HF_B = 175;                //65 行为分析
  IDS_65XX_HF_P = 176;                //65 车牌识别
  IVMS_6200_F = 177;                  //IVMS-6200(/F)人脸分析仪
  IVMS_6200_A = 178;                  //iVMS-6200(/A)
  IVMS_6200_F_S = 179;                //IVMS-6200(/F_S)人脸后检索分析仪
  DS90XX_HF_RH = 181;                 //DS90XX_HF_RH
  DS91XX_HF_RH = 182;                 //9100 RH设备
  DS78XX_S = 183;                     //78系列设备
  DS81XXHW_S = 185;                   //DVR_81XXHW_S
  DS81XXHW_ST = 186;                  //DVR_81XXHW_ST
  DS91XXHW_ST = 187;                  //DVR_91XXHW_ST
  DS91XX_ST = 188;                    //DVR_91XX_ST
  DS81XX_ST = 189;                    //DVR_81XX_ST
  DS81XXH_ST = 190;                   //DS81XXHDI_ST,DS81XXHE_ST
  DS73XXH_ST = 191;                   //DS73XXHI_ST
  DS81XX_SH = 192;                    //审讯81SH，81SHF
  DS81XX_SN = 193;                    //审讯81SNL
  DS96XXN_ST = 194;                   //NVR：DS96xxN_ST
  DS86XXN_ST = 195;                   //NVR：DS86xxN_ST
  DS80XXHF_ST = 196;                  //DS80xxHF_ST
  DS90XXHF_ST = 197;                  //DS90xxHF_ST
  DS76XXN_ST = 198;                   //NVR：DS76xxN_ST
  DS_9664N_RX = 199;                  //NVR：DS-9664N-RH、DS-9664N-RT
  ENCODER_SERVER = 200;               //编码卡服务器
  DECODER_SERVER = 201;               //解码卡服务器
  PCNVR_SERVER = 202;                 //PCNVR存储服务器
  CVR_SERVER = 203;                   //CVR
  DS_91XXHFH_ST = 204;                //高清DVR：DS_91xxHFH_ST
  DS_66XXHFH = 205;                   //66高清编码器
  TRAFFIC_TS_SERVER = 210;            //终端服务器
  TRAFFIC_VAR = 211;                  //视频分析记录仪
  IPCALL = 212;                       //IP可视对讲主机
  SAN_SERVER = 213;                   //SAN(与CVR_SERVER相同的程序，只是模式不同)
  DS_B11_M_CLASS = 301;               //B11
  DS_B12_M_CLASS = 302;               //B12
  DS_6504HF_B11_CLASS = 303;          //B11内部编码器
  DS_6504HF_B12_CLASS = 304;          //B12内部编码器
  DS_6401HFH_B11V_CLASS = 305;        //VGA高清
  DS_6401HFH_B12V_CLASS = 306;        //VGA高清
  DS_6408HFH_B11S_CLASS = 307;        //SDI
  DS_6408HFH_B12S_CLASS = 308;        //SDI
  DS_6504D_B11H_CLASS = 309;          //B11内部高清解码器
  DS_6504D_B11B_CLASS = 310;          //B11内部标清解码器
  DS_6504D_B12B_CLASS = 311;          //B12内部标清解码器
  DS_6504D_B11V_CLASS = 312;          //B11内部VGA解码器
  DS_6504D_B12V_CLASS = 313;          //B12内部VGA解码器
  DS_6401HFH_B10R_CLASS = 314;        //B10 RGB高清
  DS_6401HFH_B10D_CLASS = 315;        //B10 DVI高清
  DS_6401HFH_B10H_CLASS = 316;        //B10 HDMI高清
  DS_6401HFH_B11R_CLASS = 317;        //B11 RGB高清
  DS_6401HFH_B11D_CLASS = 318;        //B11 DVI高清
  DS_6401HFH_B11H_CLASS = 319;        //B11 HDMI高清
  DS_6401HFH_B12R_CLASS = 320;        //B12 RGB高清
  DS_6401HFH_B12D_CLASS = 321;        //B12 DVI高清
  DS_6401HFH_B12H_CLASS = 322;        //B12 HDMI高清
  DS_65XXD_B10Ex_CLASS = 323;         //B10 netra高清解码
  DS_6516HW_B10_CLASS = 324;          //B10 netra高清编码
  DS_6504D_B11Ex_CLASS = 328;         //B11 netra高清解码
  DS_6504D_B12Ex_CLASS = 329;         //B12 netra高清解码
  DS_6512_B11_CLASS = 330;            //B11 netra高清编码
  DS_6512_B12_CLASS = 331;            //B12 netra高清编码
  DS_6504D_B10H_CLASS = 332;          //视频综合平台内部高清解码器
  DS_65XXT_B10_CLASS = 333;           //视频综合平台转码子系统
  DS_65XXD_B10_CLASS = 335;           //视频综合平台万能解码板
  DS_IVMSE_B10X_CLASS = 336;          //X86服务器子系统
  DS_6532D_B10ES_CLASS = 337;         //增强型解码板_SDI(B10)
  DS_6508HFH_B10ES_CLASS = 338;       //SDI输入编码子系统
  DS_82NCG_CLASS = 340;               //联网网关中的子系统
  DS_82VAG_CLASS = 341;               //联网网关中的子系统
  DS_1802XXF_B10_CLASS = 342;         //光口交换子系统
  iDS_6504_B10EVAC_CLASS = 343;       //智能子系统
  iDS_6504_B10EDEC_CLASS = 344;       //智能子系统
  DS_6402HFH_B10EV_CLASS = 345;       //netra编码(VGA)
  DS_6402HFH_B10ED_CLASS = 346;       //netra编码(DVI)
  DS_6402HFH_B10EH_CLASS = 347;       //netra编码(HDMI)
  DS_6404HFH_B10T_RX_CLASS = 348;     //光纤接入编码
  DS_6504D_AIO_CLASS = 349;           //netra高清解码
  DS_IVMST_B10_CLASS = 350;           //X86转码子系统
  DS_6402_AIO_CLASS = 351;            //netra编码
  DS_iVMSE_AIO_CLASS = 352;           //x86服务器子系统
  DS_AIO_M_CLASS = 353;               //一体机
  DS_6508HF_B10E_CLASS = 355;         //BNC输入编码子系统
  DS_6404HFH_B10ES_CLASS = 356;       //SDI输入编码子系统
  DS_6402HFH_B10ER_CLASS = 358;       //RGB输入编码子系统
  DS_6404HFH_B10T_RM_CLASS = 361;     //光纤输入编码子系统
  DS_6516D_B10EB_CLASS = 362;         //BNC输出解码子系统
  DS_6516D_B10ES_CLASS = 363;         //SDI输出解码子系统
  DS_6508_B11E_CLASS = 365;           //BNC输入编码子系统
  DS_6402_B11ES_CLASS = 366;          //SDI输入编码子系统
  DS_6402_B11EV_CLASS = 367;          //VGA输入编码子系统
  DS_6402_B11ER_CLASS = 368;          //RGB输入编码子系统
  DS_6402_B11ED_CLASS = 369;          //DVI输入编码子系统
  DS_6402_B11EH_CLASS = 370;          //HDMI输入编码子系统
  DS_6516D_B11EB_CLASS = 371;         //BNC输出解码子系统
  DS_6516D_B11ES_CLASS = 372;         //SDI输出解码子系统
  DS_6508_B12E_CLASS = 373;           //BNC输入编码子系统
  DS_6402_B12ES_CLASS = 375;          //SDI输入编码子系统
  DS_6402_B12EV_CLASS = 376;          //VGA输入编码子系统
  DS_6402_B12ER_CLASS = 377;          //RGB输入编码子系统
  DS_6402_B12ED_CLASS = 378;          //DVI输入编码子系统
  DS_6402_B12EH_CLASS = 379;          //HDMI输入编码子系统
  DS_6516D_B12EB_CLASS = 380;         //BNC输出解码子系统
  DS_iVMSE_AIO_8100x_CLASS = 381;     //金融行业一体机X86子系统
  DS_iVMSE_AIO_87x_CLASS = 382;       //智能楼宇一体机X86子系统
  DS_6532D_B11ES_CLASS = 384;         //增强型解码板_SDI(B11)
  DS_6532D_B12ES_CLASS = 385;         //增强型解码板_SDI(B12)
  DS_B20_MSU_NP = 400;                //B20主控板
  DS_6416HFH_B20S = 401;              //SDI输入编码
  DS_6416HFH_B20_RM = 402;            //光纤输入高清编码
  DS_6564D_B20D = 403;                //DVI解码
  DS_6564D_B20H = 404;                //HDMI解码
  DS_6564D_B20V = 405;                //VGA解码
  DS_B20_6516D_DEV_CLASS = 406;       //B20解码子系统
  DS_6408HFH_B20V = 407;              //VGA编码板
  DS_MMC_B20_CLASS = 408;             //B20主控
  DS_CARD_CHIP_B20_CLASS = 409;       //B20主控子板
  DS_6564D_B20B_DEV_CLASS = 410;      //BNC解码子系统
  DS_6564D_B20S_DEV_CLASS = 411;      //SDI解码子系统
  DS_6532HF_B20B_DEV_CLASS = 412;     //BNC编码子系统
  DS_6408HFH_B20D_DEV_CLASS = 413;    //DVI编码子系统
  DS_6408HFH_B20H_DEV_CLASS = 414;    //HDMI编码子系统
  DS_IVMSE_B20_CLASS = 415;           //X86服务器子系统
  DS_6402HFH_B20Y_DEV_CLASS = 416;    //YUV编码子系统
  DS_6508HW_B20_DEV_CLASS = 417;      //HW编码子系统
  DS_B20N128Fx_B20_DEV_CLASS = 418;   //DS_B20N128Fx_M级联板
  DS_AIO_MCU_NP_DEV_CLASS = 419;      //IO主控板
  DS_6402_AIO_EV_DEV_CLASS = 420;     //VGA编码
  DS_6508D_AIO_EV_DEV_CLASS = 421;    //VGA解码
  DS_6508D_AIO_ED_DEV_CLASS = 422;    //DVI解码
  DS_6508D_AIO_EH_DEV_CLASS = 423;    //HDMI解码
  DS_6508HD_B20F_DEV_CLASS = 424;     //视频增强板
  DS_6402HFH_B20ES_DEV_CLASS = 425;   //3G SID编码
  DS_6532D_B20_DEV_CLASS = 426;       //B20解码子系统
  DS_IVMST_B20_DEV_CLASS = 427;       //X86服务器子系统
  DS_6416HFH_B20DD_DEV_CLASS = 428;   //DVI双链路
  DS_6441VS_B20_DEV_CLASS = 429;      //相机拼接类型
  DS_6404HFH_B20T_CLASS = 431;        //TVI
  DS_FS22_B20_DEV_CLASS = 432;        //交换机子系统
  DS_IVMSE_B20UH_DEV_CLASS = 433;     //超高清X86输入
  DS_6404HFH_B20Fx_DEV_CLASS = 436;   //光端机接入子系统
  DS_N128x_B20Fy_CLASS = 437;         //级联子系统
  DS_181600F_B20_CLASS = 438;         //网络光纤子系统
  DS_6904UD_B20H_CLASS = 439;         //超高清解码子系统
  DS_B21_MCU_NP_CLASS = 440;          //B21主控
  DS_B21_S10_x_CLASS = 441;           //B21机箱 x = A/S/D
  DS_6402HFH_B21D_CLASS = 442;        //B21编码子系统
  DS_6508HD_B21D_CLASS = 443;         //B21解码子系统
  DS_iVMSE_B21HW_CLASS = 444;         //B21 X86子系统
  DS_C10S = 501;                      //C10S 集中式大屏控制器
  DS_C10N_SDI = 551;                  //SDI处理器
  DS_C10N_BIW = 552;                  //8路BNC处理器
  DS_C10N_DON = 553;                  //显示处理器
  DS_C10N_TVI = 554;                  //TVI输入板
  DS_C10N_DI2 = 555;                  //DVI 2路输入板
  DS_C10N_AUDIO_OUT = 556;            //DVI，HDMI，VGA输出板带音频
  DS_C10N_AUDIO_IN = 557;             //DVI，HDMI，VGA输入板带音频
  DS_C20N = 570;                      //分布式大屏控制器
  DS_C20N_BNC = 571;                  //BNC输入设备
  DS_C20N_DVI = 572;                  //DVI输入设备
  DS_C20N_DP = 573;                   //DP输入设备
  DS_C20N_OUT = 574;                  //输出设备
  DS_19M00_MN = 601;                  //报警主机百兆网络模块
  DS_KH8302_A = 602;                  //室内机
  DS_KD8101_2 = 603;                  //门口机
  DS_KM8301 = 604;                    //管理机
  DS_KVXXXX_XX = 605;                 //别墅门口机
  DS64XXHD_T = 701;                   //64-T高清解码器
  DS_65XXD = 703;                     //65万能解码器
  DS63XXD_T = 704;                    //63-T标清解码器
  SCE_SERVER = 705;                   //抓屏服务器
  DS_64XXHD_S = 706;                  //DS-64xxHD-S高清解码器
  DS_68XXT = 707;                     //多功能视音频转码器
  DS_65XXD_T = 708;                   //65D-T万能解码器
  DS_65XXHD_T = 709;                  //65HD-T万能解码器
  DS_69XXUD = 710;                    //69XXUD超高清解码器
  DS_65XXUD = 711;                    //65XXUD解码器
  DS_65XXUD_L = 712;                  //65XXUD_L解码器
  DS_65XXUD_T = 713;                  //65XXUD_T解码器
  DS_D20XX = 750;                     //LCD屏幕 解码卡
  DS_C50S = 751;                      //SDI矩阵
  DS_D50XX = 752;                     //LCD屏幕解码卡
  DS_D40 = 760;                       //LED屏发送卡
  DS_65VMXX = 770;                    //视频会议服务器
  DS_65VTXX = 771;                    //视频会议终端
  DS_65VTA = 772;                     //视频会议一体式终端
  DS_65VT_RX = 773;                   //互动教学终端
  DS_CS_SERVER = 800;                 //虚拟屏服务器
  DS_K260X = 850;                     //门禁主机
  DS_K1TXXX = 851;                    //指纹一体机
  DS_K280X = 852;                     //经济型门禁主机
  DS_K1T80X = 854;                    //经济型门禁一体机
  RAC_6X00 = 856;                     //汉军指纹门禁一体机
  DS_K2602_AX = 857;                  //人员通道主机
  DS_K1T803F = 858;                   //经济型指纹门禁产品
  DS_K2700 = 859;                     //分布式三层架构门禁主机
  DS_K270X = 860;                     //分布式三层架构就地控制器
  DS_K1T500S = 861;                   //视屏门禁一体机
  DS_K1A801F = 862;                   //经济型指纹门禁产品
  DS_K1T600X = 863;                   //人脸识别门禁一体机
  DS_K22X = 864;                      //梯控主控制器
  DS_K2M0016AX = 865;                 //梯控分控制器
  DS_K560XX = 870;                    //立式/台式智能身份识别终端
  DS_6800M = 900;                     //68M合码器
  DS_68NTH = 901;                     //信息发布主机
  DS_D60S = 902;                      //信息发布服务器
  DS_D10 = 931;                       //背投显示设备
  DS_3K0X_NM = 951;                   //光纤收发器
  DS_3E2328 = 952;                    //百兆交换机
  SCREEN_LINK_SERVER = 971;           //屏幕服务器
  DS_D51OPSXX = 972;                  //OPS电脑盒
  IP_PTSYS_MEGA200 = 1001;            //IP 200万一体化云台
  IPCAM_FISHEYE = 1002;               //鱼眼摄像机
  IPCAM_FISHEYE_E = 1003;             //经济型鱼眼摄像机
  IPCAM_BINOCULAR = 1004;             //双目摄像机
  IPCAM_365 = 1010;                   //支持365的平台的IPC CAM
  IPCAM_R0 = 1011;                    //支持A5S的平台的IPC CAM
  IPCAM_R1 = 1012;                    //支持385的平台的IPC CAM
  IPCAM_R2 = 1013;                    //支持R2的平台的IPC CAM
  IPCAM_R3 = 1014;                    //支持8127的平台的IPC CAM
  IPCAM_R4 = 1015;                    //支持S2的平台的IPC CAM
  IPDOME_365 = 1110;                  //支持365的平台的IPD CAM
  IPDOME_R0 = 1111;                   //支持A5S的平台的IPD CAM
  IPDOME_R1 = 1112;                   //支持385的平台的IPD CAM
  IPDOME_R2 = 1113;                   //支持R2的平台的IPD CAM
  IPDOME_R3 = 1114;                   //支持8127的平台的IPD CAM
  IPDOME_R4 = 1115;                   //支持S2的平台的IPD CAM
  ITCCAM_R3 = 1211;                   //支持8127的平台的ITCCAM
  UAV_S = 1300;                       //无人机基站设备(Ummanned Aerial Vehicle – Station)
  TRAFFIC_ECT = 1400;                 //出入口终端服务器
  TRAFFIC_PARKING_SERVER = 1401;      //停车场服务器
  TRAFFIC_TME = 1402;                 //TME出入口控制机
  DS90XXHW_ST = 2001;                 //混合DVR：DS-90xxHW-ST
  DS72XXHX_SH = 2002;                 //DS-72xxHV-SH，DS-72xxHF-SH
  DS_92XX_HF_ST = 2003;               //DS-92xxHF-ST
  DS_91XX_HF_XT = 2004;               //Netra DVR: DS-91xxHF-XT
  DS_90XX_HF_XT = 2005;               //Netra 混合DVR: DS-90xxHF-XT
  DS_73XXHX_SH = 2006;                //Netra DVR: DS-73xxHX-SH
  DS_72XXHFH_ST = 2007;               //Netra DVR: DS-72xxHFH-ST
  DS_67XXHF_SATA = 2008;              //DVS: DS-67xxHF-SATA
  DS_67XXHW = 2009;                   //DVS: DS-67xxHW
  DS_67XXHW_SATA = 2010;              //DVS: DS-67xxHW-SATA
  DS_67XXHF = 2011;                   //DVS: DS-67xxHF
  DS_72XXHF_SV = 2012;                //DVR: DS-72xxHF-SV
  DS_72XXHW_SV = 2013;                //DVR: DS-72xxHW-SV
  DS_81XXHX_SH = 2014;                //DVR: DS-81xxHX-SH
  DS_71XXHX_SL = 2015;                //DVR: DS-71xxHX-SL
  DS_76XXH_ST = 2016;                 //DVR: DS-76xxH-ST
  DS_73XXHFH_ST = 2017;               //DVR: DS-73xxHFH-ST
  DS_81XXHFH_ST = 2018;               //DVR: DS-81xxHFH-ST
  DS_72XXHFH_SL = 2019;               //DVR: DS-72xxHFH-SL
  DS_FDXXCGA_FLT = 2020;              //2盘位ATM
  IDS_91XX_HF_ST_A = 2100;            //iDS-9100HF-ST/A
  IDS_91XX_HF_ST_B = 2101;            //iDS-9100HF-ST/B
  IDS_90XX_HF_ST_A = 2102;            //iDS-9000HF-ST/A
  IDS_90XX_HF_ST_B = 2103;            //iDS-9000HF-ST/B
  IDS_81XX_HF_ST_A = 2104;            //iDS-8100HF-ST/A
  IDS_81XX_HF_ST_B = 2105;            //iDS-8100HF-ST/B
  IDS_80XX_HF_ST_A = 2106;            //iDS-8000HF-ST/A
  IDS_80XX_HF_ST_B = 2107;            //iDS-8000HF-ST/B
  IDS_8104_AHFL_ST = 2108;            //智能混合ATM机
  IDS_2CD6812F_C = 2109;              //垂直双目相机
  DS_77XXN_ST = 2201;                 //Netra NVR: DS-77xxN-ST
  DS_95XX_N_ST = 2202;                //Netra NVR: DS-95xxN-ST
  DS_85XX_N_ST = 2203;                //Netra NVR: DS-85xxN-ST
  DS_96XX_N_XT = 2204;                //Netra NVR: DS-96xxN-XT
  DS_76XX_N_SE = 2205;                //Netra NVR: DS-76xxN-SE、DS-78xxN-SH
  DS_86XXSN_SX = 2206;                //Netra 审讯NVR，包括4种类型：DS-8608SNL-SP、DS-8608SNL-ST、DS-8608SN-SP、DS-8608SN-ST，L表示带LCD，P表POE
  DS_96XX_N_RX = 2207;                //Netra NVR: DS-96xxN-RX
  DS_71XXN_SL = 2208;                 //DS-71XXN-SL 民用产品
  CS_N1_1XX = 2209;                   //CS_N1_1XX，民用事业部所用
  DS_71XXN_SN = 2210;                 //71XX_N_SN 经济型民用产品
  CS_N1_2XX = 2211;                   //N1_2XX 民用事业部所用
  DS_76XX_N_SHT = 2212;               //76XX_N_SHT
  DS_96XXX_N_E = 2213;                //高新性能NVR(256路)
  DS_76XXN_EX = 2214;                 //76/78N-EX(/N/P)系列NVR，包括4/8/16路的E1一盘位设备和8/16/32路的E2两盘位设备
  DS_77XXN_E4 = 2215;                 //77N-E4(/N/P)系列NVR，包括8/16/32路设备
  DS_86XXN_E8 = 2216;                 //86N-E8(/N/P)系列NVR，包括8/16/32路设备
  DS_9616N_H8 = 2217;                 //DS_9616N_H8
  DS_72XXHX_EX_GXY = 2218;            //72系列无线DVR产品(其中72后面的xx表示通道数，E后面的X表示的是盘位数，G XY表示的是无线类型)
  DS_76XXN_EX_GXY = 2219;             //76系列无线NVR产品(其中76后面的xx表示通道数，E后面的X表示的是盘位数，G XY表示的是无线类型)
  DS_72XXHXH_SH_21 = 2220;            //XVR: 72HGH-SH系列4路
  DS_72XXHXH_SH_31 = 2221;            //XVR: 72HGH-SH系列8路16路、72HQH-SH系列
  DS_73XXHXH_SH_31 = 2222;            //XVR: 73HGH-SH系列、73HQH-SH系列
  DS_81XXHXH_SH_31 = 2223;            //XVR: 81HGH-SH系列、81HQH-SH系列
  DS_71XXHXH_SH_21 = 2224;            //XVR: 71HGH-SH系列4路
  DS_71XXHXH_SH_31 = 2225;            //XVR: 71HGH-SH系列8路16路、71HQH-SH系列
  DS_NBXX_N_E1_X = 2226;              //便携式主机
  DS_96XXN_FX = 2230;                 //96N-Fx系列NVR，包括8/16/32路设备
  DS_86XXN_FX = 2231;                 //86N-Fx系列NVR，包括8/16/32路设备
  DS_96XXXN_HX = 2232;                //96xxxN-Hx系列高性能NVR
  DS_86XXN_I = 2233;                  //86N_I系列NVR
  DS_77XX_N_I = 2234;                 //77N_I系列NVR
  DS_76XX_N_I = 2235;                 //76N_I系列NVR
  DS_78XX_N_I = 2236;                 //78N_I系列NVR
  DS_96XXXN_IX = 2237;                //96xxxN-Ix系列NVR(DS-96128N-I16、DS-96128N-I24、DS-96256N-I16、DS-96256N-I24)
  DS_90XXHXH_XT = 2238;               //DS-9016HQH-XT
  PCNVR_IVMS_4200 = 2301;             //iVMS-4200的存储服务器
  IVMS_6200_TP = 2401;                //IVMS-6200 交通诱导分析仪
  IVMS_6200_TF = 2402;                //IVMS-6200 交通取证分析仪
  IVMS_6200_D = 2403;                 //iVMS-6200(/D)
  IDS_81XXAHW_ST = 2405;              //iDS-81xxAHW-ST
  IDS_81XXAHW_SP = 2406;              //iDS-81xxAHW-SP
  IDS_81XXAHWL_ST = 2407;             //iDS-81xxAHWL-ST
  IDS_81XXAHWL_SP = 2408;             //iDS-81xxAHWL-SP
  IDS_9616N_H8 = 2409;                //iDS_9616N_H8
  IVMS_6200_SMD = 2500;               //IVMS_6200_SMD
  DS_81XXAHW_ST = 2501;               //81HW-ST混合ATM DVR
  DS_81XXAHW_SP = 2502;               //81HW-SP混合ATM DVR
  DS_81XXAHWL_ST = 2503;              //81AHWL-ST混合ATM DVR
  DS_81XXAHWL_SP = 2504;              //81AHWL-SP混合ATM DVR
  DS_81XXAHGH_E4 = 2601;              //TVI ATM机
  DS_81XX_SHXL_K4 = 6101;             //KY2017平台审讯机DS-8104SHFH(L)-K4/4P
  DS_8116THFHL_F4 = 6201;             //标准庭审主机DS-8116THFHL-F4
  DS_81XXAHQ_E4 = 6301;               //DS_81XXAHQ_E4(TVI ATM)
  IDS_81XXAHQ_E4 = 6302;              //IDS_81XXAHQ_E4(智能TVI ATM)
  IDS_96XX_NX_S = 7502;               //超脑智能NVR(人体识别)
  IDS_96XX_NX_V = 7503;               //超脑加油站NVR
  IDS_96XX_NX_FA = 7504;              //人脸超脑智能NVR
  IDS_86XX_NX_IX_B = 7505;            //86系列安全帽检测NVR产品：IDS-8632NX-I8/B
  iDS_PT = 8451;                      //全景细节相机
  IDS_67XX_NX_L = 12502;              //67系列录播NVS产品
  IDS_ENIXX_XHE = 12503;              //录播NVS行业专业产品
  IDS_67XX_NX_B = 12505;              //67系列安全帽检测NVS产品：iDS-6708NX/B
  DS_IE63XX_E_FA = 13001;             //脸谱单机
  DS_GPKIA01XX = 13002;               //猎鹰服务器
  DS_IC01XX_F = 13003;                //脸谱纯分析
  DS_DS_FS = 13004;                   //人脸静态数据服务器
  DS_DS_FD = 13005;                   //抓拍检测服务器
  DS_GPKIB01XX_V = 13006;             //刀锋
  DS_I01CP_CD = 13007;                //模型对比服务器
  DS_LNX_RF = 13501;                  //智能锁盒子

{-------- 预览回调数据类型 ----------------------------------------------------}
  NET_DVR_SYSHEAD = 1;          //系统头数据
  NET_DVR_STREAMDATA = 2;       //流数据(包括复合流或音视频分开的视频流数据)
  NET_DVR_AUDIOSTREAMDATA = 3;  //音频数据
  NET_DVR_PRIVATE_DATA = 112;   //私有数据,包括智能信息

{-------- 报警消息 ------------------------------------------------------------}
  COMM_UPLOAD_PLATE_RESULT = $2800; //交通抓拍结果
  COMM_ITS_PLATE_RESULT = $3050;    //交通抓拍结果(新报警信息)

{-------- 获取状态信息 --------------------------------------------------------}
  NET_DVR_FILE_SUCCESS = 1000;    //获取文件信息成功
  NET_DVR_FILE_NOFIND = 1001;     //未查找到文件
  NET_DVR_ISFINDING = 1002;       //正在查找请等待
  NET_DVR_NOMOREFILE = 1003;      //没有更多的文件，查找结束
  NET_DVR_FILE_EXCEPTION = 1004;  //查找文件时异常

{-------- 控制录像回放状态命令 ------------------------------------------------}
  NET_DVR_PLAYSTART = 1;            //开始播放
  NET_DVR_PLAYPAUSE = 3;            //暂停播放
  NET_DVR_PLAYRESTART = 4;          //恢复播放(在暂停后调用将恢复暂停前的速度播放)
  NET_DVR_PLAYFAST = 5;             //快放
  NET_DVR_PLAYSLOW = 6;             //慢放
  NET_DVR_PLAYNORMAL = 7;           //正常速度播放(快放或者慢放时调用将恢复单倍速度播放)
  NET_DVR_PLAYFRAME = 8;            //单帧放(恢复正常回放使用NET_DVR_PLAYNORMAL命令)
  NET_DVR_PLAYSTARTAUDIO = 9;       //打开声音
  NET_DVR_PLAYSTOPAUDIO = 10;       //关闭声音
  NET_DVR_PLAYAUDIOVOLUME = 11;     //调节音量，取值范围[0,$ffff]
  NET_DVR_PLAYSETPOS = 12;          //改变文件回放的进度
  NET_DVR_PLAYGETPOS = 13;          //获取按文件或者按时间回放的进度
  NET_DVR_PLAYGETTIME = 14;         //获取当前已经播放的时间(按文件回放的时候有效)
  NET_DVR_PLAYGETFRAME = 15;        //获取当前已经播放的帧数(按文件回放的时候有效，倒放不支持)
  NET_DVR_GETTOTALFRAMES = 16;      //获取当前播放文件总的帧数(按文件回放的时候有效，倒放不支持)
  NET_DVR_GETTOTALTIME = 17;        //获取当前播放文件总的时间(按文件回放的时候有效)
  NET_DVR_THROWBFRAME = 20;         //丢B帧
  NET_DVR_SETSPEED = 24;            //设置码流速度，速度单位：kbps，最小为256kbps，最大为设备带宽
  NET_DVR_KEEPALIVE = 25;           //保持与设备的心跳(如果回调阻塞，建议2秒发送一次)
  NET_DVR_PLAYSETTIME = 26;         //按绝对时间定位
  NET_DVR_PLAYGETTOTALLEN = 27;     //获取按时间回放对应时间段内的所有文件的总长度
  NET_DVR_PLAY_FORWARD = 29;        //倒放切换为正放
  NET_DVR_PLAY_REVERSE = 30;        //正放切换为倒放
  NET_DVR_SET_TRANS_TYPE = 32;      //设置转封装类型
  NET_DVR_PLAY_CONVERT = 33;        //回放转码
  NET_DVR_START_DRAWFRAME = 34;     //开始抽帧回放
  NET_DVR_STOP_DRAWFRAME = 35;      //停止抽帧回放
  NET_DVR_CHANGEWNDRESOLUTION = 36; //窗口大小改变时下发通知给播放库(仅Linux版本适用)
  NET_DVR_VOD_DRAG_ING = 37;        //拖动中
  NET_DVR_VOD_DRAG_END = 38;        //拖动结束

{-------- 控制命令 ------------------------------------------------------------}
  NET_DVR_CMD_TRIGGER_PERIOD_RECORD = 6144; //外部命令触发录像，对应“命令触发”录像类型

{======== 枚举定义 ============================================================}

type
  {-------- 抓图模式 --------}
  CAPTURE_MODE = ( {$REGION ''}
    BMP_MODE = 0, //BMP模式
    JPEG_MODE = 1 //JPRG模式
    {$ENDREGION} );

  {-------- 车牌类型 --------}
  VCA_PLATE_TYPE = ( {$REGION ''}
    VCA_STANDARD92_PLATE = 0, //标准民用车与军车车牌
    VCA_STANDARD02_PLATE,     //02式民用车牌
    VCA_WJPOLICE_PLATE,       //武警车车牌
    VCA_JINGCHE_PLATE,        //警车车牌
    STANDARD92_BACK_PLATE,    //民用车双行尾牌
    VCA_SHIGUAN_PLATE,        //使馆车牌
    VCA_NONGYONG_PLATE,       //农用车车牌
    VCA_MOTO_PLATE,           //摩托车车牌
    NEW_ENERGY_PLATE          //新能源车牌
    {$ENDREGION} );

  {-------- 车牌颜色 --------}
  VCA_PLATE_COLOR = ( {$REGION ''}
    VCA_BLUE_PLATE = 0, //蓝色车牌
    VCA_YELLOW_PLATE,   //黄色车牌
    VCA_WHITE_PLATE,    //白色车牌
    VCA_BLACK_PLATE,    //黑色车牌
    VCA_GREEN_PLATE,    //绿色车牌
    VCA_BKAIR_PLATE,    //民航黑色车牌
    VCA_OTHER = $ff     //其他
    {$ENDREGION} );

  {-------- 国家索引值 --------}
  COUNTRY_INDEX = ( {$REGION ''}
    COUNTRY_NONSUPPORT = 0,     //算法库不支持牌识国家
    (* 欧洲，48个国家和地区，其中塞浦路斯(Cyprus)属于亚欧两洲交界国家 *)
    COUNTRY_CZE = 1,            //Czech Republic 捷克共和国
    COUNTRY_FRA = 2,            //France 法国
    COUNTRY_DEU = 3,            //Germany 德国
    COUNTRY_ESP = 4,            //Spain  西班牙
    COUNTRY_ITA = 5,            //Italy  意大利
    COUNTRY_NLD = 6,            //Netherlands 荷兰
    COUNTRY_POL = 7,            //Poland  波兰
    COUNTRY_SVK = 8,            //Slovakia  斯洛伐克
    COUNTRY_BLR = 9,            //Belorussia  白俄罗斯
    COUNTRY_MDA = 10,           //Moldova  摩尔多瓦
    COUNTRY_RUS = 11,           //Russia  俄罗斯
    COUNTRY_UKR = 12,           //Ukraine  乌克兰
    COUNTRY_BEL = 13,           //Belgium  比利时
    COUNTRY_BGR = 14,           //Bulgaria  保加利亚
    COUNTRY_DNK = 15,           //Denmark  丹麦
    COUNTRY_FIN = 16,           //Finland  芬兰
    COUNTRY_GBR = 17,           //Great Britain  英国
    COUNTRY_GRC = 18,           //Greece  希腊
    COUNTRY_HRV = 19,           //Croatia  克罗地亚
    COUNTRY_HUN = 20,           //Hungary  匈牙利
    COUNTRY_ISR = 21,           //Israel  以色列
    COUNTRY_LUX = 22,           //Luxembourg  卢森堡
    COUNTRY_MKD = 23,           //Macedonia  马其顿共和国
    COUNTRY_NOR = 24,           //Norway  挪威
    COUNTRY_PRT = 25,           //Portugal  葡萄牙
    COUNTRY_ROU = 26,           //Romania  多马尼亚
    COUNTRY_SRB = 27,           //Serbia  塞尔维亚
    COUNTRY_AZE = 28,           //Azerbaijan 阿塞邦疆共和国
    COUNTRY_GEO = 29,           //Georgia  即格鲁吉亚
    COUNTRY_KAZ = 30,           //Kazakhstan 哈萨克斯坦
    COUNTRY_LTU = 31,           //Lithuania 立陶宛共和国
    COUNTRY_TKM = 32,           //Turkmenistan 土库曼斯坦
    COUNTRY_UZB = 33,           //Uzbekistan 乌兹别克斯坦
    COUNTRY_LVA = 34,           //Latvia 拉脱维亚
    COUNTRY_EST = 35,           //Estonia 爱沙尼亚
    COUNTRY_ALB = 36,           //Albania 阿尔巴尼亚
    COUNTRY_AUT = 37,           //Austria 奥地利
    COUNTRY_BIH = 38,           //Bosnia and Herzegovina 波斯尼亚和黑塞哥维那
    COUNTRY_IRL = 39,           //Ireland 爱尔兰
    COUNTRY_ISL = 40,           //Iceland 冰岛
    COUNTRY_VAT = 41,           //Vatican 梵蒂冈
    COUNTRY_MLT = 42,           //Malta 马耳他
    COUNTRY_SWE = 43,           //Sweden 瑞典
    COUNTRY_CHE = 44,           //Switzerland 瑞士
    COUNTRY_CYP = 45,           //Cyprus 塞浦路斯
    COUNTRY_TUR = 46,           //Turkey 土耳其
    COUNTRY_SVN = 47,           //Slovenia 斯洛文尼亚
    COUNTRY_MTG = 48,           //Montenegro 黑山共和国
    COUNTRY_KOV = 49,           //Kosovo 科索沃
    COUNTRY_ADR = 50,           //Andorra 安道尔
    COUNTRY_ARM = 51,           //Armenia 亚美尼亚(亚洲)
    COUNTRY_MON = 52,           //Monaco 摩纳哥
    COUNTRY_LIE = 53,           //Liechtenstein 列支敦士登
    COUNTRY_SMO = 54,           //San Marino 圣马力诺
    COUNTRY_RES1 = 55,          //保留字段
    COUNTRY_RES2 = 56,          //保留字段
    COUNTRY_RES3 = 57,          //保留字段
    COUNTRY_RES4 = 58,          //保留字段
    (* 亚洲 48个国家 其中 塞浦路斯(Cyprus)属于亚欧两洲交界国家 *)
    COUNTRY_CHI = 59,           //China 中国
    COUNTRY_IBN = 60,           //In bahrain 巴林
    COUNTRY_SKR = 61,           //South Korea 韩国
    COUNTRY_LEB = 62,           //Lebanon 黎巴嫩
    COUNTRY_NEP = 63,           //Nepal 尼泊尔
    COUNTRY_THA = 64,           //Thailand 泰国
    COUNTRY_PAK = 65,           //Pakistan 巴基斯坦
    COUNTRY_EMI = 66,           //The united Arab emirates 阿拉伯联合酋长国
    COUNTRY_BHU = 67,           //Bhutan 不丹
    COUNTRY_OMA = 68,           //Oman 阿曼
    COUNTRY_KOR = 69,           //North Korea 朝鲜
    COUNTRY_PHI = 70,           //The Philippines 菲律宾
    COUNTRY_CAM = 71,           //Cambodia 柬埔寨
    COUNTRY_QAT = 72,           //Qatar 卡塔尔
    COUNTRY_KYR = 73,           //Kyrgyzstan 吉尔吉斯斯坦
    COUNTRY_MAL = 74,           //The maldives 马尔代夫
    COUNTRY_MLY = 75,           //Malaysia 马来西亚
    COUNTRY_MOG = 76,           //Mongolia 蒙古
    COUNTRY_ARA = 77,           //Saudi Arabia 沙特阿拉伯
    COUNTRY_BRU = 78,           //brunei 文莱
    COUNTRY_LAO = 79,           //Laos 老挝
    COUNTRY_JAP = 80,           //Japan 日本
    COUNTRY_RES19 = 81,         //保留字段
    COUNTRY_PAS = 82,           //Palestinian state 巴勒斯坦国
    COUNTRY_TAJ = 83,           //Tajikistan 塔吉克斯坦
    COUNTRY_KUW = 84,           //Kuwait 科威特
    COUNTRY_SYR = 85,           //Syria 叙利亚
    COUNTRY_IND = 86,           //India 印度
    COUNTRY_ISA = 87,           //Indonesia 印度尼西亚
    COUNTRY_AFG = 88,           //Afghanistan 阿富汗
    COUNTRY_LAN = 89,           //Sri Lanka 斯里兰卡
    COUNTRY_IRQ = 90,           //Iraq 伊拉克
    COUNTRY_VIE = 91,           //Vietnam 越南
    COUNTRY_IRA = 92,           //Iran 伊朗
    COUNTRY_YEM = 93,           //yemen 也门
    COUNTRY_JOR = 94,           //Jordan 约旦
    COUNTRY_BUR = 95,           //Burma 缅甸
    COUNTRY_SIK = 96,           //Sikkim 锡金
    COUNTRY_BAN = 97,           //Bangladesh 孟加拉国
    COUNTRY_SGA = 98,           //Singapore 新加坡
    COUNTRY_EAT = 99,           //East timor 东帝汶(已宣布独立，但世界上许多国家不承认)
    COUNTRY_RES5 = 100,         //保留字段
    COUNTRY_RES6 = 101,         //保留字段
    COUNTRY_RES7 = 102,         //保留字段
    COUNTRY_RES8 = 103,         //保留字段
    (* 非洲 60个国家和地区 *)
    COUNTRY_EGT = 104,          //Egypt 埃及
    COUNTRY_LIY = 105,          //Libya 利比亚
    COUNTRY_SUA = 106,          //Sudan 苏丹
    COUNTRY_TUN = 107,          //Tunisia 突尼斯
    COUNTRY_ALG = 108,          //Algeria 阿尔及利亚
    COUNTRY_MCC = 109,          //Morocco 摩洛哥
    COUNTRY_ETH = 110,          //Ethiopia 埃塞俄比亚
    COUNTRY_ERI = 111,          //Eritrea 厄立特里亚
    COUNTRY_SDE = 112,          //Somalia Democratic 索马里
    COUNTRY_DJI = 113,          //Djibouti 吉布提
    COUNTRY_KEN = 114,          //Kenya 肯尼亚
    COUNTRY_TAI = 115,          //Tanzania 坦桑尼亚
    COUNTRY_UGA = 116,          //Uganda 乌干达
    COUNTRY_RWA = 117,          //Rwanda 卢旺达
    COUNTRY_BUD = 118,          //Burundi 布隆迪
    COUNTRY_SEY = 119,          //Seychelles 塞舌尔
    COUNTRY_CHA = 120,          //Chad 乍得
    COUNTRY_CEA = 121,          //Central African 中非
    COUNTRY_CON = 122,          //Cameroon 喀麦隆
    COUNTRY_EQG = 123,          //Equatorial Guinea赤道几内亚
    COUNTRY_GAB = 124,          //Gabon 加蓬
    COUNTRY_TCO = 125,          //the Congo 刚果共和国(即：刚果(布))
    COUNTRY_DRC = 126,          //Democratic Republic of the Congo 刚果民主共和国(即：刚果(金))
    COUNTRY_STP = 127,          //Sao Tome and Principe 圣多美和普林西比
    COUNTRY_MAN = 128,          //Mauritania 毛里塔尼亚
    COUNTRY_WSA = 129,          //Western Sahara 西撒哈拉(撒拉威)
    COUNTRY_SEL = 130,          //Senegal 塞内加尔
    COUNTRY_TGA = 131,          //the Gambia 冈比亚
    COUNTRY_MAI = 132,          //Mali 马里
    COUNTRY_BUF = 133,          //Burkina Faso 布基纳法索
    COUNTRY_GUI = 134,          //Guinea 几内亚
    COUNTRY_GUB = 135,          //Guinea-Bissau 几内亚比绍
    COUNTRY_CAV = 136,          //Cape Verde 佛得角
    COUNTRY_SLE = 137,          //Sierra Leone 塞拉利昂
    COUNTRY_LIR = 138,          //Liberia 利比里亚
    COUNTRY_IVC = 139,          //Ivory Coast 科特迪瓦
    COUNTRY_GHA = 140,          //Ghana 加纳
    COUNTRY_TGO = 141,          //Togo 多哥
    COUNTRY_BEN = 142,          //Benin 贝宁
    COUNTRY_NIG = 143,          //Niger 尼日尔
    COUNTRY_ZAB = 144,          //Zambia 赞比亚
    COUNTRY_ANG = 145,          //Angola 安哥拉
    COUNTRY_ZBE = 146,          //Zimbabwe 津巴布韦
    COUNTRY_MAW = 147,          //Malawi 马拉维
    COUNTRY_MOQ = 148,          //Mozambique 莫桑比克
    COUNTRY_BOT = 149,          //Botswana 博茨瓦纳
    COUNTRY_NAM = 150,          //Namibia 纳米比亚
    COUNTRY_SAF = 151,          //South Africa 南非
    COUNTRY_SWD = 152,          //Swaziland 斯威士兰
    COUNTRY_LES = 153,          //Lesotho 莱索托
    COUNTRY_MAG = 154,          //Madagasca 马达加斯加
    COUNTRY_UOC = 155,          //Union of Comoros 科摩罗
    COUNTRY_MAT = 156,          //Mauritius 毛里求斯
    COUNTRY_NGE = 157,          //Nigeria 尼日利亚
    COUNTRY_SSD = 158,          //South Sudan 南苏丹
    COUNTRY_SAH = 159,          //Saint Helena 圣赫勒拿(英国海外省)
    COUNTRY_MYT = 160,          //Mayotte 马约特(法国海外省)
    COUNTRY_REN = 161,          //Reunion 留尼旺(法国海外省)
    COUNTRY_CAI = 162,          //Canary Islands 加那利群岛(西班牙海外领土)
    COUNTRY_AZO = 163,          //AZORES 亚速尔群岛(北大西洋非洲西海岸(葡萄牙))
    COUNTRY_MAD = 164,          //Madeira 马德拉群岛(北大西洋非洲西海岸(葡萄牙))
    COUNTRY_RES9 = 165,         //保留字段
    COUNTRY_RES10 = 166,        //保留字段
    COUNTRY_RES11 = 167,        //保留字段
    COUNTRY_RES12 = 168,        //保留字段
    (* 美洲 55个国家和地区 *)
    COUNTRY_CAD = 169,          //Canada 加拿大
    COUNTRY_GRE = 170,          //Greenland Nuuk 格陵兰(丹麦王国的海外自治领土)
    COUNTRY_PIE = 171,          //Pierre and Miquelon 圣皮埃尔和密克隆(法国海外领土)
    COUNTRY_USA = 172,          //United States 美国
    COUNTRY_BER = 173,          //Bermuda 百慕大(英国海外领土)
    COUNTRY_MEX = 174,          //Mexico 墨西哥
    COUNTRY_GUA = 175,          //Guatemala 危地马拉
    COUNTRY_BLI = 176,          //Belize 伯利兹
    COUNTRY_SAR = 177,          //El Salvador 萨尔瓦多
    COUNTRY_HOR = 178,          //Honduras 洪都拉斯
    COUNTRY_NIC = 179,          //Nicaragua 尼加拉瓜
    COUNTRY_COR = 180,          //Costa Rica 哥斯达黎加
    COUNTRY_PAN = 181,          //Panama 巴拿马
    COUNTRY_TBM = 182,          //The Bahamas 巴哈马
    COUNTRY_TCI = 183,          //The Turks and Caicos Islands 特克斯群岛和凯科斯群岛(英国海外领土)
    COUNTRY_CUB = 184,          //Cuba 古巴
    COUNTRY_JAM = 185,          //Jamaica 牙买加
    COUNTRY_CAY = 186,          //Cayman Islands 开曼群岛(英国海外领土)
    COUNTRY_HAT = 187,          //Haiti 海地
    COUNTRY_TDO = 188,          //The Dominican 多米尼加
    COUNTRY_PUR = 189,          //Puerto Rico 波多黎各(美国海外领土)
    COUNTRY_VIL = 190,          //The United States Virgin Islands美属维尔京群岛
    COUNTRY_BVI = 191,          //The British Virgin Islands 英属维尔京群岛
    COUNTRY_ATV = 192,          //Anguilla The Valley 安圭拉(英国海外领土)
    COUNTRY_ANB = 193,          //Antigua and Barbuda 安提瓜和巴布达
    COUNTRY_CSM = 194,          //Collectivité de Saint-Martin 法属圣马丁(法国海外领土)
    COUNTRY_ACY = 195,          //Autonomous country 荷属圣马丁(荷兰王国的自治国 )
    COUNTRY_SBY = 196,          //Saint-Barthélemy 圣巴泰勒米岛(法国海外领土)
    COUNTRY_SKN = 197,          //Saint Kitts and Nevis 圣基茨和尼维斯
    COUNTRY_MOT = 198,          //Montserrat 蒙特塞拉特(英国海外领土)
    COUNTRY_GLP = 199,          //Guadeloupe 瓜德罗普(法国海外领土)
    COUNTRY_DOM = 200,          //Dominica 多米尼克
    COUNTRY_MTE = 201,          //Martinique 马提尼克(法国海外领土)
    COUNTRY_LUC = 202,          //St. Lucia 圣卢西亚
    COUNTRY_SVG = 203,          //Saint Vincent and the Grenadines 圣文森特和格林纳丁斯
    COUNTRY_GRD = 204,          //Grenada 格林纳达
    COUNTRY_BAR = 205,          //Barbados 巴巴多斯
    COUNTRY_TRT = 206,          //Trinidad and Tobago 特立尼达和多巴哥
    COUNTRY_CUR = 207,          //Cura?ao 库拉索(荷兰王国的一个自治国)
    COUNTRY_ARB = 208,          //Aruba 阿鲁巴(荷兰王国的一个自治国)
    COUNTRY_NEA = 209,          //Netherlands Antilles 荷属安的列斯
    COUNTRY_COL = 210,          //Colombia 哥伦比亚
    COUNTRY_VEN = 211,          //Venezuela 委内瑞拉
    COUNTRY_GUY = 212,          //Guyana 圭亚那
    COUNTRY_SUR = 213,          //Suriname 苏里南
    COUNTRY_FRN = 214,          //Guyane Francaise 法属圭亚那
    COUNTRY_ECU = 215,          //Ecuador 厄瓜多尔
    COUNTRY_PER = 216,          //Peru 秘鲁
    COUNTRY_BOL = 217,          //Bolivia 玻利维亚
    COUNTRY_PAR = 218,          //Paraguay 巴拉圭
    COUNTRY_CLE = 219,          //Chile 智利
    COUNTRY_BRA = 220,          //Brazil 巴西
    COUNTRY_UGY = 221,          //Uruguay 乌拉圭
    COUNTRY_ARG = 222,          //Argentina 阿根廷
    COUNTRY_RES13 = 223,        //保留字段
    COUNTRY_RES14 = 224,        //保留字段
    COUNTRY_RES15 = 225,        //保留字段
    COUNTRY_RES16 = 226,        //保留字段
    (* 大洋洲 25个国家和地区 *)
    COUNTRY_ATN = 227,          //Australien 澳大利亚
    COUNTRY_NED = 228,          //Neuseeland 新西兰
    COUNTRY_PNG = 229,          //Papua New Guinea 巴布亚新几内亚
    COUNTRY_SAN = 230,          //Salomonen 所罗门群岛
    COUNTRY_VAU = 231,          //Vanuatu 瓦努阿图
    COUNTRY_NCN = 232,          //New Caledonia 新喀里多尼亚(法国的海外属地)
    COUNTRY_PAU = 233,          //Palau 帕劳
    COUNTRY_FSM = 234,          //Federated States of Micronesia 密克罗尼西亚联邦
    COUNTRY_MRI = 235,          //Marshall Island  马绍尔群岛
    COUNTRY_CNM = 236,          //Commonwealth of the Northern Mariana Islands 北马里亚纳群岛(美国的海外属地)
    COUNTRY_TEG = 237,          //The Territory of Guahan 关岛(美国的海外属地)
    COUNTRY_NUR = 238,          //Nauru 瑙鲁
    COUNTRY_KIB = 239,          //Kiribati 基里巴斯
    COUNTRY_FID = 240,          //Fidschi 斐济群岛
    COUNTRY_TNG = 241,          //Tonga 汤加
    COUNTRY_TUV = 242,          //Tuvalu 图瓦卢
    COUNTRY_WEF = 243,          //Wallis et Futuna  瓦利斯和富图纳(法国的海外属地)
    COUNTRY_TIS = 244,          //The Independent State of Samoa 萨摩亚
    COUNTRY_EAS = 245,          //Eastern Samoa 美属萨摩亚
    COUNTRY_TOE = 246,          //Tokelau 托克劳(新西兰)
    COUNTRY_NUE = 247,          //Niue 纽埃(新西兰)
    COUNTRY_TCD = 248,          //The Cook Islands 库克群岛(新西兰)
    COUNTRY_PFP = 249,          //Polynésie fran?aiseFrench Polynesia 法属波利尼西亚
    COUNTRY_PID = 250,          //Pitcairn Islands 皮特凯恩群岛(英国的海外属地)
    COUNTRY_HAW = 251,          //Hawaii State 夏威夷(美国的海外属地)
    COUNTRY_RES17 = 252,        //保留字段
    COUNTRY_RES18 = 253,        //保留字段
    COUNTRY_UNRECOGNIZED = $fe, //Unrecognized 无法识别
    COUNTRY_ALL = $ff           //ALL 全部
    {$ENDREGION});

  {-------- 汽车品牌 --------}
  VLR_VEHICLE_CLASS = ( {$REGION ''}
    VLR_OTHER = 0,              //其它
    VLR_VOLKSWAGEN = 1,         //大众
    VLR_BUICK = 2,              //别克
    VLR_BMW = 3,                //宝马
    VLR_HONDA = 4,              //本田
    VLR_PEUGEOT = 5,            //标致
    VLR_TOYOTA = 6,             //丰田
    VLR_FORD = 7,               //福特
    VLR_NISSAN = 8,             //日产
    VLR_AUDI = 9,               //奥迪
    VLR_MAZDA = 10,             //马自达
    VLR_CHEVROLET = 11,         //雪佛兰
    VLR_CITROEN = 12,           //雪铁龙
    VLR_HYUNDAI = 13,           //现代
    VLR_CHERY = 14,             //奇瑞
    VLR_KIA = 15,               //起亚
    VLR_ROEWE = 16,             //荣威
    VLR_MITSUBISHI = 17,        //三菱
    VLR_SKODA = 18,             //斯柯达
    VLR_GEELY = 19,             //吉利
    VLR_ZHONGHUA = 20,          //中华
    VLR_VOLVO = 21,             //沃尔沃
    VLR_LEXUS = 22,             //雷克萨斯
    VLR_FIAT = 23,              //菲亚特
    VLR_EMGRAND = 24,           //帝豪
    VLR_DONGFENG = 25,          //东风
    VLR_BYD = 26,               //比亚迪
    VLR_SUZUKI = 27,            //铃木
    VLR_JINBEI = 28,            //金杯
    VLR_HAIMA = 29,             //海马
    VLR_SGMW = 30,              //五菱
    VLR_JAC = 31,               //江淮
    VLR_SUBARU = 32,            //斯巴鲁
    VLR_ENGLON = 33,            //英伦
    VLR_GREATWALL = 34,         //长城
    VLR_HAFEI = 35,             //哈飞
    VLR_ISUZU = 36,             //五十铃
    VLR_SOUEAST = 37,           //东南
    VLR_CHANA = 38,             //长安
    VLR_FOTON = 39,             //福田
    VLR_XIALI = 40,             //夏利
    VLR_BENZ = 41,              //奔驰
    VLR_FAW = 42,               //一汽
    VLR_NAVECO = 43,            //依维柯
    VLR_LIFAN = 44,             //力帆
    VLR_BESTURN = 45,           //一汽奔腾
    VLR_CROWN = 46,             //皇冠
    VLR_RENAULT = 47,           //雷诺
    VLR_JMC = 48,               //JMC
    VLR_MG = 49,                //MG名爵
    VLR_KAMA = 50,              //凯马
    VLR_ZOTYE = 51,             //众泰
    VLR_CHANGHE = 52,           //昌河
    VLR_XMKINGLONG = 53,        //厦门金龙
    VLR_HUIZHONG = 54,          //上海汇众
    VLR_SZKINGLONG = 55,        //苏州金龙
    VLR_HIGER = 56,             //海格
    VLR_YUTONG = 57,            //宇通
    VLR_CNHTC = 58,             //中国重汽
    VLR_BEIBEN = 59,            //北奔重卡
    VLR_XINGMA = 60,            //华菱星马
    VLR_YUEJIN = 61,            //跃进
    VLR_HUANGHAI = 62,          //黄海
    VLR_OLDWALL = 63,           //老款长城
    VLR_CHANACOMMERCIAL = 64,   //长安商用
    VLR_PORSCHE = 65,           //保时捷
    VLR_CADILLAC = 66,          //凯迪拉克
    VLR_INFINITI = 67,          //英菲尼迪
    VLR_GLEAGLE = 68,           //吉利全球鹰
    VLR_JEEP = 69,              //JEEP
    VLR_LANDROVER = 70,         //路虎
    VLR_CHANGFENG = 71,         //长丰
    VLR_BENNI = 72,             //长安奔奔
    VLR_ERA = 73,               //福田时代
    VLR_TAURUS = 74,            //长安金牛星
    VLR_EADO = 75,              //长安逸动
    VLR_SHANQI = 76,            //陕汽
    VLR_HONGYAN = 77,           //红岩汽车
    VLR_DRAGON = 78,            //霸龙汽车
    VLR_JACTEXT = 79,           //江淮JAC
    VLR_JACBUS = 80,            //江淮现代客车
    VLR_ANKAI = 81,             //安凯客车
    VLR_SHENLONG = 82,          //申龙客车
    VLR_DAEWOO = 83,            //大宇客车
    VLR_WUZHENG = 84,           //五征汽车
    VLR_MAN = 85,               //MAN汽车
    VLR_ZHONGTONG = 86,         //中通客车
    VLR_BAOJUN = 87,            //宝骏
    VLR_BQWEIWANG = 88,         //北汽威旺
    VLR_TRUMPCHE = 89,          //传祺
    VLR_LUFENG = 90,            //陆风
    VLR_HMZHENGZHOU = 91,       //海马郑州
    VLR_BEIJING = 92,           //北京汽车
    VLR_ZHONGSHUN = 93,         //中顺
    VLR_WEILIN = 94,            //威麟汽车
    VLR_OPEL = 95,              //欧宝
    VLR_KARRY = 96,             //开瑞
    VLR_SMA = 97,               //华普汽车
    VLR_SMATEXT = 98,           //华普汽车文字SMA
    VLR_YUWIN = 99,             //江铃驭胜
    VLR_MINI = 100,             //宝马MINI
    VLR_MARK = 101,             //丰田MARKX
    VLR_HAVAL = 102,            //哈弗HAVAL
    VLR_OGA = 103,              //讴歌
    VLR_VENUCIA = 104,          //启辰
    VLR_BYD2 = 105,             //比亚迪样式2
    VLR_SMART = 106,            //奔驰SMART
    VLR_BAW = 107,              //北京汽车制造厂BAW
    VLR_LUXGEN = 108,           //纳智捷
    VLR_YEMA = 109,             //野马
    VLR_ZTE = 110,              //中兴
    VLR_EVERUS = 111,           //理念
    VLR_CHRYSLER = 112,         //克莱斯勒
    VLR_GONOW = 113,            //吉奥汽车
    VLR_SHJIANG = 114,          //松花江
    VLR_RUILIN = 115,           //瑞麟
    VLR_FORTA = 116,            //福达
    VLR_GAGUAR = 117,           //捷豹
    VLR_HEIBAO = 118,           //黑豹
    VLR_TKING = 119,            //唐骏
    VLR_TKINGTEXT = 120,        //唐骏文字
    VLR_FODAY = 121,            //福迪
    VLR_LOTUS = 122,            //莲花汽车
    VLR_NANJUN = 123,           //南骏
    VLR_SHUANGHUAN = 124,       //双环汽车
    VLR_SAIBAO = 125,           //哈飞赛豹
    VLR_HAWTAI = 126,           //华泰
    VLR_LIFO = 127,             //永源飞碟
    VLR_JONWAY = 128,           //永源汽车
    VLR_FULONGMA = 129,         //福龙马
    VLR_WEILI = 130,            //潍力
    VLR_ANCHI = 131,            //江淮安驰
    VLR_PAIXI = 132,            //派喜
    VLR_HIGERTEXT = 133,        //海格HIGER文字
    VLR_RIYECAR = 134,          //广汽日野轻卡
    VLR_RIYETRUCK = 135,        //广汽日野重卡
    VLR_JIANGNAN = 136,         //江南
    VLR_OLDZOTYE = 137,         //老款众泰
    VLR_OLDXIALI = 138,         //老款夏利
    VLR_NEWAOCHI = 139,         //新奥驰
    VLR_CDW = 140,              //王牌重汽
    VLR_CDWTEXT = 141,          //王牌重汽文字
    VLR_CIIMO = 142,            //本田思铭
    VLR_CHANADS = 143,          //长安谛艾仕
    VLR_DS = 144,               //道奇
    VLR_ROHENS = 145,           //现代劳恩斯酷派
    VLR_YANTAI = 146,           //燕台
    VLR_SHUANGLONG = 147,       //双龙
    VLR_FENGLING = 148,         //时代风菱
    VLR_XINKAI = 149,           //新凯
    VLR_GMC = 150,              //GMC
    VLR_DATONG = 151,           //上汽大通
    VLR_BQYINXIANG = 152,       //北汽银翔
    VLR_NEWCHERY = 153,         //新奇瑞
    VLR_MUDAN = 154,            //牡丹
    VLR_DAYUN = 155,            //大运汽车
    VLR_DONGWO = 156,           //东沃汽车
    VLR_UNION = 157,            //联合汽车
    VLR_CHUNZHOU = 158,         //春洲客车
    VLR_SANY = 159,             //三一重工
    VLR_YAXING = 160,           //亚星客车
    VLR_HENGTONG = 161,         //恒通客车
    VLR_SHAOLIN = 162,          //少林客车
    VLR_YOUNG = 163,            //青年客车
    VLR_STOM = 164,             //十通
    VLR_SANHUAN = 165,          //三环
    VLR_XUGONG = 166,           //徐工
    VLR_BEIFANG = 167,          //北方汽车
    VLR_JIANGHUAN = 168,        //江环货车
    VLR_BJFARM = 169,           //北京农用
    VLR_NEWDADI = 170,          //新大地汽车
    VLR_SUNWIN = 171,           //申沃客车
    VLR_YINGTIAN = 172,         //英田
    VLR_QINGQI = 173,           //轻骑
    VLR_CHUFENG = 174,          //楚风汽车
    VLR_SCANIA = 175,           //斯堪尼亚
    VLR_JIULONG = 176,          //九龙客车
    VLR_YOUYI = 177,            //友谊客车
    VLR_SHANGRAO = 178,         //上饶客车
    VLR_JIJIANG = 179,          //吉江
    VLR_YANGZI = 180,           //扬子客车
    VLR_XIWO = 181,             //西沃客车
    VLR_CHANGJIANG = 182,       //长江客车
    VLR_WUYI = 183,             //武夷
    VLR_CHENGDU = 184,          //成都客车
    VLR_TIANMA = 185,           //天马
    VLR_BAOLONG = 186,          //宝龙
    VLR_NEWYATU = 187,          //新雅途
    VLR_BARUI = 188,            //起亚霸锐
    VLR_GUANZHI = 189,          //观致
    VLR_XIYATE = 190,           //西雅特
    VLR_BINLI = 191,            //宾利
    VLR_DADI = 192,             //大迪
    VLR_FUQI = 193,             //富奇
    VLR_HANGTIAN = 194,         //航天汽车
    VLR_HENGTIAN = 195,         //恒天汽车
    VLR_JMCCAR = 196,           //江铃轻汽
    VLR_KAERSEN = 197,          //卡尔森汽车
    VLR_KAWEI = 198,            //卡威汽车
    VLR_LANBOJINI = 199,        //兰博基尼
    VLR_MASHALADI = 200,        //玛莎拉蒂
    VLR_SHUCHI = 201,           //舒驰客车
    VLR_SHILI = 202,            //实力客车
    VLR_HUABEI = 203,           //中客华北
    VLR_YIZHENG = 204,          //上汽仪征
    VLR_CHUNLAN = 205,          //春兰汽车
    VLR_DAFA = 206,             //大发汽车
    VLR_SHENYE = 207,           //神野汽车
    VLR_FALALI = 208,           //法拉利汽车
    VLR_FUXING = 209,           //福星汽车
    VLR_ANYUAN = 210,           //安源客车
    VLR_JINGHUA = 211,          //京华客车
    VLR_TAIHU = 212,            //太湖客车
    VLR_WUZHOULONG = 213,       //五洲龙
    VLR_CHANGLONG = 214,        //常隆客车
    VLR_YUEXI = 215,            //悦西客车
    VLR_SHENMA = 216,           //神马客车
    VLR_LUSHAN = 217,           //庐山牌
    VLR_WANFENG = 218,          //万丰牌
    VLR_GZYUNBAO = 219,         //广州云豹
    VLR_ZHONGDA = 220,          //中大汽车
    VLR_THREEWHEEL = 221,       //三轮车
    VLR_TWOWHEEL = 222,         //二轮车
    VLR_JBC = 223,              //金杯JBC
    VLR_YZJIANG = 224,          //扬子江客车
    VLR_CNJ = 225,              //南骏CNJ
    VLR_FORLAND = 226,          //福田时代文字
    VLR_FARMCAR = 227,          //农用车
    VLR_DONGFANGHONG = 228,     //东方红
    VLR_STEYR = 229,            //斯太尔汽车
    VLR_HONGQI = 230            //红旗
    {$ENDREGION} );

  {-------- 车辆类型 --------}
  VTR_RESULT = ( {$REGION ''}
    VTR_RESULT_OTHER = 0,               //未知
    VTR_RESULT_BUS = 1,                 //客车
    VTR_RESULT_TRUCK = 2,               //货车
    VTR_RESULT_CAR = 3,                 //轿车
    VTR_RESULT_MINIBUS = 4,             //面包车
    VTR_RESULT_SMALLTRUCK = 5,	        //小货车
    VTR_RESULT_HUMAN = 6,	              //行人
    VTR_RESULT_TUMBREL = 7,             //二轮车
    VTR_RESULT_TRIKE = 8,               //三轮车
    VTR_RESULT_SUV_MPV = 9,             //SUV/MPV
    VTR_RESULT_MEDIUM_BUS = 10,         //中型客车
    VTR_RESULT_MOTOR_VEHICLE = 11,      //机动车
    VTR_RESULT_NON_MOTOR_VEHICLE = 12,  //非机动车
    VTR_RESULT_SMALLCAR = 13,           //小型轿车
    VTR_RESULT_MICROCAR = 14,           //微型轿车
    VTR_RESULT_PICKUP = 15,             //皮卡车
    VTR_RESULT_CONTAINER_TRUCK = 16,    //集装箱卡车
    VTR_RESULT_MINI_TRUCK = 17,         //微卡，栏板卡
    VTR_RESULT_SLAG_CAR = 18,           //渣土车
    VTR_RESULT_CRANE = 19,              //吊车，工程车
    VTR_RESULT_OIL_TANK_TRUCK = 20,     //油罐车
    VTR_RESULT_CONCRETE_MIXER = 21,     //混凝土搅拌车
    VTR_RESULT_PLATFORM_TRAILER = 22,   //平板拖车
    VTR_RESULT_HATCHBACK = 23,          //两厢轿车
    VTR_RESULT_SALOON = 24,             //三厢轿车
    VTR_RESULT_SPORT_SEDAN = 25         //轿跑
    {$ENDREGION} );

  {-------- 关联车道方向 --------}
  ITC_RELA_LANE_DIRECTION_TYPE = ( {$REGION ''}
    ITC_RELA_LANE_DIRECTION_UNKNOW = 0,     //其它
    ITC_RELA_LANE_EAST_WEST = 1,            //从东向西
    ITC_RELA_LANE_WEST_EAST = 2,            //从西向东
    ITC_RELA_LANE_SOUTH_NORTH = 3,          //从南向北
    ITC_RELA_LANE_NORTH_SOUTH = 4,          //从北向南
    ITC_RELA_LANE_EASTSOUTH_WESTNORTH = 5,  //从东南向西北
    ITC_RELA_LANE_WESTNORTH_EASTSOUTH = 6,  //从西北向东南
    ITC_RELA_LANE_EASTNORTH_WESTSOUTH = 7,  //从东北向西南
    ITC_RELA_LANE_WESTSOUTH_EASTNORTH = 8   //从西南向东北
    {$ENDREGION} );

{======== 结构定义及回调函数声明 ==============================================}

type

{-------- 通用/基础 -----------------------------------------------------------}

/// <summary>接收异常消息的回调函数</summary>
/// <param name="dwType" IO="Out">异常或重连等消息的类型</param>
/// <param name="lUserID" IO="Out">登录ID</param>
/// <param name="lHandle" IO="Out">出现异常的相应类型的句柄</param>
/// <param name="pUser" IO="Out">用户数据</param>
  ExceptionCallBack = procedure(dwType: DWORD; lUserID, lHandle: LONG; pUser: PVOID); stdcall;

  {-------- 设备参数结构体 --------}
  NET_DVR_DEVICEINFO = packed record
    sSerialNumber: array[0..SERIALNO_LEN - 1] of Byte;  //序列号
    byAlarmInPortNum: Byte;                             //模拟报警输入个数
    byAlarmOutPortNum: Byte;                            //模拟报警输出个数
    byDiskNum: Byte;                                    //硬盘个数
    byDVRType: Byte;                                    //设备类型
    byChanNum: Byte;                                    //设备模拟通道个数
    byStartChan: Byte;                                  //起始通道号，目前设备通道号从1开始
  end;

  LPNET_DVR_DEVICEINFO = ^NET_DVR_DEVICEINFO;

  {-------- 设备参数结构体 --------}
  NET_DVR_DEVICEINFO_V30 = packed record
    sSerialNumber: array[0..SERIALNO_LEN - 1] of Byte;  //序列号
    byAlarmInPortNum: Byte;                             //模拟报警输入个数
    byAlarmOutPortNum: Byte;                            //模拟报警输出个数
    byDiskNum: Byte;                                    //硬盘个数
    byDVRType: Byte;                                    //设备类型
    byChanNum: Byte;                                    //设备模拟通道个数，数字(IP)通道最大个数为byIPChanNum+byHighDChanNum*256
    byStartChan: Byte;                                  //模拟通道的起始通道号，从1开始
    byAudioChanNum: Byte;                               //设备语音对讲通道数
    byIPChanNum: Byte;                                  //设备最大数字通道个数，低8位
    byZeroChanNum: Byte;                                //零通道编码个数
    byMainProto: Byte;                                  //主码流传输协议类型：0-private，1-rtsp，2-同时支持私有协议和rtsp协议取流(默认采用私有协议取流)
    bySubProto: Byte;                                   //子码流传输协议类型：0-private，1-rtsp，2-同时支持私有协议和rtsp协议取流(默认采用私有协议取流)
    bySupport: Byte;                                    //能力
    bySupport1: Byte;                                   //能力集扩充
    bySupport2: Byte;                                   //能力集扩充
    wDevType: Word;                                     //设备型号
    bySupport3: Byte;                                   //能力集扩展
    byMultiStreamProto: Byte;                           //是否支持多码流
    byStartDChan: Byte;                                 //起始数字通道号，0表示无数字通道，比如DVR或IPC
    byStartDTalkChan: Byte;                             //起始数字对讲通道号，区别于模拟对讲通道号，0表示无数字对讲通道
    byHighDChanNum: Byte;                               //数字通道个数，高8位
    bySupport4: Byte;                                   //能力集扩展
    byLanguageType: Byte;                               //支持语种能力
    byVoiceInChanNum: Byte;                             //音频输入通道数
    byStartVoiceInChanNo: Byte;                         //音频输入起始通道号，0表示无效
    byRes3: array[0..1] of Byte;                        //保留，置为0
    byMirrorChanNum: Byte;                              //镜像通道个数，录播主机中用于表示导播通道
    wStartMirrorChanNo: Word;                           //起始镜像通道号
    byRes2: array[0..1] of Byte;                        //保留，置为0
  end;

  LPNET_DVR_DEVICEINFO_V30 = ^NET_DVR_DEVICEINFO_V30;

/// <summary>登录状态回调函数</summary>
/// <param name="lUserID" IO="Out">用户ID，NET_DVR_Login_V40的返回值</param>
/// <param name="dwResult" IO="Out">登录状态：0-异步登录失败，1-异步登录成功</param>
/// <param name="lpDeviceInfo" IO="Out">设备信息，设备序列号、通道、能力等参数</param>
/// <param name="pUser" IO="Out">用户数据</param>
  LoginResultCallBack = procedure(lUserID: LONG; dwResult: DWORD; lpDeviceInfo: LPNET_DVR_DEVICEINFO_V30; pUser: PVOID); stdcall;

  {-------- 用户登录参数结构体 --------}
  NET_DVR_USER_LOGIN_INFO = packed record
    sDeviceAddress: array[0..NET_DVR_DEV_ADDRESS_MAX_LEN - 1] of AnsiChar;  //设备地址，IP 或者普通域名
    byUseTransport: Byte;                                                   //是否启用能力集透传：0-不启用透传，默认；1-启用透传
    wPort: Word;                                                            //设备端口号
    sUserName: array[0..NET_DVR_LOGIN_USERNAME_MAX_LEN - 1] of AnsiChar;    //登录用户名
    sPassword: array[0..NET_DVR_LOGIN_PASSWD_MAX_LEN - 1] of AnsiChar;      //登录密码
    cbLoginResult: LoginResultCallBack;                                     //登录状态回调函数，bUseAsynLogin 为1时有效
    pUser: PVOID;                                                         //用户数据
    bUseAsynLogin: BOOL;                                                    //是否异步登录：0-否，1-是
    byProxyType: Byte;                                                      //代理服务器类型：0-不使用代理，1-使用标准代理，2-使用EHome代理
    byUseUTCTime: Byte;                                                     //是否使用UTC时间：0-不进行转换，默认；1-输入输出UTC时间，SDK进行与设备时区的转换；2-输入输出平台本地时间，SDK进行与设备时区的转换
    byLoginMode: Byte;                                                      //登录模式：0-SDK私有协议，1-ISAPI协议，2-自适应(设备支持协议类型未知时使用，一般不建议)
    byHttps: Byte;                                                          //ISAPI协议登录时是否启用HTTPS(byLoginMode为1时有效)：0-不启用，1-启用，2-自适应(设备支持协议类型未知时使用，一般不建议)
    iProxyID: LONG;                                                         //代理服务器序号，添加代理服务器信息时相对应的服务器数组下标值
    byRes3: array[0..119] of Byte;                                          //保留，置为0
  end;

  LPNET_DVR_USER_LOGIN_INFO = ^NET_DVR_USER_LOGIN_INFO;

  {-------- 设备参数结构体 --------}
  NET_DVR_DEVICEINFO_V40 = packed record
    struDeviceV30: NET_DVR_DEVICEINFO_V30;  //设备参数
    bySupportLock: Byte;                    //设备是否支持锁定功能，为1时，dwSurplusLockTime和byRetryLoginTime有效
    byRetryLoginTime: Byte;                 //剩余可尝试登录的次数，用户名、密码错误时，此参数有效
    byPasswordLevel: Byte;                  //密码安全等级：0-无效，1-默认密码，2-有效密码，3-风险较高的密码，当管理员用户的密码为出厂默认密码(12345)或者风险较高的密码时，建议上层客户端提示用户更改密码
    byProxyType: Byte;                      //代理服务器类型：0-不使用代理，1-使用标准代理，2-使用EHome代理
    dwSurplusLockTime: DWORD;               //剩余时间，单位：秒，用户锁定时此参数有效
    byCharEncodeType: Byte;                 //字符编码类型：0-无字符编码信息(老设备)，1-GB2312，2-GBK，3-BIG5，4-Shift_JIS(日文)，5-EUC-KR(韩文)，6-UTF-8，7-ISO8859-1，8-ISO8859-2，9-ISO8859-3，…，依次类推，21-ISO8859-15(西欧)
    bySupportDev5: Byte;                    //支持v50版本的设备参数获取，设备名称和设备类型名称长度扩展为64字节
    byLoginMode: Byte;                      //登录模式：0-SDK私有协议，1-ISAPI协议
    byRes2: array[0..252] of Byte;          //保留，置为0
  end;

  LPNET_DVR_DEVICEINFO_V40 = ^NET_DVR_DEVICEINFO_V40;

  {-------- JPEG图像信息结构体 --------}
  NET_DVR_JPEGPARA = packed record
    wPicSize: Word;     //图片尺寸：$ff-Auto(使用当前码流分辨率)
    wPicQuality: Word;  //图片质量系数：0-最好，1-较好，2-一般
  end;

  LPNET_DVR_JPEGPARA = ^NET_DVR_JPEGPARA;

  {-------- 时间参数结构体 --------}
  NET_DVR_TIME = packed record
    dwYear: DWORD;       //年
    dwMonth: DWORD;      //月
    dwDay: DWORD;        //日
    dwHour: DWORD;       //时
    dwMinute: DWORD;     //分
    dwSecond: DWORD;     //秒
  end;

  LPNET_DVR_TIME = ^NET_DVR_TIME;

  {-------- 时间参数结构体 --------}
  NET_DVR_TIME_EX = packed record
    wYear: Word;                  //年
    byMonth: Byte;                //月
    byDay: Byte;                  //日
    byHour: Byte;                 //时
    byMinute: Byte;               //分
    bySecond: Byte;               //秒
    byRes: Byte;                  //保留
  end;

  LPNET_DVR_TIME_EX = ^NET_DVR_TIME_EX;

  {-------- 时间参数结构体 --------}
  NET_DVR_TIME_V30 = packed record
    wYear: Word;                  //年
    byMonth: Byte;                //月
    byDay: Byte;                  //日
    byHour: Byte;                 //时
    byMinute: Byte;               //分
    bySecond: Byte;               //秒
    byRes: Byte;                  //保留
    wMilliSec: Word;              //毫秒
    byRes1: array[0..1] of Byte;  //保留
  end;

  LPNET_DVR_TIME_V30 = ^NET_DVR_TIME_V30;

  {-------- IP地址结构体 --------}
  NET_DVR_IPADDR = packed record
    sIpV4: array[0..15] of AnsiChar;  //设备IPv4地址
    sIpV6: array[0..127] of Byte;     //设备IPv6地址
  end;

  LPNET_DVR_IPADDR = ^NET_DVR_IPADDR;

  {-------- 流ID信息结构体 --------}
  NET_DVR_STREAM_INFO = packed record
    dwSize: DWORD;                              //结构体大小
    byID: array[0..STREAM_ID_LEN - 1] of Byte;  //流ID，为字母、数字和"_"的组合。全部为0时，无效
    dwChannel: DWORD;                           //关联的设备通道
    byRes: array[0..31] of Byte;                //保留，置为0
  end;

  LPNET_DVR_STREAM_INFO = ^NET_DVR_STREAM_INFO;

{-------- 实时预览 ------------------------------------------------------------}

/// <summary>码流数据回调函数</summary>
/// <param name="lRealHandle" IO="Out">当前的预览句柄</param>
/// <param name="dwDataType" IO="Out">数据类型</param>
/// <param name="pBuffer" IO="Out">存放数据的缓冲区指针</param>
/// <param name="dwBufSize" IO="Out">缓冲区大小</param>
/// <param name="pUser" IO="Out">用户数据</param>
  RealDataCallBack_V30 = procedure(lRealHandle: LONG; dwDataType: DWORD; pBuffer: PByte; dwBufSize: DWORD; pUser: PVOID); stdcall;

/// <summary>码流数据回调函数</summary>
/// <param name="lRealHandle" IO="Out">当前的预览句柄，NET_DVR_RealPlay_V40的返回值</param>
/// <param name="dwDataType" IO="Out">数据类型</param>
/// <param name="pBuffer" IO="Out">存放数据的缓冲区指针</param>
/// <param name="dwBufSize" IO="Out">缓冲区大小</param>
/// <param name="pUser" IO="Out">用户数据</param>
  RealDataCallBack = procedure(lRealHandle: LONG; dwDataType: DWORD; pBuffer: PByte; dwBufSize: DWORD; pUser: PVOID); stdcall;

  {-------- 预览参数结构体 --------}
  NET_DVR_CLIENTINFO = packed record
    lChannel: LONG;           //通道号，1~32表示模拟通道1~32，9000系列混合型DVR和NVR等设备的IP通道从33开始
    lLinkMode: LONG;          //最高位(31)为0表示主码流，为1表示子码流；0~30位表示连接方式：0-TCP方式，1-UDP方式，2-多播方式
    hPlayWnd: HWND;           //播放窗口的句柄，为NULL表示不显示图像
    sMultiCastIP: PAnsiChar;  //多播组地址
  end;

  LPNET_DVR_CLIENTINFO = ^NET_DVR_CLIENTINFO;

  {-------- 预览参数结构体 --------}
  NET_DVR_PREVIEWINFO = packed record
    lChannel: LONG;                                   //通道号，目前设备模拟通道号从1开始，数字通道的起始通道号通过NET_DVR_GetDVRConfig(配置命令NET_DVR_GET_IPPARACFG_V40)获取(dwStartDChan)。
    dwStreamType: DWORD;                              //码流类型：0-主码流，1-子码流，2-三码流，3-虚拟码流，以此类推
    dwLinkMode: DWORD;                                //连接方式：0-TCP方式，1-UDP方式，2-多播方式，3-RTP方式，4-RTP/RTSP，5-RTP/HTTP，6-HRUDP(可靠传输)
    hPlayWnd: HWND;                                   //播放窗口的句柄，为NULL表示不解码显示
    bBlocked: BOOL;                                   //0-非阻塞取流，1-阻塞取流
    bPassbackRecord: BOOL;                            //是否启用录像回传：0-不启用录像回传，1-启用录像回传
    byPreviewMode: Byte;                              //延迟预览模式：0-正常预览，1-延迟预览
    byStreamID: array[0..STREAM_ID_LEN - 1] of Byte;  //流ID，为字母、数字和"_"的组合，lChannel为$ffffffff时启用此参数
    byProtoType: Byte;                                //应用层取流协议：0-私有协议，1-RTSP协议。
    byRes1: Byte;                                     //保留，置为0
    byVideoCodingType: Byte;                          //码流数据编解码类型：0-通用编码数据，1-热成像探测器产生的原始数据
    dwDisplayBufNum: DWORD;                           //播放库播放缓冲区最大缓冲帧数，取值范围：1、6(默认，自适应播放模式)、15，置0时默认为1
    byRes: array[0..215] of Byte;                     //保留，置为0
  end;

  LPNET_DVR_PREVIEWINFO = ^NET_DVR_PREVIEWINFO;

{-------- 报警布防/监听 -------------------------------------------------------}

  {-------- 报警设备信息结构体 --------}
  NET_DVR_ALARMER = packed record
    byUserIDValid: Byte;                                //userid是否有效：0-无效；1-有效
    bySerialValid: Byte;                                //序列号是否有效：0-无效；1-有效
    byVersionValid: Byte;                               //版本号是否有效：0-无效；1-有效
    byDeviceNameValid: Byte;                            //设备名字是否有效：0-无效；1-有效
    byMacAddrValid: Byte;                               //MAC地址是否有效：0-无效；1-有效
    byLinkPortValid: Byte;                              //Login端口是否有效：0-无效；1-有效
    byDeviceIPValid: Byte;                              //设备IP是否有效：0-无效；1-有效
    bySocketIPValid: Byte;                              //Socket IP是否有效：0-无效；1-有效
    lUserID: LONG;                                      //NET_DVR_Login或NET_DVR_Login_V30返回值, 布防时有效
    sSerialNumber: array[0..SERIALNO_LEN - 1] of Byte;  //序列号
    dwDeviceVersion: DWORD;                             //版本信息：V3.0以上版本支持的设备最高8位为主版本号，次高8位为次版本号，低16位为修复版本号；V3.0以下版本支持的设备高16位表示主版本，低16位表示次版本
    sDeviceName: array[0..NAME_LEN - 1] of AnsiChar;    //设备名称
    byMacAddr: array[0..MACADDR_LEN - 1] of Byte;       //MAC地址
    wLinkPort: Word;                                    //设备通讯端口
    sDeviceIP: array[0..127] of AnsiChar;               //设备IP地址
    sSocketIP: array[0..127] of AnsiChar;               //报警主动上传时的Socket IP地址
    byIpProtocol: Byte;                                 //IP协议：0-IPV4；1-IPV6
    byRes2: array[0..10] of Byte;                       //保留，置为0
  end;

  LPNET_DVR_ALARMER = ^NET_DVR_ALARMER;

/// <summary>报警消息等回调函数</summary>
/// <param name="lCommand" IO="Out">上传的消息类型，不同的报警信息对应不同的类型，通过类型区分是什么报警信息</param>
/// <param name="pAlarmer" IO="Out">报警设备信息，包括设备序列号、IP地址、登录IUserID句柄等</param>
/// <param name="AlarmInfo" IO="Out">报警信息，通过lCommand值判断pAlarmer对应的结构体</param>
/// <param name="dwBufLen" IO="Out">报报警信息缓存大小</param>
/// <param name="pUser" IO="Out">用户数据</param>
  MSGCallBack = procedure(lCommand: LONG; pAlarmer: LPNET_DVR_ALARMER; AlarmInfo: PAnsiChar; dwBufLen: DWORD; pUser: PVOID); stdcall;

/// <summary>报警消息等回调函数</summary>
/// <param name="lCommand" IO="Out">上传的消息类型，不同的报警信息对应不同的类型，通过类型区分是什么报警信息</param>
/// <param name="pAlarmer" IO="Out">报警设备信息，包括设备序列号、IP地址、登录IUserID句柄等</param>
/// <param name="AlarmInfo" IO="Out">报警信息，通过lCommand值判断pAlarmer对应的结构体</param>
/// <param name="dwBufLen" IO="Out">报报警信息缓存大小</param>
/// <param name="pUser" IO="Out">用户数据</param>
  MSGCallBack_V31 = function(lCommand: LONG; pAlarmer: LPNET_DVR_ALARMER; AlarmInfo: PAnsiChar; dwBufLen: DWORD; pUser: PVOID): BOOL; stdcall;

  {-------- 报警布防参数结构体 --------}
  NET_DVR_SETUPALARM_PARAM = packed record
    dwSize: DWORD;                //结构体大小
    byLevel: Byte;                //布防优先级：0-一等级(高)，1-二等级(中)，2-三等级(低)
    byAlarmInfoType: Byte;        //智能交通报警信息上传类型：0-老报警信息(NET_DVR_PLATE_RESULT)，1-新报警信息
    byRetAlarmTypeV40: Byte;      //0-移动侦测、视频丢失、遮挡、IO信号量等报警信息以普通方式上传，1-报警信息以数据可变长方式上传
    byRetDevInfoVersion: Byte;    //CVR上传报警信息类型(仅对接CVR时有效)：0-COMM_ALARM_DEVICE，1-COMM_ALARM_DEVICE_V40
    byRetVQDAlarmType: Byte;      //VQD报警上传类型(仅对接VQD诊断功能的设备有效)：0-COMM_ALARM_VQD，1-COMM_ALARM_VQD_EX
    byFaceAlarmDetection: Byte;   //人脸报警信息类型：1-人脸侦测报警，0-人脸抓拍报警
    bySupport: Byte;              //能力：bit0-二级布防是否上传图片，0-上传，1-不上传；Bit1-是否启用断网续传数据确认机制，0-不开启，1-开启
    byBrokenNetHttp: Byte;        //断网续传类型(设备目前只支持一个断网续传布防连接)，按位表示，0-不续传，1-续传：bit0-车牌检测(IPC)，bit1-客流统计(IPC)，bit2-热度图统计(IPC)，bit3-人脸抓拍(IPC)，bit4-人脸对比(IPC)，bit5-JSON报警透传(IPC)
    wTaskNo: Word;                //任务处理号
    byDeployType: Byte;           //布防类型：0-客户端布防，1-实时布防
    byRes1: array[0..2] of Byte;  //保留，置为0
    byAlarmTypeURL: Byte;         //报警图片数据类型：bit0-人脸抓拍中图片数据上传类型，0-二进制传输，1-URL传输；bit1-EVENT_JSON中图片数据上传类型：0-二进制传输，1-URL传输；bit2-人脸比对中图片数据上传类型：0-二进制传输，1-URL传输
    byCustomCtrl: Byte;           //bit0表示是否上传副驾驶人脸子图: 0-不上传，1-上传。
  end;

  LPNET_DVR_SETUPALARM_PARAM = ^NET_DVR_SETUPALARM_PARAM;

  {-------- 报警布防参数V50结构体 --------}
  NET_DVR_SETUPALARM_PARAM_V50 = packed record
    dwSize: DWORD;                  //结构体大小
    byLevel: Byte;                  //布防优先级：0-一等级(高)，1-二等级(中)，2-三等级(低)
    byAlarmInfoType: Byte;          //智能交通报警信息上传类型：0-老报警信息，1-新报警信息
    byRetAlarmTypeV40: Byte;        //0-移动侦测、视频丢失、遮挡、IO信号量等报警信息以普通方式上传，1-报警信息以数据可变长方式上传
    byRetDevInfoVersion: Byte;      //CVR上传报警信息类型(仅对接CVR时有效)：0-COMM_ALARM_DEVICE，1-COMM_ALARM_DEVICE_V40
    byRetVQDAlarmType: Byte;        //VQD报警上传类型(仅对接VQD诊断功能的设备有效)：0-COMM_ALARM_VQD，1-COMM_ALARM_VQD_EX
    byFaceAlarmDetection: Byte;     //人脸报警信息类型：1-人脸侦测报警，0-人脸抓拍报警
    bySupport: Byte;                //能力：bit0-二级布防是否上传图片，0-上传，1-不上传；Bit1-是否启用断网续传数据确认机制，0-不开启，1-开启
    byBrokenNetHttp: Byte;          //断网续传类型(设备目前只支持一个断网续传布防连接)，按位表示，0-不续传，1-续传：bit0-车牌检测(IPC)，bit1-客流统计(IPC)，bit2-热度图统计(IPC)，bit3-人脸抓拍(IPC)，bit4-人脸对比(IPC)，bit5-JSON报警透传(IPC)
    wTaskNo: Word;                  //任务处理号
    byDeployType: Byte;             //布防类型：0-客户端布防，1-实时布防
    byRes1: array[0..2] of Byte;    //保留，置为0
    byAlarmTypeURL: Byte;           //报警图片数据类型：bit0-人脸抓拍中图片数据上传类型，0-二进制传输，1-URL传输；bit1-EVENT_JSON中图片数据上传类型：0-二进制传输，1-URL传输；bit2-人脸比对中图片数据上传类型：0-二进制传输，1-URL传输
    byCustomCtrl: Byte;             //bit0表示是否上传副驾驶人脸子图: 0-不上传，1-上传。(注：只在公司内部8600/8200等平台开放)
    byRes4: array[0..127] of Byte;  //保留，置为0
  end;

  LPNET_DVR_SETUPALARM_PARAM_V50 = ^NET_DVR_SETUPALARM_PARAM_V50;

{-------- 车牌识别 ------------------------------------------------------------}

  {-------- 区域框参数结构体 --------}
  NET_VCA_RECT = packed record
    fX: Single;       //边界框左上角点的X轴坐标，取值范围[0.001,1]
    fY: Single;       //边界框左上角点的Y轴坐标，取值范围[0.001,1]
    fWidth: Single;   //边界框的宽度，取值范围[0.001,1]
    fHeight: Single;  //边界框的高度，取值范围[0.001,1]
  end;

  LPNET_VCA_RECT = ^NET_VCA_RECT;

  {-------- 交通抓拍结果车牌信息结构体 --------}
  NET_DVR_PLATE_INFO = packed record
    byPlateType: Byte;                                    //车牌类型
    byColor: Byte;                                        //车牌颜色
    byBright: Byte;                                       //车牌亮度
    byLicenseLen: Byte;                                   //车牌字符个数
    byEntireBelieve: Byte;                                //整个车牌的置信度，取值范围：0~100
    byRegion: Byte;                                       //区域索引值：0-保留，1-欧洲(Europe Region)，2-俄罗斯(Russian Region)，3-欧洲&俄罗斯(EU&CIS)，$ff-所有
    byCountry: Byte;                                      //国家索引值
    byRes: array[0..23] of Byte;                          //保留
    dwXmlLen: DWORD;                                      //XML报警信息长度
    pXmlBuf: PAnsiChar;                                   //XML报警信息指针
    struPlateRect: NET_VCA_RECT;                          //车牌位置
    sLicense: array[0..MAX_LICENSE_LEN - 1] of AnsiChar;  //车牌号码
    byBelieve: array[0..MAX_LICENSE_LEN - 1] of Byte;     //各个识别字符的置信度
  end;

  LPNET_DVR_PLATE_INFO = ^NET_DVR_PLATE_INFO;

  {-------- 交车辆信息参数结构体 --------}
  NET_DVR_VEHICLE_INFO = packed record
    dwIndex: DWORD;                     //车辆序号
    byVehicleType: Byte;                //车辆类型，0-其他车辆，1-小型车，2-大型车，3-行人触发，4-二轮车触发，5-三轮车触发，6-机动车触发
    byColorDepth: Byte;                 //车身颜色深浅，0-深色，1-浅色
    byColor: Byte;                      //车身颜色，0-其他色，1-白色，2-银色，3-灰色，4-黑色，5-红色，6-深蓝，7-蓝色，8-黄色，9-绿色，10-棕色，11-粉色，12-紫色，13-深灰，14-青色，$ff-未进行车身颜色识别
    byRaderState: Byte;                 //雷达状态：0-雷达正常，1-雷达故障，2-雷达一直发送某一个相同速度值，3-雷达送出数据为0，4-雷达送出的数据过大或过小
    wSpeed: Word;                       //车辆速度，单位km/h
    wLength: Word;                      //车身长度
    byIllegalType: Byte;                //0-正常，1-低速，2-超速，3-逆行，4-闯红灯，5-压车道线，6-不按导向，7-路口滞留，8-机占非，9-违法变道，10-机动车违反规定使用专用车道，11-黄牌车禁限，12-路口停车，13-绿灯停车，14-未礼让行人，15-违章停车，16-违章掉头，17-占用应急车道，18-禁右，19-禁左，20-压黄线，21-未系安全带，22-行人闯红灯，23-加塞，24-违法使用远光灯，25-驾驶时拨打接听手持电话，26-左转不让直行，27-右转不让左转，28-掉头不让直行，29-大弯小转。对于ITS终端服务器，该参数无效
    byVehicleLogoRecog: Byte;           //车辆主品牌
    byVehicleSubLogoRecog: Byte;        //车辆子品牌，根据不同的主类型，子品牌取值定义不同
    byVehicleModel: Byte;               //车辆子品牌年款，根据不同的主类型，子品牌年款取值定义不同
    byCustomInfo: array[0..15] of Byte; //自定义信息
    wVehicleLogoRecog: Word;            //车辆主品牌(该字段兼容byVehicleLogoRecog)
    byRes3: array[0..13] of Byte;       //保留
  end;

  LPNET_DVR_VEHICLE_INFO = ^NET_DVR_VEHICLE_INFO;

  {-------- 手动抓拍请求参数结构体 --------}
  strNET_DVR_MANUALSNAP = packed record
    byOSDEnable: Byte;            //抓拍图片上是否关闭OSD信息叠加：0-不关闭(默认)，1-关闭
    byLaneNo: Byte;               //车道号，取值范围：1~6，默认为1
    byRes: array[0..21] of Byte;  //保留，置为0
  end;

  LPNET_DVR_MANUALSNAP = ^strNET_DVR_MANUALSNAP;

  {-------- 老车牌识别结果结构体 --------}
  NET_DVR_PLATE_RESULT = packed record
    dwSize: DWORD;                          //结构体大小
    byResultType: Byte;                     //识别结果类型，0-通过视频识别，1-通过图像识别，2-长录像(支持查询)
    byChanIndex: Byte;                      //车道号
    wAlarmRecordID: Word;                   //报警录像ID(用于查询录像，仅当byResultType为2时有效)
    dwRelativeTime: DWORD;                  //相对时间(保留)
    byAbsTime: array[0..31] of Byte;        //绝对时间，精确到毫秒，yyyymmddhhnnsszzz
    dwPicLen: DWORD;                        //图片长度(近景图)
    dwPicPlateLen: DWORD;                   //车牌小图片长度(车牌彩图)
    dwVideoLen: DWORD;                      //录像内容长度
    byTrafficLight: Byte;                   //0-非红绿灯抓拍，1-绿灯时抓拍；2-红灯时抓拍
    byPicNum: Byte;                         //连拍的图片序号
    byDriveChan: Byte;                      //触发的车道号
    byVehicleType: Byte;                    //车辆类型
    dwBinPicLen: DWORD;                     //二值图长度(仅iDS-65xx支持)
    dwCarPicLen: DWORD;                     //车辆原图长度(仅iDS-65xx支持)
    dwFarCarPicLen: DWORD;                  //远景图长度(仅iDS-65xx支持)
    pBuffer3: PByte;                        //车牌二值图(仅iDS-65xx支持)
    pBuffer4: PByte;                        //车辆原图(仅iDS-65xx支持)
    pBuffer5: PByte;                        //远景图(仅iDS-65xx支持)
    byRelaLaneDirectionType: Byte;          //关联车道方向类型
    byRes3: array[0..6] of Byte;            //保留
    struPlateInfo: NET_DVR_PLATE_INFO;      //车牌信息参数
    struVehicleInfo: NET_DVR_VEHICLE_INFO;  //车辆信息参数
    pBuffer1: PByte;                        //当上传的是图片(近景图)信息时，指针指向图片信息，图片长度为dwPicLen；当上传的是录像时，指针指向录像信息，录像长度为dwVideoLen
    pBuffer2: PByte;                        //当上传的是图片(车牌图)信息时，指针指向车牌小图片信息，车牌小图片的长度为dwPicPlateLen
  end;

  LPNET_DVR_PLATE_RESULT = ^NET_DVR_PLATE_RESULT;

  {-------- 抓拍图片信息结构体 --------}
  NET_ITS_PICTURE_INFO = packed record
    dwDataLen: DWORD;                   //媒体数据长度
    byType: Byte;                       //数据类型：0-车牌图，1-场景图，2-合成图，3-特写图，4-二值图，5-码流，6-人脸子图(主驾驶)，7-人脸子图(副驾驶)，8-非机动车，9-行人，10-称重原始裸数据，11-目标图，12-主驾驶室图，13-副驾驶室图，14-人脸图抠小图
    byDataType: Byte;                   //数据上传方式：0-数据直接上传; 1-云存储服务器URL(3.7Ver)原先的图片数据变成URL数据，图片长度变成URL长度
    byCloseUpType: Byte;                //特写图类型：0-保留，1-非机动车，2-行人
    byPicRecogMode: Byte;               //特写图类型：0-保留，1-非机动车，2-行人
    dwRedLightTime: DWORD;              //经过的红灯时间，单位：s
    byAbsTime: array[0..31] of Byte;    //绝对时间点：yyyymmddhhnnsszzz
    struPlateRect: NET_VCA_RECT;        //当byType为1-场景图时，该参数表示车牌位置，用户可根据位置自己截取车牌特写图；当byType为8-非机动车，9-行人时，该参数表示人体坐标
    struPlateRecgRect: NET_VCA_RECT;    //牌识区域坐标，当图片类型为12-主驾驶室图13-副驾驶室图是，该坐标为驾驶员坐标。参数中的边界宽fWidth和高fHeight若为0，fX和fY不为0，则(fX,fY)表示牌识的中心点坐标
    pBuffer: PByte;                     //保存数据的缓冲区
    dwUTCTime: DWORD;                   //UTC时间
    byCompatibleAblity: Byte;           //兼容能力字段，按位表示，值：0-无效，1-有效。bit0-表示dwUTCTime字段是否有效
    byRes2: array[0..6] of Byte;        //保留
  end;

  LPNET_ITS_PICTURE_INFO = ^NET_ITS_PICTURE_INFO;

  {-------- 新车牌识别结果结构体 --------}
  NET_ITS_PLATE_RESULT = packed record
    dwSize: DWORD;                                      //结构体大小
    dwMatchNo: DWORD;                                   //匹配序号，由(车辆序号、数据类型、车道号)组成匹配码
    byGroupNum: Byte;                                   //图片组数量(一辆过车多台相机抓拍的图片组的总数，用于多相机数据匹配，目前该参数值为1)
    byPicNo: Byte;                                      //连拍的图片序号(接收到图片组数量后，表示接收完成；接收超时不足图片组数量时，根据需要保留或删除)
    bySecondCam: Byte;                                  //是否第二相机抓拍(如远近景抓拍的远景相机，或前后抓拍的后相机，特殊项目中会用到)
    byFeaturePicNo: Byte;                               //闯红灯电警，取第几张图作为特写图，$ff-表示不取
    byDriveChan: Byte;                                  //触发车道号
    byVehicleType: Byte;                                //车型识别：0-未知，1-客车(大型)，2-货车(大型)，3-轿车(小型)，4-面包车，5-小货车，6-行人，7-二轮车，8-三轮车，9-SUV/MPV，10-中型客车，11-机动车，12-非机动车，13-小型轿车，14-微型轿车，15-皮卡车
    byDetSceneID: Byte;                                 //检测场景号，0表示无效，其他取值：[1,4]，IPC为0(不支持)
    byVehicleAttribute: Byte;                           //车辆属性，0表示无附加属性，其他取值按位表示，bit0-黄标车(类似年检的标志)，bit1-危险品车辆，值：0-否，1-是
    wIllegalType: Word;                                 //违章类型，采用国标定义(违章代码描述)。wIllegalType为0时违法类型见dwCustomIllegalType，wIllegalType和dwCustomIllegalType都为0时表示正常卡口抓拍
    byIllegalSubType: array[0..7] of Byte;              //违章子类型
    byPostPicNo: Byte;                                  //违章时取第几张图片作为卡口图，$ff-表示不取
    byChanIndex: Byte;                                  //通道号
    wSpeedLimit: Word;                                  //限速上限(超速时有效)
    byChanIndexEx: Byte;                                //byChanIndexEx*256+byChanIndex表示实际通道号。
    byRes2: Byte;                                       //保留
    struPlateInfo: NET_DVR_PLATE_INFO;                  //车牌信息结构
    struVehicleInfo: NET_DVR_VEHICLE_INFO;              //车辆信息
    byMonitoringSiteID: array[0..47] of Byte;           //监测点编号
    byDeviceID: array[0..47] of Byte;                   //设备编号
    byDir: Byte;                                        //监测方向：1-上行(反向)，2-下行(正向)，3-双向，4-由东向西，5-由南向北，6-由西向东，7-由北向南，8-其它
    byDetectType: Byte;                                 //检测方式：0-车辆检测(不区分具体的车辆检测算法)，1-地感触发，2-视频触发，3-多帧识别，4-雷达触发，5-混行检测
    byRelaLaneDirectionType: Byte;                      //关联车道方向类型
    byCarDirectionType: Byte;                           //车辆具体行驶的方向：0-从上往下，1-从下往上
    dwCustomIllegalType: DWORD;                         //违章类型定义(用户自定义)：当wIllegalType参数为0时，使用该参数；若wIllegalType参数不为0时，以wIllegalType参数为准，该参数无效
    pIllegalInfoBuf: PByte;                             //违法代码字符信息，byIllegalFromatType为1时有效
    byIllegalFromatType: Byte;                          //违章信息格式类型：0-数字格式，1-字符格式
    byPendant: Byte;                                    //车窗是否有悬挂物：0-未知，1-车窗有悬挂物，2-车窗无悬挂物
    byDataAnalysis: Byte;                               //数据是否已分析：0-数据未分析, 1-数据已分析
    byYellowLabelCar: Byte;                             //是否黄标车：0-表示未知，1-非黄标车，2-黄标车
    byDangerousVehicles: Byte;                          //是否危险品车：0-表示未知，1-非危险品车，2-危险品车
    byPilotSafebelt: Byte;                              //主驾驶员是否系安全带：0-未知，1-系安全带，2-未系安全带
    byCopilotSafebelt: Byte;                            //副驾驶员是否系安全带：0-未知，1-系安全带，2-未系安全带
    byPilotSunVisor: Byte;                              //主驾驶是否打开遮阳板：0-未知，1-未打开遮阳板，2-打开遮阳板
    byCopilotSunVisor: Byte;                            //副驾驶是否打开遮阳板：0-未知，1-未打开遮阳板，2-打开遮阳板
    byPilotCall: Byte;                                  //主驾驶员是否在打电话：0-未知，1-未打电话，2-打电话
    byBarrierGateCtrlType: Byte;                        //道闸控制类型：0- 开闸，1- 未开闸 (专用于历史数据中相机根据黑白名单匹配后是否开闸成功的标志)
    byAlarmDataType: Byte;                              //报警数据类型：0-实时数据，1-历史数据
    struSnapFirstPicTime: NET_DVR_TIME_V30;             //端点时间(ms)(抓拍第一张图片的时间)
    dwIllegalTime: DWORD;                               //违法持续时间(ms)=抓拍最后一张图片的时间-抓拍第一张图片的时间
    dwPicNum: DWORD;                                    //图片数量(与byGroupNum不同，代表本条信息附带的图片数量)
    struPicInfo: array[0..5] of NET_ITS_PICTURE_INFO;   //图片信息，单张回调，最多6张图，由序号区分
  end;

  LPNET_ITS_PLATE_RESULT = ^NET_ITS_PLATE_RESULT;

  {-------- 违法代码字符信息结构体 --------}
  NET_ITS_ILLEGAL_INFO = packed record
    byIllegalInfo: array[0..MAX_ILLEGAL_LEN - 1] of Byte; //违章类型信息(字符格式)
    byRes: array[0..255] of Byte;                         //保留，置为0
  end;

  LPNET_ITS_ILLEGAL_INFO = ^NET_ITS_ILLEGAL_INFO;

  {-------- 触发参数结构体 --------}
  NET_DVR_SNAPCFG = packed record
    dwSize: DWORD;                                          //结构体大小
    byRelatedDriveWay: Byte;                                //触发IO关联的车道号，取值范围[0,9]
    bySnapTimes: Byte;                                      //线圈抓拍次数，0-不抓拍，非0-连拍次数，目前最大5次
    wSnapWaitTime: Word;                                    //抓拍等待时间，单位ms，取值范围[0,60000]
    wIntervalTime: array[0..MAX_INTERVAL_NUM - 1] of Word;  //连拍间隔时间，单位ms，取值范围[67,60000]
    dwSnapVehicleNum: DWORD;                                //抓拍车辆序号，从1开始，逐次递增
    struJpegPara: NET_DVR_JPEGPARA;                         //抓拍图片参数，需要设备支持(参数取值范围由设备能力集为准)，目前门禁考勤一体机V1.1支持
    byRes2: array[0..15] of Byte;                           //保留，置为0
  end;

  LPNET_DVR_SNAPCFG = ^NET_DVR_SNAPCFG;

{-------- 硬盘录像机 ----------------------------------------------------------}

  {-------- ATM文件查找条件结构体 --------}
  NET_DVR_ATMFINDINFO = packed record
    byTransactionType: Byte;    //交易类型：0-全部，1-查询，2-取款，3-存款，4-修改密码，5-转账，6-无卡查询，7-无卡存款，8-吞钞，9-吞卡，10-自定义
    byRes: array[0..2] of Byte; //保留，置为0
    dwTransationAmount: DWORD;  //交易金额，-1表示查找时不带金额
  end;

  LPNET_DVR_ATMFINDINFO = ^NET_DVR_ATMFINDINFO;

  {-------- 智能查找条件联合体 --------}
  NET_DVR_SPECIAL_FINDINFO_UNION = packed record
    case Integer of
      0:
        (byLenth: array[0..7] of Byte;);          //联合体大小，8字节
      1:
        (struATMFindInfo: NET_DVR_ATMFINDINFO;);  //ATM查询条件
  end;

  LPNET_DVR_SPECIAL_FINDINFO_UNION = ^NET_DVR_SPECIAL_FINDINFO_UNION;

  {-------- 文件查找条件结构体 --------}
  NET_DVR_FILECOND_V40 = packed record
    lChannel: LONG;                                        //通道号
    dwFileType: DWORD;                                     //录象文件类型
    dwIsLocked: DWORD;                                     //(ISAPI登录不支持)是否锁定：0-未锁定文件，1-锁定文件，$ff表示所有文件(包括锁定和未锁定)
    dwUseCardNo: DWORD;                                    //(ISAPI登录不支持)是否带ATM信息进行查询：0-不带ATM信息，1-按交易卡号查询，2-按交易类型查询，3-按交易金额查询，4-按卡号、交易类型及交易金额的组合查询，5-按课程名称查找(此时卡号表示课程名称)
    sCardNumber: array[0..CARDNUM_LEN_OUT - 1] of Byte;    //(ISAPI登录不支持)dwUseCardNo为1、4时表示卡号，有效字符个数为20；dwUseCardNo为5时表示课程名称
    struStartTime: NET_DVR_TIME;                           //开始时间
    struStopTime: NET_DVR_TIME;                            //结束时间
    byDrawFrame: Byte;                                     //(ISAPI登录不支持)是否抽帧：0-不抽帧，1-抽帧
    byFindType: Byte;                                      //(ISAPI登录不支持)0-查询普通卷，1-查询存档卷(适用于CVR设备，普通卷用于通道录像，存档卷用于备份录像)，2-查询N+1录像文件
    byQuickSearch: Byte;                                   //(ISAPI登录不支持)0-普通查询，1-快速(日历)查询，快速查询速度快于普通查询但是相关的录像信息减少(比如没有文件名、文件类型等)
    bySpecialFindInfoType: Byte;                           //(ISAPI登录不支持)专有查询类型：0-无效，1-带ATM信息的查询
    dwVolumeNum: DWORD;                                    //(ISAPI登录不支持)存档卷号，适用于CVR设备
    byWorkingDeviceGUID: array[0..GUID_LEN - 1] of Byte;   //(ISAPI登录不支持)工作机GUID，通过获取N+1设备信息(NET_DVR_WORKING_DEVICE_INFO)得到，byFindType为2时有效。
    uSpecialFindInfo: NET_DVR_SPECIAL_FINDINFO_UNION;      //(ISAPI登录不支持)专有查询条件联合体
    byStreamType: Byte;                                    //(ISAPI登录不支持)码流类型：0-主码流优先(同一个时间段只返回一种录像，优先级顺序为：主码流、子码流、三码流)，1-子码流，2-三码流，3-只搜索返回主码流录像，254-双码流搜索(优先返回主码流录像，没有主码流录像时返回子码流录像)，$ff-全部
    byAudioFile: Byte;                                     //(ISAPI登录不支持)查找音频文件：0-不搜索音频文件，1-搜索音频文件，该功能需要设备支持，启动音频搜索后只搜索音频文件
    byRes2: array[0..29] of Byte;                          //保留
  end;

  LPNET_DVR_FILECOND_V40 = ^NET_DVR_FILECOND_V40;

  {-------- 工作机信息结构体 --------}
  NET_DVR_WORKING_DEVICE_INFO = packed record
    struIP: NET_DVR_IPADDR;                                      //设备IP地址
    byLinkStatus: Byte;                                          //连接状态：0-连接失败，1-连接成功
    byWorkStatus: Byte;                                          //工作状态：0-正常，1-异常
    byBacupStatus: Byte;                                         //备份状态：0-不在备份，1-正在备份
    bySyncProgress: Byte;                                        //录像同步进度，0表示未开始，100表示同步完成
    struSyncBeginTime: NET_DVR_TIME_EX;                          //同步开始时间
    struSyncEndTime: NET_DVR_TIME_EX;                            //同步结束时间
    szSerialNumber: array[0..SERIALNO_LEN - 1] of AnsiChar;      //设备序列号
    dwSoftwareVersion: DWORD;                                    //设备软件版本
    byWorkingDeviceGUID: array[0..GUID_LEN - 1] of Byte;         //工作机GUID，非字符串，用于备机上搜索工作机录像
    szDevTypeName: array[0..DEV_TYPE_NAME_LEN - 1] of AnsiChar;  //设备类型名称
    wDevType: Word;                                              //设备类型值
  end;

  LPNET_DVR_WORKING_DEVICE_INFO = ^NET_DVR_WORKING_DEVICE_INFO;

  {-------- 工作机信息结构体 --------}
  NET_DVR_FILECOND = packed record
    lChannel: LONG;                     //通道号
    dwFileType: DWORD;                  //录象文件类型
    dwIsLocked: DWORD;                  //是否锁定：0-未锁定文件，1-锁定文件，$ff表示所有文件(包括锁定和未锁定)
    dwUseCardNo: DWORD;                 //是否带卡号查找
    sCardNumber: array[0..31] of Byte;  //卡号
    struStartTime: NET_DVR_TIME;        //开始时间
    struStopTime: NET_DVR_TIME;         //停止时间
  end;

  LPNET_DVR_FILECOND = ^NET_DVR_FILECOND;

  {-------- 文件查找结果信息结构体 --------}
  NET_DVR_FINDDATA_V40 = packed record
    sFileName: array[0..99] of AnsiChar;   //文件名，日历查询时无效
    struStartTime: NET_DVR_TIME;           //文件的开始时间
    struStopTime: NET_DVR_TIME;            //文件的结束时间
    dwFileSize: DWORD;                     //文件大小
    sCardNum: array[0..31] of AnsiChar;    //卡号
    byLocked: Byte;                        //(ISAPI登录不支持)文件是否被锁定，1-文件已锁定；0-文件未锁定
    byFileType: Byte;                      //文件类型(日历查询时无效)：0-定时录像，1-移动侦测，2-报警触发，3-报警|移动侦测，4-报警&移动侦测，5-命令触发，6-手动录像，7-震动报警，8-环境报警，9-智能报警，10-PIR报警，11-无线报警，12-呼救报警，13-移动侦测、PIR、无线、呼救等所有报警类型的"或"，14-智能交通事件，15-越界侦测，16-区域入侵侦测，17-音频异常侦测，18-场景变更侦测，19-智能侦测，20-人脸侦测21-信号量，22-回传，23-回迁录像，24-遮挡，21-POS录像，26-进入区域侦测，27-离开区域侦测，28-徘徊侦测，29-人员聚集侦测，30-快速运动侦测，31-停车侦测，32-物品遗留侦测，33-物品拿取侦测，34-火点检测，36-船只检测，37-测温预警，38-测温报警，42-温差报警，43-离线测温报警，44-防区报警，45-紧急求助，46-业务咨询，47-起身检测，48-折线攀高，49-如厕超时
    byQuickSearch: Byte;                   //(ISAPI登录不支持)0-普通查询结果，1-快速(日历)查询结果
    byRes: Byte;                           //保留
    dwFileIndex: DWORD;                    //(ISAPI登录不支持)文件索引号
    byStreamType: Byte;                    //(ISAPI登录不支持)码流类型：0-主码流，1-子码流，2-码流三
    byRes1: array[0..126] of Byte;         //保留
  end;

  LPNET_DVR_FINDDATA_V40 = ^NET_DVR_FINDDATA_V40;

  {-------- 录像文件信息结构体 --------}
  NET_DVR_FINDDATA_V30 = packed record
    sFileName: array[0..99] of AnsiChar; //文件名
    struStartTime: NET_DVR_TIME;         //文件的开始时间
    struStopTime: NET_DVR_TIME;          //文件的结束时间
    dwFileSize: DWORD;                   //文件大小
    sCardNum: array[0..31] of AnsiChar;  //(ISAPI登录不支持)卡号
    byLocked: Byte;                      //(ISAPI登录不支持)文件是否被锁定，1-文件已锁定；0-文件未锁定
    byFileType: Byte;                    //文件类型： 0-定时录像，1-移动侦测，2-报警触发，3-报警|移动侦测，4-报警&移动侦测，5-命令触发，6-手动录像，7-震动报警，8-环境报警，9-智能报警，10-PIR报警，11-无线报警，12-呼救报警，13-移动侦测、PIR、无线、呼救等所有报警类型的"或"，14-智能交通事件，15-越界侦测，16-区域入侵，17-声音异常，18-场景变更侦测
    byRes: array[0..1] of Byte;          //保留，置为0
  end;

  LPNET_DVR_FINDDATA_V30 = ^NET_DVR_FINDDATA_V30;

  {-------- 录像文件信息结构体 --------}
  NET_DVR_FIND_DATA = packed record
    sFileName: array[0..99] of AnsiChar;  //文件名
    struStartTime: NET_DVR_TIME;          //文件的开始时间
    struStopTime: NET_DVR_TIME;           //文件的结束时间
    dwFileSize: DWORD;                    //文件大小
  end;

  LPNET_DVR_FIND_DATA = ^NET_DVR_FIND_DATA;

  {-------- 录像回放结构体 --------}
  NET_DVR_VOD_PARA = packed record
    dwSize: DWORD;                    //结构体大小
    struIDInfo: NET_DVR_STREAM_INFO;  //(ISAPI登录不支持)流信息，流ID或者通道号
    struBeginTime: NET_DVR_TIME;      //开始时间
    struEndTime: NET_DVR_TIME;        //结束时间
    HWND: HWND;                       //回放的窗口句柄，若置为空，SDK仍能收到码流数据，但不解码显示
    byDrawFrame: Byte;                //(ISAPI登录不支持)是否抽帧：0-不抽帧，1-抽帧
    byVolumeType: Byte;               //(ISAPI登录不支持)0-普通录像卷，1-存档卷，适用于CVR设备，普通卷用于通道录像，存档卷用于备份录像
    byVolumeNum: Byte;                //(ISAPI登录不支持)存档卷号
    byStreamType: Byte;               //(ISAPI登录不支持)码流类型：0-主码流，1-子码流，2-码流三
    dwFileIndex: DWORD;               //(ISAPI登录不支持)存档卷上的录像文件索引，搜索存档卷录像时返回的值
    byAudioFile: Byte;                //(ISAPI登录不支持)回放音频文件：0-不回放音频文件，1-回放音频文件，该功能需要设备支持，启动音频回放后只回放音频文件
    byCourseFile: Byte;               //(ISAPI登录不支持)课程文件：0-否，1-是
    byRes2: array[0..21] of Byte;     //保留，置为0
  end;

  LPNET_DVR_VOD_PARA = ^NET_DVR_VOD_PARA;

  {-------- 回放或者下载信息结构体 --------}
  NET_DVR_PLAYCOND = packed record
    dwChannel: DWORD;                                  //通道号
    struStartTime: NET_DVR_TIME;                       //开始时间
    struStopTime: NET_DVR_TIME;                        //结束时间
    byDrawFrame: Byte;                                 //是否抽帧：0-不抽帧，1-抽帧
    byStreamType: Byte;                                //码流类型：0-主码流，1-子码流，2-码流三
    byStreamID: array[0..STREAM_ID_LEN - 1] of Byte;   //流ID，使用流ID方式时dwChannel设为$ffffffff
    byCourseFile: Byte;                                //课程文件：0-否，1-是
    byRes: array[0..28] of Byte;                       //保留
  end;

  LPNET_DVR_PLAYCOND = ^NET_DVR_PLAYCOND;

  {-------- 手动录像参数结构体 --------}
  NET_DVR_MANUAL_RECORD_PARA = packed record
    struStreamInfo: NET_DVR_STREAM_INFO;  //流ID信息
    lRecordType: LONG;                    //录像类型：0-手动录像，1-移动侦测录像，2-信号量IO报警录像，3-遮挡报警录像
    byRes: array[0..31] of Byte;          //保留，置为0
  end;

  LPNET_DVR_MANUAL_RECORD_PARA = ^NET_DVR_MANUAL_RECORD_PARA;

  {-------- 外部命令触发录像参数结构体 --------}
  NET_DVR_CMD_TRIGGER_PERIOD_RECORD_PARA = packed record
    struStreamInfo: NET_DVR_STREAM_INFO;               //流信息
    dwCmdType: DWORD;                                  //外部触发类型，作为唯一的命令码标识，赋值和对应含义由应用层自己定义
    dwRecordTimeLen: DWORD;                            //录像持续时间，0表示停止，单位：秒
    byEventID: array[0..MAX_EVENTID_LEN - 1] of Byte;  //事件ID，作为附加信息
    dwLockDuration: DWORD;                             //锁定持续时间，单位：秒，$ffffffff-永久锁定，0-不锁
    byBackUp: Byte;                                    //是否存档，0-不存档，1-存档
    byPreRecord: Byte;                                 //是否预录，0-不预录，1-预录
    byRes: array[0..121] of Byte;                      //保留，置为0
  end;

  LPNET_DVR_CMD_TRIGGER_PERIOD_RECORD_PARA = ^NET_DVR_CMD_TRIGGER_PERIOD_RECORD_PARA;

{======== 接口函数 ============================================================}

{-------- 通用/基础 -----------------------------------------------------------}

{-------- 准备工作 --------}

/// <summary>初始化SDK，调用其他SDK函数的前提。</summary>
/// <returns>True成功，False失败</returns>
function NET_DVR_Init: BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>释放SDK资源，在程序结束之前调用。</summary>
/// <returns>True成功，False失败</returns>
function NET_DVR_Cleanup: BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>返回最后操作的错误码</summary>
/// <returns>错误码</returns>
function NET_DVR_GetLastError: DWORD; stdcall; external 'HCNetSDK.dll';

/// <summary>设置网络连接超时时间和连接尝试次数</summary>
/// <param name="dwWaitTime" IO="In">超时时间，单位毫秒，取值范围[300,75000]</param>
/// <param name="dwTryTimes" IO="In">连接尝试次数(保留)</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetConnectTime(dwWaitTime: DWORD = 3000; dwTryTimes: DWORD = 3): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>设置重连功能</summary>
/// <param name="dwInterval" IO="In">重连间隔，单位:毫秒</param>
/// <param name="bEnableRecon" IO="In">是否重连</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetReconnect(dwInterval: DWORD; bEnableRecon: BOOL = True): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>启用日志文件写入接口</summary>
/// <param name="nLogLevel" IO="In">日志的等级(默认为0)：0-关闭日志，1-只输出ERROR错误日志，2-输出ERROR错误信息和DEBUG调试信息，3-输出ERROR错误信息、DEBUG调试信息和INFO普通信息等所有信息</param>
/// <param name="strLogDir" IO="In">日志文件的路径，默认值为"C:\SdkLog\"</param>
/// <param name="bAutoDel" IO="In">是否删除超出的文件数，默认值为TRUE</param>
/// <returns>True成功，False失败</returns>
/// <remarks>日志文件路径必须是绝对路径。</remarks>
function NET_DVR_SetLogToFile(nLogLevel: DWORD = 0; strLogDir: PAnsiChar = nil; bAutoDel: BOOL = True): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>注册接收异常、重连等消息的窗口句柄</summary>
/// <param name="nMessage" IO="In">消息</param>
/// <param name="hWnd" IO="In">接收异常信息消息的窗口句柄</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetDVRMessage(nMessage: UINT; HWND: HWND): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>注册接收异常、重连等消息的窗口句柄或回调函数</summary>
/// <param name="nMessage" IO="In">消息</param>
/// <param name="hWnd" IO="In">接收异常信息消息的窗口句柄</param>
/// <param name="cbExceptionCallBack" IO="In">接收异常消息的回调函数，回调当前异常的相关信息</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>True成功，False失败</returns>
/// <remarks>hWnd和cbExceptionCallBack不能同时为NULL</remarks>
function NET_DVR_SetExceptionCallBack_V30(nMessage: UINT; HWND: HWND; cbExceptionCallBack: ExceptionCallBack; pUser: PVOID): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 登录/登出 --------}

/// <summary>用户注册设备</summary>
/// <param name="sDVRIP" IO="In">设备IP地址</param>
/// <param name="wDVRPort" IO="In">设备端口号</param>
/// <param name="sUserName" IO="In">登录的用户名</param>
/// <param name="sPassword" IO="In">用户密码</param>
/// <param name="lpDeviceInfo" IO="Out">设备信息</param>
/// <returns>-1表示失败，其他值表示返回的用户ID值</returns>
function NET_DVR_Login(sDVRIP: PAnsiChar; wDVRPort: Word; sUserName, sPassword: PAnsiChar; lpDeviceInfo: LPNET_DVR_DEVICEINFO): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>用户注册设备</summary>
/// <param name="sDVRIP" IO="In">设备IP地址或是静态域名，字符数不大于128个</param>
/// <param name="wDVRPort" IO="In">设备端口号</param>
/// <param name="sUserName" IO="In">登录的用户名</param>
/// <param name="sPassword" IO="In">用户密码</param>
/// <param name="lpDeviceInfo" IO="Out">设备信息</param>
/// <returns>-1表示失败，其他值表示返回的用户ID值</returns>
function NET_DVR_Login_V30(sDVRIP: PAnsiChar; wDVRPort: Word; sUserName, sPassword: PAnsiChar; lpDeviceInfo: LPNET_DVR_DEVICEINFO_V30): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>用户注册设备(支持异步登录)</summary>
/// <param name="pLoginInfo" IO="In">登录参数，包括设备地址、登录用户、密码等</param>
/// <param name="lpDeviceInfo" IO="Out">设备信息(同步登录即pLoginInfo中bUseAsynLogin为0时有效)</param>
/// <returns>对于同步登录，接口返回-1表示登录失败，其他值表示返回的用户ID值</returns>
function NET_DVR_Login_V40(pLoginInfo: LPNET_DVR_USER_LOGIN_INFO; lpDeviceInfo: LPNET_DVR_DEVICEINFO_V40): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>用户注销</summary>
/// <param name="lUserID" IO="In">用户ID号，NET_DVR_Login_V40等登录接口的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_Logout(lUserID: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>用户注销</summary>
/// <param name="lUserID" IO="In">用户ID号，NET_DVR_Login_V40等登录接口的返回值</param>
/// <returns>True成功，False失败</returns>
/// <remarks>该接口强制停止该用户的所有操作和释放所有的资源，确保该ID对应的线程都安全退出，资源得到释放。建议使用NET_DVR_Logout接口实现注销功能。</remarks>
function NET_DVR_Logout_V30(lUserID: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 设备抓图 --------}

/// <summary>单帧数据捕获并保存成JPEG图</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号</param>
/// <param name="lpJpegPara" IO="In">JPEG图像参数</param>
/// <param name="sPicFileName" IO="In">保存JPEG图的文件路径(包括文件名)</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_CaptureJPEGPicture(lUserID, lChannel: LONG; lpJpegPara: LPNET_DVR_JPEGPARA; sPicFileName: PAnsiChar): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>单帧数据捕获并保存成JPEG存放在指定的内存空间中</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号</param>
/// <param name="lpJpegPara" IO="In">JPEG图像参数</param>
/// <param name="sJpegPicBuffer" IO="In"> 保存JPEG数据的缓冲区</param>
/// <param name="dwPicSize" IO="In">输入缓冲区大小</param>
/// <param name="lpSizeReturned" IO="Out">返回图片数据的大小</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_CaptureJPEGPicture_NEW(lUserID, lChannel: LONG; lpJpegPara: LPNET_DVR_JPEGPARA; sJpegPicBuffer: Pointer; dwPicSize: DWORD; lpSizeReturned: LPDWORD): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 参数配置 ------------------------------------------------------------}

{-------- 系统参数 --------}

/// <summary>获取设备的配置信息</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="dwCommand" IO="In">设备配置命令</param>
/// <param name="lChannel" IO="In">通道号，不同的命令对应不同的取值，如果该参数无效则置为0xFFFFFFFF即可</param>
/// <param name="lpOutBuffer" IO="Out">接收数据的缓冲指针</param>
/// <param name="dwOutBufferSize" IO="In">接收数据的缓冲长度(以字节为单位)，不能为0</param>
/// <param name="lpBytesReturned" IO="Out">实际收到的数据长度指针，不能为NULL</param>
/// <returns></returns>
function NET_DVR_GetDVRConfig(lUserID: LONG; dwCommand: DWORD; lChannel: LONG; lpOutBuffer: LPVOID; dwOutBufferSize: DWORD; lpBytesReturned: LPDWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>设置设备的配置信息</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="dwCommand" IO="In">设备配置命令</param>
/// <param name="lChannel" IO="In">通道号，不同的命令对应不同的取值，如果该参数无效则置为0xFFFFFFFF即可</param>
/// <param name="lpInBuffer" IO="In">入数据的缓冲指针</param>
/// <param name="dwInBufferSize" IO="In">输入数据的缓冲长度(以字节为单位)</param>
/// <returns></returns>
function NET_DVR_SetDVRConfig(lUserID: LONG; dwCommand: DWORD; lChannel: LONG; lpInBuffer: LPVOID; dwInBufferSize: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 实时预览 ------------------------------------------------------------}

{-------- 实时预览 --------}

/// <summary>实时预览</summary>
/// <param name="lUserID">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lpClientInfo">预览参数</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopRealPlay等函数的句柄参数</returns>

function NET_DVR_RealPlay(lUserID: LONG; lpClientInfo: LPNET_DVR_CLIENTINFO): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>实时预览</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lpClientInfo" IO="In">预览参数</param>
/// <param name="cbRealDataCallBack" IO="In">码流数据回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <param name="bBlocked" IO="In">请求码流过程是否阻塞：0-否；1-是</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopRealPlay等函数的句柄参数</returns>
function NET_DVR_RealPlay_V30(lUserID: LONG; lpClientInfo: LPNET_DVR_CLIENTINFO; cbRealDataCallBack: RealDataCallBack_V30; pUser: PVOID; bBlocked: BOOL): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>实时预览(支持多码流)</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lpPreviewInfo" IO="In">预览参数</param>
/// <param name="fRealDataCallBack_V30" IO="In">码流数据回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopRealPlay等函数的句柄参数</returns>
/// <remarks>fRealDataCallBack_V30回调函数中不能执行可能会占用时间较长的接口或操作，不建议调用该SDK(HCNetSDK.dll)本身的接口。</remarks>
function NET_DVR_RealPlay_V40(lUserID: LONG; lpPreviewInfo: LPNET_DVR_PREVIEWINFO; fRealDataCallBack_V30: RealDataCallBack; pUser: PVOID): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>停止预览</summary>
/// <param name="lRealHandle" IO="In">预览句柄</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopRealPlay(lRealHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 预览抓图 --------}

/// <summary>设置抓图模式</summary>
/// <param name="dwCaptureMode" IO="In">抓图模式</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetCapturePictureMode(dwCaptureMode: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>预览时抓图并保存成图片文件</summary>
/// <param name="lRealHandle" IO="In">预览句柄，NET_DVR_RealPlay_V40的返回值</param>
/// <param name="sPicFileName" IO="In">保存图片的文件路径，包含文件名</param>
/// <param name="dwTimeOut" IO="In">超时时间，目前无效</param>
/// <returns>True成功，False失败</returns>
/// <remarks>该接口为预览阻塞模式抓图，预览接口必须传入有效的窗口句柄，正常解码显示的时候才能调用该接口进行抓图</remarks>
function NET_DVR_CapturePictureBlock(lRealHandle: LONG; const sPicFileName: PAnsiChar; dwTimeOut: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>预览时抓图并保存在指定内存中</summary>
/// <param name="lRealHandle" IO="In">预览句柄，NET_DVR_RealPlay_V40的返回值</param>
/// <param name="pPicBuf" IO="In">保存图片数据的缓冲区</param>
/// <param name="dwPicSize" IO="In">缓冲区大小，分配的缓冲区内存必须大于等于图片数据的大小</param>
/// <param name="lpSizeReturned" IO="Out">返回图片数据的实际大小</param>
/// <returns>True成功，False失败</returns>
/// <remarks>该接口为预览阻塞模式抓图，预览接口必须传入有效的窗口句柄，正常解码显示的时候才能调用该接口进行抓图</remarks>
function NET_DVR_CapturePictureBlock_New(lRealHandle: LONG; pPicBuf: Pointer; dwPicSize: DWORD; lpSizeReturned: LPDWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>预览时，单帧数据捕获并保存成图片</summary>
/// <param name="lRealHandle" IO="In">预览句柄</param>
/// <param name="sPicFileName" IO="In">保存图象的文件路径(包括文件名)</param>
/// <returns>True成功，False失败</returns>
/// <remarks>要求在调用NET_DVR_RealPlay_V40等接口时传入非空的播放句柄(播放库解码显示)</remarks>
function NET_DVR_CapturePicture(lRealHandle: LONG; sPicFileName: PAnsiChar): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 声音播放控制 --------}

/// <summary>设置声音播放模式</summary>
/// <param name="dwMode" IO="In">声音播放模式：1-独占声卡，单路音频模式；2-共享声卡，多路音频模式</param>
/// <returns>True成功，False失败</returns>
/// <remarks>默认为独占播放</remarks>
function NET_DVR_SetAudioMode(dwMode: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>独占声卡模式下开启声音</summary>
/// <param name="lRealHandle" IO="In">预览句柄</param>
/// <returns>True成功，False失败</returns>
/// <remarks>如果当前是共享模式播放，调用该接口将返回失败。以独占方式只能打开一路通道播放，即依次打开多个通道时仅打开最后一路</remarks>
function NET_DVR_OpenSound(lRealHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>独占声卡模式下关闭声音</summary>
/// <returns>True成功，False失败</returns>
function NET_DVR_CloseSound: BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>共享声卡模式下开启声音</summary>
/// <param name="lRealHandle" IO="In">预览句柄</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_OpenSoundShare(lRealHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>共享声卡模式下关闭声音</summary>
/// <returns>True成功，False失败</returns>
function NET_DVR_CloseSoundShare: BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>调节播放音量</summary>
/// <param name="lRealHandle" IO="In">预览句柄</param>
/// <param name="wVolume" IO="In">音量，取值范围[0,$ffff]</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_Volume(lRealHandle: LONG; wVolume: Word): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 报警布防/监听 -------------------------------------------------------}

{-------- 注册回调函数 --------}

/// <summary>注册回调函数，接收设备报警消息等</summary>
/// <param name="fMessageCallBack" IO="In">回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>True成功，False失败</returns>

function NET_DVR_SetDVRMessageCallBack_V30(fMessageCallBack: MSGCallBack; pUser: PVOID): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>注册回调函数，接收设备报警消息等</summary>
/// <param name="fMessageCallBack" IO="In">回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetDVRMessageCallBack_V31(fMessageCallBack: MSGCallBack_V31; pUser: PVOID): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>注册报警信息回调函数</summary>
/// <param name="iIndex" IO="In">回调函数索引，取值范围：[0,15]</param>
/// <param name="fMessageCallBack" IO="In">回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_SetDVRMessageCallBack_V50(iIndex: Integer; fMessageCallBack: MSGCallBack; pUser: PVOID): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 布防/撤防 --------}

/// <summary>建立报警上传通道，获取报警等信息</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login或者NET_DVR_Login_30的返回值</param>
/// <returns>-1表示失败，其他值作为NET_DVR_CloseAlarmChan函数的句柄参数</returns>
/// <remarks>启动布防前，需要调用注册回调函数的接口才能获取到上传的报警等信息</remarks>
function NET_DVR_SetupAlarmChan(lUserID: LONG): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>建立报警上传通道，获取报警等信息</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login或者NET_DVR_Login_V30的返回值</param>
/// <returns>-1表示失败，其他值作为NET_DVR_CloseAlarmChan_V30函数的句柄参数</returns>
/// <remarks>启动布防前，需要调用注册回调函数的接口才能获取到上传的报警等信息</remarks>
function NET_DVR_SetupAlarmChan_V30(lUserID: LONG): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>建立报警上传通道，获取报警等信息</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login或者NET_DVR_Login_V30的返回值</param>
/// <param name="lpSetupParam" IO="In">报警布防参数</param>
/// <returns>-1表示失败，其他值作为NET_DVR_CloseAlarmChan_V30函数的句柄参数</returns>
/// <remarks>启动布防前，需要调用注册回调函数的接口NET_DVR_SetDVRMessageCallBack_V30才能获取到上传的报警等信息</remarks>
function NET_DVR_SetupAlarmChan_V41(lUserID: LONG; lpSetupParam: LPNET_DVR_SETUPALARM_PARAM): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>建立报警上传通道，获取报警等信息(支持报警订阅模式，兼容V41接口)</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login或者NET_DVR_Login_V30的返回值</param>
/// <param name="lpSetupParam" IO="In">报警布防参数</param>
/// <param name="sPicFileName" IO="In">报警订阅条件信息，输入为XML格式</param>
/// <param name="dwDataLen" IO="In">报警订阅条件信息长度</param>
/// <returns>-1表示失败，其他值作为NET_DVR_CloseAlarmChan_V30函数的句柄参数</returns>
function NET_DVR_SetupAlarmChan_V50(lUserID: LONG; lpSetupParam: LPNET_DVR_SETUPALARM_PARAM_V50; const sPicFileName: PAnsiChar; dwDataLen: DWORD): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>撤销报警上传通道</summary>
/// <param name="lAlarmHandle" IO="In">NET_DVR_SetupAlarmChan的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_CloseAlarmChan(lAlarmHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>撤销报警上传通道</summary>
/// <param name="lAlarmHandle" IO="In">NET_DVR_SetupAlarmChan_V30或者NET_DVR_SetupAlarmChan_V41的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_CloseAlarmChan_V30(lAlarmHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 报警监听 --------}

/// <summary>启动监听，接收设备主动上传的报警等信息</summary>
/// <param name="sLocalIP" IO="In">PC机本地IP地址,可以置为NULL</param>
/// <param name="wLocalPort" IO="In">PC本地监听端口号。由用户设置，必须和设备端设置的一致</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StartListen(sLocalIP: PAnsiChar; wLocalPort: Word): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>启动监听，接收设备主动上传的报警等信息(支持多线程)</summary>
/// <param name="sLocalIP" IO="In">PC机本地IP地址，可以置为NULL</param>
/// <param name="wLocalPort" IO="In">PC本地监听端口号。由用户设置，必须和设备端设置的一致</param>
/// <param name="DataCallback" IO="In">回调函数</param>
/// <param name="pUser" IO="In">用户数据</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopListen_V30函数的句柄参数</returns>
function NET_DVR_StartListen_V30(sLocalIP: PAnsiChar; wLocalPort: Word; DataCallback: MSGCallBack; pUser: PVOID): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>停止监听</summary>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopListen: BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>停止监听(支持多线程)</summary>
/// <param name="lListenHandle" IO="In">监听句柄，NET_DVR_StartListen_V30的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopListen_V30(lListenHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 车牌识别 ------------------------------------------------------------}

/// <summary>手动触发抓拍</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lpInter" IO="In">手动抓拍参数</param>
/// <param name="lpOuter" IO="Out">识别结果参数</param>
/// <returns>True成功，False失败</returns>
/// <remarks>对于抓拍机，该接口一般用于调试，不能频繁调用进行抓图。调用该接口前，应用层需要自己先分配lpOuter输出结构体里面的pBuffer1等缓冲区的内存，不能小于抓图图片的数据大小</remarks>
function NET_DVR_ManualSnap(lUserID: LONG; lpInter: LPNET_DVR_MANUALSNAP; lpOuter: LPNET_DVR_PLATE_RESULT): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>网络触发抓拍</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lpInter" IO="In">网络触发抓拍配置</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_ContinuousShoot(lUserID: LONG; lpInter: LPNET_DVR_SNAPCFG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 硬盘录像机 ----------------------------------------------------------}

{-------- 手动录像 --------}

/// <summary>远程手动启动设备录像</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号，$00ff表示所有模拟通道，$ff00表示所有数字通道，$ffff表示所有模拟和数字通道</param>
/// <param name="lRecordType" IO="In">录像类型：0-手动，1-报警，2-回传，3-信号，4-移动，5-遮挡</param>
/// <returns>True成功，False失败</returns>
/// <remarks>录像类型设置需要设备支持，不支持默认为手动录像。除了CVR，其他设备目前只支持手动录像</remarks>
function NET_DVR_StartDVRRecord(lUserID, lChannel, lRecordType: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>远程手动停止设备录像</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号，$00ff表示所有模拟通道，$ff00表示所有数字通道，$ffff表示所有模拟和数字通道</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopDVRRecord(lUserID, lChannel: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>按流id远程开启设备手动录像</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="pRecordPara" IO="In">手动录像参数</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StartManualRecord(lUserID: LONG; pRecordPara: LPNET_DVR_MANUAL_RECORD_PARA): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>按流id远程停止设备手动录像</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="pIDInfo" IO="In">流ID信息</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopManualRecord(lUserID: LONG; pIDInfo: LPNET_DVR_STREAM_INFO): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>远程控制</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="dwCommand" IO="In">控制命令</param>
/// <param name="lpInBuffer" IO="In">输入参数，具体内容跟控制命令相关</param>
/// <param name="dwInBufferSize" IO="In">输入参数长度</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_RemoteControl(lUserID: LONG; dwCommand: DWORD; lpInBuffer: LPVOID; dwInBufferSize: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 录像索引 --------}

/// <summary>即时刷新录像索引</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="dwChannel" IO="In">通道号</param>
/// <returns>True成功，False失败</returns>
/// <remarks>和通道相关，需要设备支持，设备默认每2分钟刷新一次</remarks>
function NET_DVR_UpdateRecordIndex(lUserID: LONG; dwChannel: DWORD): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 录像文件查找 --------}

/// <summary>根据文件类型、时间查找设备录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号</param>
/// <param name="dwFileType" IO="In">要查找的文件类型：$ff-全部；0-定时录像；1-移动侦测；2-报警触发；3-报警或动测；4-报警和动测；5-命令触发；6-手动录像；7-智能录像</param>
/// <param name="lpStartTime" IO="In">文件的开始时间</param>
/// <param name="lpStopTime" IO="In">文件的结束时间</param>
/// <returns>-1表示失败，其他值作为NET_DVR_FindClose等函数的参数</returns>
function NET_DVR_FindFile(lUserID, lChannel: LONG; dwFileType: DWORD; lpStartTime, lpStopTime: LPNET_DVR_TIME): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>根据文件类型、时间查找设备录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="pFindCond" IO="In">欲查找的文件信息结构</param>
/// <returns>-1表示失败，其他值作为NET_DVR_FindClose等函数的参数</returns>
function NET_DVR_FindFile_V30(lUserID: LONG; pFindCond: LPNET_DVR_FILECOND): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>根据文件类型、时间查找设备录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="pFindCond" IO="In">欲查找的文件信息结构</param>
/// <returns>-1表示失败，其他值作为NET_DVR_FindClose等函数的参数</returns>
function NET_DVR_FindFile_V40(lUserID: LONG; pFindCond: LPNET_DVR_FILECOND_V40): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>逐个获取查找到的文件信息</summary>
/// <param name="lFindHandle" IO="In">文件查找句柄，NET_DVR_FindFile的返回值</param>
/// <param name="lpFindData" IO="Out">保存文件信息的指针</param>
/// <returns>-1表示失败，其他值表示当前的获取状态等信息</returns>
function NET_DVR_FindNextFile(lFindHandle: LONG; lpFindData: LPNET_DVR_FIND_DATA): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>逐个获取查找到的文件信息</summary>
/// <param name="lFindHandle" IO="In">文件查找句柄，NET_DVR_FindFile_V40或者NET_DVR_FindFile_V30的返回值</param>
/// <param name="lpFindData" IO="Out">保存文件信息的指针</param>
/// <returns>-1表示失败，其他值表示当前的获取状态等信息</returns>
function NET_DVR_FindNextFile_V30(lFindHandle: LONG; lpFindData: LPNET_DVR_FINDDATA_V30): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>逐个获取查找到的文件信息</summary>
/// <param name="lFindHandle" IO="In">文件查找句柄，NET_DVR_FindFile_V40或者NET_DVR_FindFile_V30的返回值</param>
/// <param name="lpFindData" IO="Out">保存文件信息的指针</param>
/// <returns>-1表示失败，其他值表示当前的获取状态等信息</returns>
function NET_DVR_FindNextFile_V40(lFindHandle: LONG; lpFindData: LPNET_DVR_FINDDATA_V40): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>关闭文件查找，释放资源</summary>
/// <param name="lFindHandle" IO="In">文件查找句柄，NET_DVR_FindFile_V40、NET_DVR_FindFileByEvent或者NET_DVR_FindFile_V30的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_FindClose_V30(lFindHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 回放 --------}

/// <summary>按时间回放录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="lChannel" IO="In">通道号</param>
/// <param name="lpStartTime" IO="In">文件的开始时间</param>
/// <param name="lpStopTime" IO="In">文件结束时间</param>
/// <param name="HWND" IO="In">回放的窗口句柄，若置为空，SDK仍能收到码流数据，但不解码显示</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopPlayBack等函数的参数</returns>
function NET_DVR_PlayBackByTime(lUserID, lChannel: LONG; lpStartTime, lpStopTime: LPNET_DVR_TIME; HWND: HWND): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>按流ID和时间回放录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="pVodPara" IO="In">查找条件</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopPlayBack等函数的参数</returns>
function NET_DVR_PlayBackByTime_V40(lUserID: LONG; pVodPara: LPNET_DVR_VOD_PARA): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>按文件名回放录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40等登录接口的返回值</param>
/// <param name="sPlayBackFileName" IO="In">回放的文件名，长度不能超过100字节</param>
/// <param name="HWND" IO="In">回放的窗口句柄，若置为空，SDK仍能收到码流数据，但不解码显示</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopPlayBack等函数的参数</returns>
function NET_DVR_PlayBackByName(lUserID: LONG; sPlayBackFileName: PAnsiChar; HWND: HWND): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>控制录像回放的状态</summary>
/// <param name="lPlayHandle" IO="In">播放句柄，NET_DVR_PlayBackByName或NET_DVR_PlayBackByTime的返回值</param>
/// <param name="dwControlCode" IO="In">控制录像回放状态命令</param>
/// <param name="dwInValue" IO="In">设置的参数，如设置文件回放的进度(命令值NET_DVR_PLAYSETPOS)时，此参数表示进度值；如开始播放(命令值NET_DVR_PLAYSTART)时，此参数表示断点续传的文件位置(Byte)</param>
/// <param name="lpOutValue" IO="Out">获取的参数，如获取当前播放文件总的时间(命令值NET_DVR_GETTOTALTIME )，此参数就是得到的总时间</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_PlayBackControl(lPlayHandle: LONG; dwControlCode, dwInValue: DWORD; lpOutValue: PDWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>控制录像回放的状态</summary>
/// <param name="lPlayHandle" IO="In">播放句柄，NET_DVR_PlayBackByName、NET_DVR_PlayBackReverseByName或NET_DVR_PlayBackByTime_V40、NET_DVR_PlayBackReverseByTime_V40的返回值</param>
/// <param name="dwControlCode" IO="In">控制录像回放状态命令</param>
/// <param name="lpInBuffer" IO="In">指向输入参数的指针</param>
/// <param name="dwInLen" IO="In">输入参数的长度(抽帧回放控制时必须输入正确的长度，当dwInLen为NET_DVR_TIME_EX结构体大小时，按NET_DVR_TIME_EX处理；当dwInLen为NET_DVR_VOD_DRAWFRAME_PARA结构体大小时，按NET_DVR_VOD_DRAWFRAME_PARA处理)</param>
/// <param name="lpOutBuffer" IO="Out">指向输出参数的指针</param>
/// <param name="lpOutLen" IO="Out">输出参数的长度</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_PlayBackControl_V40(lPlayHandle: LONG; dwControlCode: DWORD; lpInBuffer: LPVOID; dwInLen: DWORD; lpOutBuffer: LPVOID; lpOutLen: PDWORD): BOOL; stdcall; external 'HCNetSDK.dll';

/// <summary>停止回放录像文件</summary>
/// <param name="lPlayHandle" IO="In">回放句柄，NET_DVR_PlayBackByName、NET_DVR_PlayBackByTime_V40或者NET_DVR_PlayBackReverseByName、NET_DVR_PlayBackReverseByTime_V40的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopPlayBack(lPlayHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

{-------- 下载 --------}

/// <summary>按文件名下载录像文件</summary>
/// <param name="lUserID" IO="In"></param>
/// <param name="sDVRFileName" IO="In">要下载的录像文件名，文件名长度需小于100字节</param>
/// <param name="sSavedFileName" IO="In">下载后保存到PC机的文件路径，需为绝对路径(包括文件名)</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopGetFile等函数的参数</returns>
function NET_DVR_GetFileByName(lUserID: LONG; sDVRFileName, sSavedFileName: PAnsiChar): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>按时间下载录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login_V40的返回值</param>
/// <param name="lChannel" IO="In">通道号</param>
/// <param name="lpStartTime" IO="In">开始时间</param>
/// <param name="lpStopTime" IO="In">结束时间</param>
/// <param name="sSavedFileName" IO="In">下载后保存到PC机的文件路径，需为绝对路径</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopGetFile等函数的参数</returns>
function NET_DVR_GetFileByTime(lUserID, lChannel: LONG; lpStartTime, lpStopTime: LPNET_DVR_TIME; sSavedFileName: PAnsiChar): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>按时间下载录像文件</summary>
/// <param name="lUserID" IO="In">NET_DVR_Login或者NET_DVR_Login_V30的返回值</param>
/// <param name="sSavedFileName" IO="In">下载后保存到PC机的文件路径，需为绝对路径(包括文件名)</param>
/// <param name="pDownloadCond" IO="In">下载条件</param>
/// <returns>-1表示失败，其他值作为NET_DVR_StopGetFile等函数的参数</returns>
function NET_DVR_GetFileByTime_V40(lUserID: LONG; sSavedFileName: PAnsiChar; pDownloadCond: LPNET_DVR_PLAYCOND): LONG; stdcall; external 'HCNetSDK.dll';

/// <summary>停止下载录像文件</summary>
/// <param name="lFileHandle" IO="In">下载句柄，NET_DVR_GetFileByName、NET_DVR_GetFileByTime_V40或NET_DVR_GetFileByTime的返回值</param>
/// <returns>True成功，False失败</returns>
function NET_DVR_StopGetFile(lFileHandle: LONG): BOOL; stdcall; external 'HCNetSDK.dll';

implementation

end.


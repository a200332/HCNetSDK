(*******************************************************************************
  名  称: 海康威视设备封装
  作  者: JXQ
  日  期: 2018.12.20
*******************************************************************************)

unit HIK_Device;

interface

uses
  SysUtils, Windows, Messages, Classes, HCNetSDK, jpeg, plaympeg4, ExtCtrls;

type

  /// <summary>通用函数集</summary>
  THIK_PubFunc = class(TObject)
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>获取对应错误码的错误信息</summary>
    /// <param name="AErrCode">SDK返回的错误码</param>
    /// <returns>错误码对应的字符串</returns>
    function GetErrorInfo(ErrCode: DWORD): string;
    /// <summary>获取最后一次的错误信息</summary>
    /// <returns>最后一次错误码对应的字符串</returns>
    function GetLastErrInfo: string;
    /// <summary>设置连接超时时间及重连间隔</summary>
    /// <param name="AWaitTime">超时时间，单位毫秒，取值范围[300,75000]</param>
    /// <param name="AReconnTime">重连间隔，单位毫秒</param>
    procedure SetConnectTime(WaitTime: Integer = 5000; ReconnTime: Integer = 10000);
    /// <summary>设置SDK日志文件记录方式</summary>
    /// <param name="LogPath">日志文件路径，必须是绝对路径</param>
    /// <param name="LogLevel">日志等级：0-关闭，1-Error，2-Error/Debug，3-All</param>
    procedure SetSDKLog(LogPath: string; LogLevel: DWORD = 0);
    /// <summary>时间转换成时间结构体</summary>
    /// <param name="ADateTime">时间</param>
    /// <returns>时间结构体</returns>
    function DateTimeToDVRTime(ADateTime: TDateTime): NET_DVR_TIME;
    /// <summary>时间结构体转换成时间</summary>
    /// <param name="ADVRTime">时间结构体</param>
    /// <returns>时间</returns>
    function DVRTimeToDateTime(ADVRTime: NET_DVR_TIME): TDateTime;
  end;

  /// <summary>海康设备基类</summary>
  THIK_DeviceBase = class(TObject)
  private
    FPubFunc: THIK_PubFunc;
    FOnExept: ExceptionCallBack;
    FOnAlarm: MSGCallBack;
    function GetLoginState: Boolean;
    procedure SetOnExept(const Value: ExceptionCallBack);
    procedure SetOnAlarm(const Value: MSGCallBack);
    function GetAlarmState: Boolean;
  protected
    mUserID: LONG;                          //登录ID
    mDeviceInfo: NET_DVR_DEVICEINFO_V30;    //设备信息
    mAlarmHandle: LONG;                     //布防句柄
    mAlarmParam: NET_DVR_SETUPALARM_PARAM;  //布防参数
    /// <summary>报警布防</summary>
    /// <remarks>不能直接使用，需在子类中设置完报警参数后调用该方法才能完成布防</remarks>
    procedure OpenCustomAlarm;
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>登陆</summary>
    /// <param name="AIP">设备IP</param>
    /// <param name="APort">设备端口</param>
    /// <param name="AUserName">用户名</param>
    /// <param name="APassword">密码</param>
    /// <returns>是否登陆成功</returns>
    function Login(AIP: string; APort: Word; AUserName, APassword: string): Boolean; virtual;
    /// <summary>登出</summary>
    procedure Logout; virtual;
    /// <summary>获取设备时间</summary>
    /// <returns>设备时间</returns>
    function GetDeviceTime: TDateTime;
    /// <summary>报警撤防</summary>
    procedure CloseAlarm;
    /// <summary>通用函数</summary>
    property PubFunc: THIK_PubFunc read FPubFunc;
    /// <summary>是否已登录</summary>
    property IsLogin: Boolean read GetLoginState;
    /// <summary>是否已经布防</summary>
    property IsAlarm: Boolean read GetAlarmState;
    /// <summary>异常事件，需要写自己的回调函数进行处理</summary>
    /// <remarks>回调函数必须使用stdcall关键字，且必须使用全局函数，至少是单元级全局</remarks>
    property OnExept: ExceptionCallBack read FOnExept write SetOnExept;
    /// <summary>报警事件，需要写自己的回调函数进行处理</summary>
    /// <remarks>回调函数必须使用stdcall关键字，且必须使用全局函数，至少是单元级全局</remarks>
    property OnAlarm: MSGCallBack read FOnAlarm write SetOnAlarm;
  end;

  /// <summary>海康IPC摄像头</summary>
  THIK_IPC_Device = class(THIK_DeviceBase)
  private
    mPlaySound: Boolean;
    function GetRealPlayState: Boolean;
  protected
    mRealHandle: LONG;                  //预览句柄
    mPreviewInfo: NET_DVR_PREVIEWINFO;  //预览参数
    mJpegPara: NET_DVR_JPEGPARA;        //设备抓图参数
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>登出</summary>
    procedure Logout; override;
    /// <summary>设备抓图并存为文件</summary>
    /// <param name="APicName">抓图文件名(jpg)</param>
    /// <returns>是否保存成功</returns>
    function SavePic(APicName: string): Boolean; overload;
    /// <summary>设备抓图并存入jpg对象</summary>
    /// <param name="AJpgPic">要存抓图的jpg对象</param>
    /// <returns>是否保存成功</returns>
    function SavePic(out AJpgPic: TJPEGImage): Boolean; overload;
    /// <summary>打开预览，使用SDK解码显示</summary>
    /// <param name="ViewWindowHandle">显示预览的窗口句柄</param>
    /// <param name="PlaySound">预览时播放声音</param>
    procedure OpenPreview(ViewWindowHandle: HWND; PlaySound: Boolean = False);
    /// <summary>关闭预览</summary>
    procedure ClosePreview;
    /// <summary>预览时播放声音</summary>
    procedure OpenSound;
    /// <summary>关闭声音</summary>
    procedure CloseSound;
    /// <summary>预览抓图并存为文件</summary>
    /// <param name="APicName">抓图文件名</param>
    /// <returns>是否保存成功</returns>
    function SaveRealPic(APicName: string): Boolean;
    /// <summary>是否正在实时预览</summary>
    property IsRealPlay: Boolean read GetRealPlayState;
  end;

  /// <summary>硬盘录像机回放标记</summary>
  THIK_DVRFlag = record
    Recording: Boolean;     //是否在录像
    PlayHandle: LONG;       //播放句柄
    WinHWND: HWND;          //回放窗口句柄
    PauseFlag: Integer;     //暂停标记
    PlayPos: Integer;       //播放进度
    DownloadHandle: LONG;   //下载句柄
    DownloadPos: Integer;   //下载进度
  end;

  /// <summary>海康硬盘录像机</summary>
  THIK_DVR_Device = class(THIK_DeviceBase)
  private
    tmrState: TTimer;
    mVodPara: NET_DVR_VOD_PARA;       //回放参数
    mDownloadPara: NET_DVR_PLAYCOND;  //下载参数
    function GetIsRecording(AChannel: Integer): Boolean;
    function GetChannle(AIndex: Integer): LONG;
    /// <summary>初始化标记</summary>
    procedure InitFlag;
    /// <summary>获取进度</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    /// <param name="AType">进度类型：1-回放，2-下载</param>
    /// <returns>进度值</returns>
    function GetPos(AChannel, AType: Integer): Integer;
    procedure tmrStateTimer(Sender: TObject);
  protected
    mFlag: array of THIK_DVRFlag;
    /// <summary>内部通道号，与设备型号有关</summary>
    property Channle[AIndex: Integer]: LONG read GetChannle;
  public
    constructor Create;
    destructor Destroy; override;
    function Login(AIP: string; APort: Word; AUserName, APassword: string): Boolean; override;
    procedure Logout; override;
    /// <summary>开始录像</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    /// <returns>是否操作成功</returns>
    function StartRecord(AChannel: LONG): Boolean;
    /// <summary>停止录像</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    procedure StopRecord(AChannel: LONG);
    /// <summary>回放录像</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    /// <param name="ABeginTime">开始时间</param>
    /// <param name="AEndTime">结束时间</param>
    /// <param name="APlayHandle">回放窗口句柄</param>
    /// <returns>是否操作成功</returns>
    function PlayStart(AChannel: LONG; ABeginTime, AEndTime: TDateTime; APlayHandle: HWND): Boolean;
    /// <summary>暂停回放</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    procedure PlayPause(AChannel: LONG);
    /// <summary>停止回放</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    procedure PlayStop(AChannel: LONG);
    /// <summary>下载录像</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    /// <param name="ABeginTime">开始时间</param>
    /// <param name="AEndTime">结束时间</param>
    /// <param name="AFileName">保存文件名，必须绝对路径</param>
    /// <returns>是否操作成功</returns>
    function DownloadStart(AChannel: LONG; ABeginTime, AEndTime: TDateTime; AFileName: string): Boolean;
    /// <summary>停止下载</summary>
    /// <param name="AChannel">通道号，从1开始</param>
    procedure DownloadStop(AChannel: LONG);
    /// <summary>是否正在录像</summary>
    property IsRecording[AChannel: Integer]: Boolean read GetIsRecording;
//    property On
  end;

  /// <summary>海康播放器</summary>
  THIK_Player = class(TObject)
  private
    FViewHWND: HWND;
    function GetVolume: Integer;
    procedure SetVolume(AVolume: Integer);
  protected
    mPort: LONG;
    mPauseFlag: Cardinal; //暂停标记
    mIsOpen: Boolean; //是否已打开文件
    mIsPlay: Boolean; //是否正在播放
  public
    constructor Create;
    destructor Destroy; override;
    /// <summary>打开录像文件</summary>
    /// <param name="AFileName" IO="In">文件名</param>
    /// <returns>是否打开成功</returns>
    function OpenVideo(AFileName: string): Boolean;
    /// <summary>关闭录像文件</summary>
    procedure CloseVideo;
    /// <summary>开始播放</summary>
    /// <param name="AViewHWND" IO="In">显示所在窗口的句柄</param>
    procedure Play(AViewHWND: HWND);
    /// <summary>暂停/继续</summary>
    procedure Pause;
    /// <summary>停止播放</summary>
    procedure Stop;
    /// <summary>获取错误信息</summary>
    /// <returns>错误信息</returns>
    function GetLastErrorInfo: string;
    /// <summary>显示的窗口句柄</summary>
    property ViewHWND: HWND read FViewHWND;
    /// <summary>音量</summary>
    property Volume: Integer read GetVolume write SetVolume;
  end;

implementation

uses
  DateUtils;

var
  HIK_PubFunc: THIK_PubFunc = nil;  //通用函数实例，公用，应保持仅此一个实例

{======== 默认的回调函数实现 ==================================================}

/// <summary>默认的索引回调函数</summary>
/// <param name="nPort" IO="Out">播放器通道号</param>
/// <param name="nUser" IO="Out">用户数据</param>

procedure FileRefDoneCB_Default(nPort: DWORD; nUser: DWORD); stdcall;
begin

end;

{ THIK_PubFunc }

constructor THIK_PubFunc.Create;
begin
  NET_DVR_Init;
end;

function THIK_PubFunc.DateTimeToDVRTime(ADateTime: TDateTime): NET_DVR_TIME;
var
  YY, MM, DD, HH, NN, SS, ZZZ: Word;
begin
  DecodeDateTime(ADateTime, YY, MM, DD, HH, NN, SS, ZZZ);
  Result.dwYear := YY;
  Result.dwMonth := MM;
  Result.dwDay := DD;
  Result.dwHour := HH;
  Result.dwMinute := NN;
  Result.dwSecond := SS;
end;

destructor THIK_PubFunc.Destroy;
begin
  NET_DVR_Cleanup;
  inherited Destroy;
end;

function THIK_PubFunc.DVRTimeToDateTime(ADVRTime: NET_DVR_TIME): TDateTime;
var
  YY, MM, DD, HH, NN, SS, ZZZ: Word;
begin
  YY := ADVRTime.dwYear;
  MM := ADVRTime.dwMonth;
  DD := ADVRTime.dwDay;
  HH := ADVRTime.dwHour;
  NN := ADVRTime.dwMinute;
  SS := ADVRTime.dwSecond;
  ZZZ := 0;
  Result := EncodeDateTime(YY, MM, DD, HH, NN, SS, ZZZ);
end;

function THIK_PubFunc.GetErrorInfo(ErrCode: DWORD): string;
begin
  Result := '';
  case ErrCode of
    NET_DVR_PASSWORD_ERROR:
      Result := '用户名密码错误';
    NET_DVR_NOENOUGHPRI:
      Result := '权限不足';
    NET_DVR_NOINIT:
      Result := 'SDK未初始化';
    NET_DVR_CHANNEL_ERROR:
      Result := '通道号错误';
    NET_DVR_OVER_MAXLINK:
      Result := '连接到设备的用户个数超过最大';
    NET_DVR_VERSIONNOMATCH:
      Result := '版本不匹配';
    NET_DVR_NETWORK_FAIL_CONNECT:
      Result := '连接设备失败';
    NET_DVR_NETWORK_SEND_ERROR:
      Result := '向设备发送失败';
    NET_DVR_NETWORK_RECV_ERROR:
      Result := '从设备接收数据失败';
    NET_DVR_NETWORK_RECV_TIMEOUT:
      Result := '从设备接收数据超时';
    NET_DVR_NETWORK_ERRORDATA:
      Result := '传送的数据有误';
    NET_DVR_ORDER_ERROR:
      Result := '调用次序错误';
    NET_DVR_COMMANDTIMEOUT:
      Result := '设备命令执行超时';
    NET_DVR_PARAMETER_ERROR:
      Result := '参数错误';
    NET_DVR_NODISK:
      Result := '设备无硬盘';
    NET_DVR_NOSUPPORT:
      Result := '设备不支持';
    NET_DVR_DVRNORESOURCE:
      Result := '设备资源不足';
    NET_DVR_NOSPECFILE:
      Result := '回放时设备没有指定的文件';
    NET_DVR_CREATEFILE_ERROR:
      Result := '创建文件出错';
    NET_DVR_ALLOC_RESOURCE_ERROR:
      Result := 'SDK资源分配错误';
    NET_DVR_AUDIO_MODE_ERROR:
      Result := '声卡模式错误';
    NET_DVR_NOENOUGH_BUF:
      Result := '缓冲区太小';
    NET_DVR_CREATESOCKET_ERROR:
      Result := '创建SOCKET出错';
    NET_DVR_MAX_NUM:
      Result := '个数达到最大';
    NET_DVR_USERNOTEXIST:
      Result := '用户不存在';
    NET_DVR_PLAYERFAILED:
      Result := '调用播放库中某个函数失败';
    NET_DVR_MAX_USERNUM:
      Result := '登录设备的用户数达到最大';
    NET_DVR_GETLOCALIPANDMACFAIL:
      Result := '获得本地PC的IP地址或物理地址失败';
    NET_DVR_LOADPLAYERSDKFAILED:
      Result := '载入当前目录下Player Sdk出错';
    NET_DVR_LOADPLAYERSDKPROC_ERROR:
      Result := '找不到Player Sdk中某个函数入口';
    NET_DVR_LOADDSSDKFAILED:
      Result := '载入当前目录下DSsdk出错';
    NET_DVR_LOADDSSDKPROC_ERROR:
      Result := '找不到DsSdk中某个函数入口';
    NET_DVR_BINDSOCKET_ERROR:
      Result := '绑定套接字失败';
    NET_DVR_SOCKETCLOSE_ERROR:
      Result := 'socket连接中断';
    NET_DVR_SOCKETLISTEN_ERROR:
      Result := '监听失败';
    NET_DVR_PROGRAM_EXCEPTION:
      Result := '程序异常';
    NET_DVR_IPCHAN_NOTALIVE:
      Result := '预览时外接IP通道不在线';
    NET_DVR_RTSP_SDK_ERROR:
      Result := '加载标准协议通讯库StreamTransClient失败';
    NET_DVR_CONVERT_SDK_ERROR:
      Result := '加载转封装库失败';
  end;
  if ErrCode > 0 then
    Result := '0x' + IntToHex(ErrCode, 8) + ':' + Result;
end;

function THIK_PubFunc.GetLastErrInfo: string;
begin
  Result := GetErrorInfo(NET_DVR_GetLastError);
end;

procedure THIK_PubFunc.SetConnectTime(WaitTime, ReconnTime: Integer);
begin
  if WaitTime < 300 then
    WaitTime := 300
  else if WaitTime > 75000 then
    WaitTime := 75000;
  NET_DVR_SetConnectTime(WaitTime);
  NET_DVR_SetReconnect(ReconnTime);
end;

procedure THIK_PubFunc.SetSDKLog(LogPath: string; LogLevel: DWORD);
begin
  NET_DVR_SetLogToFile(LogLevel, PAnsiChar(AnsiString(LogPath)));
end;

{ THIK_DeviceBase }

procedure THIK_DeviceBase.CloseAlarm;
begin
  if IsAlarm then
    if NET_DVR_CloseAlarmChan_V30(mAlarmHandle) then
      mAlarmHandle := -1;
end;

constructor THIK_DeviceBase.Create;
begin
  inherited Create;
  NET_DVR_Init;
  //通用函数实例存在则直接引用，不存在则先创建再引用
  if not Assigned(HIK_PubFunc) then
    HIK_PubFunc := THIK_PubFunc.Create;
  FPubFunc := HIK_PubFunc;
  mUserID := -1;
  FillChar(mDeviceInfo, SizeOf(NET_DVR_DEVICEINFO_V30), 0);
  FillChar(mAlarmParam, SizeOf(NET_DVR_SETUPALARM_PARAM), 0);
  FOnExept := nil;
  FOnAlarm := nil;
end;

destructor THIK_DeviceBase.Destroy;
begin
  CloseAlarm;
  Logout;
  FPubFunc := nil;
  NET_DVR_Cleanup;
  inherited Destroy;
end;

function THIK_DeviceBase.GetAlarmState: Boolean;
begin
  Result := mAlarmHandle >= 0;
end;

function THIK_DeviceBase.GetDeviceTime: TDateTime;
var
  tmpTime: NET_DVR_TIME;
  nLen: DWORD;
begin
  if IsLogin then
  begin
    if NET_DVR_GetDVRConfig(mUserID, NET_DVR_GET_TIMECFG, $FFFFFFFF, @tmpTime, SizeOf(tmpTime), @nLen) then
      Result := PubFunc.DVRTimeToDateTime(tmpTime);
  end;
end;

function THIK_DeviceBase.GetLoginState: Boolean;
begin
  Result := mUserID >= 0;
end;

function THIK_DeviceBase.Login(AIP: string; APort: Word; AUserName, APassword: string): Boolean;
begin
  if IsLogin then
    Result := False
  else
  begin
    mUserID := NET_DVR_Login_V30(PAnsiChar(AnsiString(AIP)), APort, PAnsiChar(AnsiString(AUserName)), PAnsiChar(AnsiString(APassword)), @mDeviceInfo);
    Result := IsLogin;
  end;
end;

procedure THIK_DeviceBase.Logout;
begin
  if IsLogin then
    if NET_DVR_Logout(mUserID) then
      mUserID := -1;
end;

procedure THIK_DeviceBase.OpenCustomAlarm;
begin
  if IsLogin and (not IsAlarm) then
  begin
    //设置报警参数
    mAlarmParam.dwSize := SizeOf(NET_DVR_SETUPALARM_PARAM);
    if Assigned(FOnAlarm) then
      mAlarmHandle := NET_DVR_SetupAlarmChan_V41(mUserID, @mAlarmParam);
  end;
end;

procedure THIK_DeviceBase.SetOnAlarm(const Value: MSGCallBack);
begin
  FOnAlarm := Value;
  if Assigned(FOnAlarm) then
    NET_DVR_SetDVRMessageCallBack_V31(@FOnAlarm, Pointer(Self));
end;

procedure THIK_DeviceBase.SetOnExept(const Value: ExceptionCallBack);
begin
  FOnExept := Value;
  if Assigned(FOnExept) then
    NET_DVR_SetExceptionCallBack_V30(WM_NULL, 0, @FOnExept, Pointer(Self));
end;

{ THIK_IPC_Device }

procedure THIK_IPC_Device.ClosePreview;
begin
  if IsRealPlay then
  begin
    CloseSound;
    if NET_DVR_StopRealPlay(mRealHandle) then
      mRealHandle := -1;
  end;
end;

procedure THIK_IPC_Device.CloseSound;
begin
  if NET_DVR_CloseSound then
    mPlaySound := False;
end;

constructor THIK_IPC_Device.Create;
begin
  inherited Create;
  mRealHandle := -1;
  FillChar(mPreviewInfo, SizeOf(NET_DVR_PREVIEWINFO), 0);
  //初始化设备抓图参数
  mJpegPara.wPicSize := $FF;  //抓图尺寸 $FF-使用当前码流分辨率
  mJpegPara.wPicQuality := 0; //抓图质量 0-最好
end;

destructor THIK_IPC_Device.Destroy;
begin

  inherited Destroy;
end;

function THIK_IPC_Device.GetRealPlayState: Boolean;
begin
  Result := mRealHandle >= 0;
end;

procedure THIK_IPC_Device.Logout;
begin
  ClosePreview;
  inherited Logout;
end;

procedure THIK_IPC_Device.OpenPreview(ViewWindowHandle: HWND; PlaySound: Boolean);
begin
  if IsLogin and (not IsRealPlay) then
  begin
    //设置预览参数
    mPreviewInfo.lChannel := 1;                 //通道
    mPreviewInfo.dwStreamType := 0;             //主码流
    mPreviewInfo.dwLinkMode := 0;               //使用TCP连接
    mPreviewInfo.hPlayWnd := ViewWindowHandle;  //播放句柄
    //设置预览抓图格式为 jpg
    NET_DVR_SetCapturePictureMode(Ord(JPEG_MODE));
    //开始预览
    mRealHandle := NET_DVR_RealPlay_V40(mUserID, @mPreviewInfo, nil, nil);
    if PlaySound then
      OpenSound;
  end;
end;

procedure THIK_IPC_Device.OpenSound;
begin
  //需等待接收到预览码流后再播放声音才有效，为简化直接 Sleep
  if IsRealPlay and (not mPlaySound) then
  begin
    Sleep(1000);
    mPlaySound := NET_DVR_OpenSound(mRealHandle);
    //设置音量
    NET_DVR_Volume(mRealHandle, $FFFF);
  end;
end;

function THIK_IPC_Device.SavePic(APicName: string): Boolean;
begin
  Result := False;
  if IsLogin then
    Result := NET_DVR_CaptureJPEGPicture(mUserID, 1, @mJpegPara, PAnsiChar(AnsiString(APicName)));
end;

function THIK_IPC_Device.SavePic(out AJpgPic: TJPEGImage): Boolean;
var
  ms: TMemoryStream;
  tmpBuff: array[0..512 * 1024 - 1] of Byte;  //512K
  DataLen: DWORD;
begin
  Result := False;
  if IsLogin then
  try
    ms := TMemoryStream.Create;
    if NET_DVR_CaptureJPEGPicture_NEW(mUserID, 1, @mJpegPara, @tmpBuff, SizeOf(tmpBuff), @DataLen) then
    begin
      ms.Write(tmpBuff, DataLen);
      ms.Position := 0;
      AJpgPic.LoadFromStream(ms);
      Result := True;
    end;
  finally
    ms.Free;
  end;
end;

function THIK_IPC_Device.SaveRealPic(APicName: string): Boolean;
begin
  Result := False;
  if IsLogin and IsRealPlay then
    Result := NET_DVR_CapturePictureBlock(mRealHandle, PAnsiChar(AnsiString(APicName)), 0);
end;

{ THIK_DVR_Device }

constructor THIK_DVR_Device.Create;
begin
  inherited Create;
  tmrState := TTimer.Create(nil);
  tmrState.Enabled := False;
  tmrState.Interval := 1000;
  tmrState.OnTimer := tmrStateTimer;
end;

destructor THIK_DVR_Device.Destroy;
begin
  tmrState.Free;
  inherited Destroy;
end;

function THIK_DVR_Device.DownloadStart(AChannel: LONG; ABeginTime, AEndTime: TDateTime; AFileName: string): Boolean;
var
  tmpDownloadHandle: LONG;
begin
  Result := False;
  if IsLogin then
  begin
    DownloadStop(AChannel);
    FillChar(mDownloadPara, SizeOf(NET_DVR_PLAYCOND), 0);
    mDownloadPara.dwChannel := Channle[AChannel];
    mDownloadPara.struStartTime := Self.PubFunc.DateTimeToDVRTime(ABeginTime);
    mDownloadPara.struStopTime := Self.PubFunc.DateTimeToDVRTime(AEndTime);
    tmpDownloadHandle := NET_DVR_GetFileByTime_V40(mUserID, PAnsiChar(AnsiString(AFileName)), @mDownloadPara);
    if tmpDownloadHandle >= 0 then
    begin
      //下载录像
      Result := NET_DVR_PlayBackControl_V40(tmpDownloadHandle, NET_DVR_PLAYSTART, 0, 0, 0, 0);
      //记录标记
      mFlag[AChannel - 1].DownloadHandle := tmpDownloadHandle;
    end;
  end;
end;

procedure THIK_DVR_Device.DownloadStop(AChannel: LONG);
begin
  if mFlag[AChannel - 1].DownloadHandle >= 0 then
  begin
    //关闭回放
    if NET_DVR_StopPlayBack(mFlag[AChannel - 1].DownloadHandle) then
      mFlag[AChannel - 1].DownloadHandle := -1;
  end;
end;

function THIK_DVR_Device.GetChannle(AIndex: Integer): LONG;
begin
  if IsLogin then
    Result := AIndex + mDeviceInfo.byStartDChan - 1
  else
    Result := -1;
end;

function THIK_DVR_Device.GetIsRecording(AChannel: Integer): Boolean;
begin
  if IsLogin then
    Result := mFlag[AChannel - 1].Recording
  else
    Result := False;
end;

function THIK_DVR_Device.GetPos(AChannel, AType: Integer): Integer;
begin
  Result := -1;
  case AType of
    1: //回放
      begin
        if not NET_DVR_PlayBackControl_V40(mFlag[AChannel - 1].PlayHandle, NET_DVR_PLAYGETPOS, 0, 0, @Result, 0) then
          Exit;
        if Result > 100 then
          Result := -1;
        //回放结束则停止
        if Result = 100 then
          PlayStop(AChannel);
      end;
    2: //下载
      begin
        if not NET_DVR_PlayBackControl_V40(mFlag[AChannel - 1].DownloadHandle, NET_DVR_PLAYGETPOS, 0, 0, @Result, 0) then
          Exit;
        if Result > 100 then
          Result := -1;
        //下载结束则停止
        if Result = 100 then
          DownloadStop(AChannel);
      end;
  end;
end;

function THIK_DVR_Device.Login(AIP: string; APort: Word; AUserName, APassword: string): Boolean;
begin
  Result := inherited Login(AIP, APort, AUserName, APassword);
  if Result then
  begin
    //初始化标记
    InitFlag;
    tmrState.Enabled := True;
  end;
end;

procedure THIK_DVR_Device.Logout;
var
  i: Integer;
begin
  tmrState.Enabled := False;
  for i := 1 to mDeviceInfo.byIPChanNum - 1 do
  begin
    DownloadStop(i);
    PlayStop(i);
    StopRecord(i);
  end;
  inherited Logout;
end;

procedure THIK_DVR_Device.InitFlag;
var
  i: Integer;
begin
  SetLength(mFlag, mDeviceInfo.byIPChanNum);
  for i := 0 to Length(mFlag) - 1 do
  begin
    mFlag[i].Recording := False;
    mFlag[i].PlayHandle := -1;
    mFlag[i].WinHWND := 0;
    mFlag[i].PauseFlag := 0;
    mFlag[i].PlayPos := -1;
    mFlag[i].DownloadHandle := -1;
    mFlag[i].DownloadPos := -1;
  end;
end;

procedure THIK_DVR_Device.PlayPause(AChannel: LONG);
begin
  mFlag[AChannel - 1].PauseFlag := mFlag[AChannel - 1].PauseFlag + 1;
  if mFlag[AChannel - 1].PlayHandle >= 0 then
    if mFlag[AChannel - 1].PauseFlag mod 2 = 0 then
      NET_DVR_PlayBackControl_V40(mFlag[AChannel - 1].PlayHandle, NET_DVR_PLAYRESTART, 0, 0, 0, 0)
    else
      NET_DVR_PlayBackControl_V40(mFlag[AChannel - 1].PlayHandle, NET_DVR_PLAYPAUSE, 0, 0, 0, 0);
end;

function THIK_DVR_Device.PlayStart(AChannel: LONG; ABeginTime, AEndTime: TDateTime; APlayHandle: HWND): Boolean;
var
  tmpPlayHandle: LONG;
begin
  Result := False;
  if IsLogin then
  begin
    PlayStop(AChannel);
    FillChar(mVodPara, SizeOf(NET_DVR_VOD_PARA), 0);
    mVodPara.struIDInfo.dwSize := SizeOf(NET_DVR_STREAM_INFO);
    mVodPara.struIDInfo.dwChannel := Channle[AChannel];
    mVodPara.struBeginTime := Self.PubFunc.DateTimeToDVRTime(ABeginTime);
    mVodPara.struEndTime := Self.PubFunc.DateTimeToDVRTime(AEndTime);
    mVodPara.HWND := APlayHandle;
    tmpPlayHandle := NET_DVR_PlayBackByTime_V40(mUserID, @mVodPara);
    if tmpPlayHandle >= 0 then
    begin
      //回放录像
      Result := NET_DVR_PlayBackControl_V40(tmpPlayHandle, NET_DVR_PLAYSTART, 0, 0, 0, 0);
      //打开声音
      NET_DVR_PlayBackControl_V40(tmpPlayHandle, NET_DVR_PLAYSTARTAUDIO, 0, 0, 0, 0);
      //记录标记
      mFlag[AChannel - 1].PlayHandle := tmpPlayHandle;
      mFlag[AChannel - 1].WinHWND := APlayHandle;
      mFlag[AChannel - 1].PauseFlag := 0;
    end;
  end;
end;

procedure THIK_DVR_Device.PlayStop(AChannel: LONG);
begin
  if mFlag[AChannel - 1].PlayHandle >= 0 then
  begin
    //关闭声音
    NET_DVR_PlayBackControl_V40(mFlag[AChannel - 1].PlayHandle, NET_DVR_PLAYSTOPAUDIO, 0, 0, 0, 0);
    //关闭回放
    if NET_DVR_StopPlayBack(mFlag[AChannel - 1].PlayHandle) then
    begin
      mFlag[AChannel - 1].PlayHandle := -1;
      mFlag[AChannel - 1].WinHWND := 0;
      mFlag[AChannel - 1].PauseFlag := 0;
    end;
  end;
end;

function THIK_DVR_Device.StartRecord(AChannel: LONG): Boolean;
begin
  Result := False;
  if IsLogin then
  begin
    if AChannel in [mDeviceInfo.byStartChan..mDeviceInfo.byIPChanNum] then
      Result := NET_DVR_StartDVRRecord(mUserID, Channle[AChannel], 0);
    mFlag[AChannel - 1].Recording := Result;
  end;
end;

procedure THIK_DVR_Device.StopRecord(AChannel: LONG);
begin
  if IsRecording[AChannel] then
    if AChannel in [mDeviceInfo.byStartChan..mDeviceInfo.byIPChanNum] then
      if NET_DVR_StopDVRRecord(mUserID, AChannel + mDeviceInfo.byStartDChan - 1) then
        mFlag[AChannel - 1].Recording := False;
end;

procedure THIK_DVR_Device.tmrStateTimer(Sender: TObject);
var
  i: Integer;
begin
  tmrState.Enabled := False;
  for i := 0 to Length(mFlag) - 1 do
  begin
    if mFlag[i].PlayHandle >= 0 then
      mFlag[i].PlayPos := GetPos(i + 1, 1);
    if mFlag[i].DownloadHandle >= 0 then
      mFlag[i].DownloadPos := GetPos(i + 1, 2);
  end;
  tmrState.Enabled := True;
end;

{ THIK_Player }

procedure THIK_Player.CloseVideo;
begin
  Stop;
  if mIsOpen then
    PlayM4_CloseFile(mPort);
end;

constructor THIK_Player.Create;
begin
  inherited Create;
  FViewHWND := 0;
  mPort := -1;
  mIsOpen := False;
  mIsPlay := False;
  if PlayM4_GetPort(@mPort) then
  begin
    //建立索引
    PlayM4_SetFileRefCallBack(mPort, @FileRefDoneCB_Default, Cardinal(Pointer(Self)));
  end
  else
    Abort;
end;

destructor THIK_Player.Destroy;
begin
  if mPort >= 0 then
  begin
    CloseVideo;
    PlayM4_FreePort(mPort);
  end;
  inherited Destroy;
end;

function THIK_Player.GetLastErrorInfo: string;
var
  ErrorCode: Cardinal;
begin
  ErrorCode := PlayM4_GetLastError(mPort);
  case ErrorCode of
    PLAYM4_PARA_OVER:
      Result := '输入参数非法';
    PLAYM4_ORDER_ERROR:
      Result := '调用顺序不对';
    PLAYM4_TIMER_ERROR:
      Result := '多媒体时钟设置失败';
    PLAYM4_DEC_VIDEO_ERROR:
      Result := '视频解码失败';
    PLAYM4_DEC_AUDIO_ERROR:
      Result := '音频解码失败';
    PLAYM4_ALLOC_MEMORY_ERROR:
      Result := '分配内存失败';
    PLAYM4_OPEN_FILE_ERROR:
      Result := '文件操作失败';
    PLAYM4_CREATE_OBJ_ERROR:
      Result := '创建线程事件等失败';
    PLAYM4_CREATE_DDRAW_ERROR:
      Result := '创建DierctDraw失败';
    PLAYM4_CREATE_OFFSCREEN_ERROR:
      Result := '创建后端缓存失败';
    PLAYM4_BUF_OVER:
      Result := '缓冲区满，输入流失败';
    PLAYM4_CREATE_SOUND_ERROR:
      Result := '创建音频设备失败';
    PLAYM4_SET_VOLUME_ERROR:
      Result := '设置音量失败';
    PLAYM4_SUPPORT_FILE_ONLY:
      Result := '只能在播放文件时才能使用此接口';
    PLAYM4_SUPPORT_STREAM_ONLY:
      Result := '只能在播放流时才能使用此接口';
    PLAYM4_SYS_NOT_SUPPORT:
      Result := '不支持';
    PLAYM4_FILEHEADER_UNKNOWN:
      Result := '没有文件头';
    PLAYM4_VERSION_INCORRECT:
      Result := '解码器和编码器版本不对应';
    PALYM4_INIT_DECODER_ERROR:
      Result := '初始化解码器失败';
    PLAYM4_CHECK_FILE_ERROR:
      Result := '文件太短或码流无法识别';
    PLAYM4_INIT_TIMER_ERROR:
      Result := '初始化多媒体时钟失败';
    PLAYM4_BLT_ERROR:
      Result := '位拷贝失败(32位播放库下容易出现此问题，主要是渲染创建失败）';
    PLAYM4_UPDATE_ERROR:
      Result := '显示overlay失败';
    PLAYM4_OPEN_FILE_ERROR_MULTI:
      Result := '打开文件失败，流类型是复合流';
    PLAYM4_OPEN_FILE_ERROR_VIDEO:
      Result := '打开文件失败，流类型是视频流';
    PLAYM4_JPEG_COMPRESS_ERROR:
      Result := 'JPEG压缩失败';
    PLAYM4_EXTRACT_NOT_SUPPORT:
      Result := '文件不支持';
    PLAYM4_EXTRACT_DATA_ERROR:
      Result := '数据错误';
    PLAYM4_SECRET_KEY_ERROR:
      Result := '解码密钥错误';
    PLAYM4_DECODE_KEYFRAME_ERROR:
      Result := '解码关键帧失败';
    PLAYM4_NEED_MORE_DATA:
      Result := '数据不足';
    PLAYM4_INVALID_PORT:
      Result := '无效端口号';
    PLAYM4_NOT_FIND:
      Result := '查找失败';
    PLAYM4_NEED_LARGER_BUFFER:
      Result := '需要更大的缓冲区';
    PLAYM4_FAIL_UNKNOWN:
      Result := '未知的错误';
  end;
  if ErrorCode > 0 then
    Result := Format('错误码：0x%s %s', [IntToHex(ErrorCode, 8), Result]);
end;

function THIK_Player.GetVolume: Integer;
var
  tmp: Word;
begin
  tmp := PlayM4_GetVolume(mPort);
  Result := Round(tmp / $FFFF * 100);
end;

function THIK_Player.OpenVideo(AFileName: string): Boolean;
begin
  CloseVideo;
  mPauseFlag := 0;
  mIsOpen := PlayM4_OpenFile(mPort, PAnsiChar(AnsiString(AFileName)));
  Result := mIsOpen;
end;

procedure THIK_Player.Pause;
begin
  Inc(mPauseFlag);
  PlayM4_Pause(mPort, mPauseFlag mod 2);
end;

procedure THIK_Player.Play(AViewHWND: HWND);
begin
  if AViewHWND > 0 then
  begin
    Stop;
    FViewHWND := AViewHWND;
    //播放画面
    mIsPlay := PlayM4_Play(mPort, FViewHWND);
    //播放声音
    PlayM4_PlaySound(mPort);
  end;
end;

procedure THIK_Player.SetVolume(AVolume: Integer);
var
  tmp: Word;
begin
  if AVolume <= 0 then
    tmp := $0
  else if AVolume >= 100 then
    tmp := $FFFF
  else
    tmp := Round(AVolume / 100 * $FFFF);
  PlayM4_SetVolume(mPort, tmp);
end;

procedure THIK_Player.Stop;
begin
  if mIsPlay then
  begin
    //停止画面
    PlayM4_Stop(mPort);
    //停止声音
    PlayM4_StopSound;
  end;
end;

initialization

finalization
  //释放通用函数实例
  if Assigned(HIK_PubFunc) then
    FreeAndNil(HIK_PubFunc);

end.


object frmHIKTest: TfrmHIKTest
  Left = 0
  Top = 0
  Caption = #28023#24247'SDK'#27979#35797'Demo'
  ClientHeight = 521
  ClientWidth = 681
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pgc1: TPageControl
    Left = 0
    Top = 0
    Width = 681
    Height = 521
    ActivePage = ts1
    Align = alClient
    TabOrder = 0
    object ts1: TTabSheet
      Caption = #25668#20687#22836
      DesignSize = (
        673
        492)
      object imgIPCPic: TImage
        Left = 3
        Top = 3
        Width = 200
        Height = 150
        Center = True
        Picture.Data = {
          0A544A504547496D61676577020000FFD8FFE000104A46494600010101006000
          600000FFDB004300020101020101020202020202020203050303030303060404
          0305070607070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E
          0F0D0C0E0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
          0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080002000203012200021101031101
          FFC4001F0000010501010101010100000000000000000102030405060708090A
          0BFFC400B5100002010303020403050504040000017D01020300041105122131
          410613516107227114328191A1082342B1C11552D1F02433627282090A161718
          191A25262728292A3435363738393A434445464748494A535455565758595A63
          6465666768696A737475767778797A838485868788898A92939495969798999A
          A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
          D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
          01010101010101010000000000000102030405060708090A0BFFC400B5110002
          0102040403040705040400010277000102031104052131061241510761711322
          328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
          292A35363738393A434445464748494A535455565758595A636465666768696A
          737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
          A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
          E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FE7F
          E8A28A00FFD9}
        Stretch = True
      end
      object btnIPCCreate: TButton
        Left = 541
        Top = 17
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21019#24314'IPC'#23454#20363
        TabOrder = 5
        OnClick = btnIPCCreateClick
      end
      object pnlIPCPreview: TPanel
        Left = 3
        Top = 159
        Width = 520
        Height = 300
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = #23454#26102#39044#35272#30028#38754
        Color = clMoneyGreen
        ParentBackground = False
        TabOrder = 0
      end
      object btnIPCFree: TButton
        Left = 541
        Top = 434
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #37322#25918'IPC'#36164#28304
        TabOrder = 15
        OnClick = btnIPCFreeClick
      end
      object lbledtIPCIP: TLabeledEdit
        Left = 282
        Top = 19
        Width = 121
        Height = 21
        EditLabel.Width = 53
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #25668#20687#22836'IP'
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 1
      end
      object lbledtIPCPort: TLabeledEdit
        Left = 282
        Top = 46
        Width = 121
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #25668#20687#22836#31471#21475
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 2
        Text = '8000'
      end
      object lbledtIPCUser: TLabeledEdit
        Left = 282
        Top = 73
        Width = 121
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #30331#24405#29992#25143#21517
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 3
        Text = 'admin'
      end
      object lbledtIPCPassword: TLabeledEdit
        Left = 282
        Top = 100
        Width = 121
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #30331#24405#23494#30721
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        PasswordChar = '*'
        TabOrder = 4
      end
      object btnIPCLogin: TButton
        Left = 541
        Top = 63
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30331#24405
        TabOrder = 6
        OnClick = btnIPCLoginClick
      end
      object btnIPCLogout: TButton
        Left = 541
        Top = 388
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30331#20986
        TabOrder = 13
        OnClick = btnIPCLogoutClick
      end
      object btnSavePicToFile: TButton
        Left = 541
        Top = 110
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #35774#22791#25235#22270#23384#25991#20214
        TabOrder = 7
        OnClick = btnSavePicToFileClick
      end
      object btnSavePicToJPEG: TButton
        Left = 541
        Top = 156
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #35774#22791#25235#22270#23384'jpg'#23545#35937
        TabOrder = 8
        OnClick = btnSavePicToJPEGClick
      end
      object btnIPCOpenPreview: TButton
        Left = 541
        Top = 202
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #25171#24320#39044#35272
        TabOrder = 9
        OnClick = btnIPCOpenPreviewClick
      end
      object btnIPCClosePreview: TButton
        Left = 541
        Top = 249
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20851#38381#39044#35272
        TabOrder = 10
        OnClick = btnIPCClosePreviewClick
      end
      object btnSaveRealPicToFile: TButton
        Left = 541
        Top = 295
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #39044#35272#25235#22270#23384#25991#20214
        TabOrder = 11
        OnClick = btnSaveRealPicToFileClick
      end
      object statIPC: TStatusBar
        Left = 0
        Top = 473
        Width = 673
        Height = 19
        Panels = <
          item
            Width = 50
          end>
      end
      object btnIPCGetTime: TButton
        Left = 541
        Top = 341
        Width = 123
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #33719#21462#26102#38388
        TabOrder = 12
        OnClick = btnIPCGetTimeClick
      end
    end
    object ts2: TTabSheet
      Caption = #30828#30424#24405#20687#26426
      ImageIndex = 1
      DesignSize = (
        673
        492)
      object shpDVR: TShape
        Left = 520
        Top = 92
        Width = 25
        Height = 25
        Anchors = [akTop, akRight]
        Shape = stEllipse
      end
      object lbledtDVRIP: TLabeledEdit
        Left = 74
        Top = 11
        Width = 121
        Height = 21
        EditLabel.Width = 53
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #24405#20687#26426'IP'
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 0
      end
      object lbledtDVRPort: TLabeledEdit
        Left = 74
        Top = 38
        Width = 121
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #24405#20687#26426#31471#21475
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 1
        Text = '8000'
      end
      object lbledtDVRUser: TLabeledEdit
        Left = 74
        Top = 65
        Width = 121
        Height = 21
        EditLabel.Width = 65
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #30331#24405#29992#25143#21517
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 2
        Text = 'admin'
      end
      object lbledtDVRPassword: TLabeledEdit
        Left = 74
        Top = 92
        Width = 121
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #30331#24405#23494#30721
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        PasswordChar = '*'
        TabOrder = 3
      end
      object btnDVRFree: TButton
        Left = 556
        Top = 434
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #37322#25918'DVR'#36164#28304
        TabOrder = 19
        OnClick = btnDVRFreeClick
      end
      object btnDVRLogin: TButton
        Left = 556
        Top = 55
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30331#24405
        TabOrder = 8
        OnClick = btnDVRLoginClick
      end
      object btnDVRCreate: TButton
        Left = 556
        Top = 17
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21019#24314'DVR'#23454#20363
        TabOrder = 7
        OnClick = btnDVRCreateClick
      end
      object btnLogout: TButton
        Left = 556
        Top = 396
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30331#20986
        TabOrder = 17
        OnClick = btnLogoutClick
      end
      object btnDVRStartRecord: TButton
        Left = 556
        Top = 131
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #24320#22987#24405#20687
        TabOrder = 10
        OnClick = btnDVRStartRecordClick
      end
      object btnDVRStopRecord: TButton
        Left = 556
        Top = 169
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20572#27490#24405#20687
        TabOrder = 11
        OnClick = btnDVRStopRecordClick
      end
      object cbbDVRChannel: TComboBox
        Left = 220
        Top = 11
        Width = 105
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 4
        Text = '1 '#36890#36947
        Items.Strings = (
          '1 '#36890#36947
          '2 '#36890#36947
          '3 '#36890#36947
          '4 '#36890#36947
          '5 '#36890#36947
          '6 '#36890#36947
          '7 '#36890#36947
          '8 '#36890#36947)
      end
      object statDVR: TStatusBar
        Left = 0
        Top = 473
        Width = 673
        Height = 19
        Panels = <
          item
            Width = 50
          end>
      end
      object btnDVRStarPlayBack: TButton
        Left = 556
        Top = 207
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #24320#22987#22238#25918
        TabOrder = 12
        OnClick = btnDVRStarPlayBackClick
      end
      object btnDVRStopPlayBack: TButton
        Left = 556
        Top = 282
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20572#27490#22238#25918
        TabOrder = 14
        OnClick = btnDVRStopPlayBackClick
      end
      object btnDVRPasuePlayBack: TButton
        Left = 556
        Top = 244
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #26242#20572'/'#32487#32493#22238#25918
        TabOrder = 13
        OnClick = btnDVRPasuePlayBackClick
      end
      object lbledtDVRBegin: TLabeledEdit
        Left = 274
        Top = 38
        Width = 143
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #36215#22987#26102#38388
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 5
        Text = '2018-12-28 00:00:00'
      end
      object lbledtDVREnd: TLabeledEdit
        Left = 274
        Top = 65
        Width = 143
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.BiDiMode = bdLeftToRight
        EditLabel.Caption = #32467#26463#26102#38388
        EditLabel.ParentBiDiMode = False
        LabelPosition = lpLeft
        TabOrder = 6
        Text = '2018-12-28 23:59:59'
      end
      object btnDVRStartDownload: TButton
        Left = 556
        Top = 320
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #24320#22987#19979#36733
        TabOrder = 15
        OnClick = btnDVRStartDownloadClick
      end
      object btnDVRStopDownload: TButton
        Left = 556
        Top = 358
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20572#27490#19979#36733
        TabOrder = 16
        OnClick = btnDVRStopDownloadClick
      end
      object btnDVRGetTime: TButton
        Left = 556
        Top = 93
        Width = 105
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #33719#21462#26102#38388
        TabOrder = 9
        OnClick = btnDVRGetTimeClick
      end
      object pnlNVRPlayBack: TPanel
        Left = 3
        Top = 122
        Width = 511
        Height = 337
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = #22238#25918#30028#38754
        Color = clMoneyGreen
        ParentBackground = False
        TabOrder = 20
      end
    end
    object ts3: TTabSheet
      Caption = #25773#25918
      ImageIndex = 2
      DesignSize = (
        673
        492)
      object lbl1: TLabel
        Left = 602
        Top = 244
        Width = 26
        Height = 13
        Anchors = [akTop, akRight]
        Caption = #38899#37327
        ExplicitLeft = 631
      end
      object lbl2: TLabel
        Left = 595
        Top = 271
        Width = 7
        Height = 13
        Anchors = [akTop, akRight]
        Caption = '0'
        ExplicitLeft = 624
      end
      object lbl3: TLabel
        Left = 581
        Top = 423
        Width = 21
        Height = 13
        Anchors = [akTop, akRight]
        Caption = '100'
        ExplicitLeft = 610
      end
      object pnlPlayer: TPanel
        Left = 3
        Top = 3
        Width = 553
        Height = 464
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = #25773#25918#30028#38754
        Color = clMoneyGreen
        ParentBackground = False
        TabOrder = 0
      end
      object btnPlayerOpenFile: TButton
        Left = 578
        Top = 50
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #25171#24320#25991#20214
        TabOrder = 2
        OnClick = btnPlayerOpenFileClick
      end
      object btnPlayerCloseFile: TButton
        Left = 578
        Top = 174
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20851#38381#25991#20214
        TabOrder = 6
        OnClick = btnPlayerCloseFileClick
      end
      object btnPlayerPlay: TButton
        Left = 578
        Top = 81
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #25773#25918
        TabOrder = 3
        OnClick = btnPlayerPlayClick
      end
      object btnPlayerPause: TButton
        Left = 578
        Top = 112
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #26242#20572
        TabOrder = 4
        OnClick = btnPlayerPauseClick
      end
      object btnPlayerStop: TButton
        Left = 578
        Top = 143
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20572#27490
        TabOrder = 5
        OnClick = btnPlayerStopClick
      end
      object btnPlayerCreate: TButton
        Left = 578
        Top = 19
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21019#24314#23454#20363
        TabOrder = 1
        OnClick = btnPlayerCreateClick
      end
      object btnPlayerFree: TButton
        Left = 578
        Top = 205
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #37322#25918#23454#20363
        TabOrder = 7
        OnClick = btnPlayerFreeClick
      end
      object trckbrPlayerVolume: TTrackBar
        Left = 608
        Top = 263
        Width = 45
        Height = 178
        Anchors = [akTop, akRight]
        Max = 100
        Orientation = trVertical
        PageSize = 10
        Frequency = 10
        PositionToolTip = ptRight
        TabOrder = 9
        TickMarks = tmTopLeft
        OnChange = trckbrPlayerVolumeChange
      end
      object statPlayer: TStatusBar
        Left = 0
        Top = 473
        Width = 673
        Height = 19
        Panels = <
          item
            Width = 50
          end>
      end
    end
  end
  object dlgOpen1: TOpenDialog
    Left = 96
    Top = 256
  end
  object tmrError: TTimer
    OnTimer = tmrErrorTimer
    Left = 128
    Top = 256
  end
end

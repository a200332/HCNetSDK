unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls,
  StdCtrls, ComCtrls, HIK_Device, jpeg;

type
  TfrmHIKTest = class(TForm)
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    btnIPCCreate: TButton;
    pnlIPCPreview: TPanel;
    imgIPCPic: TImage;
    btnIPCFree: TButton;
    lbledtIPCIP: TLabeledEdit;
    lbledtIPCPort: TLabeledEdit;
    lbledtIPCUser: TLabeledEdit;
    lbledtIPCPassword: TLabeledEdit;
    btnIPCLogin: TButton;
    btnIPCLogout: TButton;
    btnSavePicToFile: TButton;
    btnSavePicToJPEG: TButton;
    btnIPCOpenPreview: TButton;
    btnIPCClosePreview: TButton;
    btnSaveRealPicToFile: TButton;
    ts3: TTabSheet;
    pnlPlayer: TPanel;
    btnPlayerOpenFile: TButton;
    btnPlayerCloseFile: TButton;
    btnPlayerPlay: TButton;
    btnPlayerPause: TButton;
    btnPlayerStop: TButton;
    btnPlayerCreate: TButton;
    btnPlayerFree: TButton;
    dlgOpen1: TOpenDialog;
    lbl1: TLabel;
    trckbrPlayerVolume: TTrackBar;
    lbl2: TLabel;
    lbl3: TLabel;
    statIPC: TStatusBar;
    lbledtDVRIP: TLabeledEdit;
    lbledtDVRPort: TLabeledEdit;
    lbledtDVRUser: TLabeledEdit;
    lbledtDVRPassword: TLabeledEdit;
    btnDVRFree: TButton;
    btnDVRLogin: TButton;
    btnDVRCreate: TButton;
    btnLogout: TButton;
    btnDVRStartRecord: TButton;
    btnDVRStopRecord: TButton;
    cbbDVRChannel: TComboBox;
    statDVR: TStatusBar;
    tmrError: TTimer;
    statPlayer: TStatusBar;
    shpDVR: TShape;
    btnDVRStarPlayBack: TButton;
    btnDVRStopPlayBack: TButton;
    btnDVRPasuePlayBack: TButton;
    lbledtDVRBegin: TLabeledEdit;
    lbledtDVREnd: TLabeledEdit;
    btnDVRStartDownload: TButton;
    btnDVRStopDownload: TButton;
    btnIPCGetTime: TButton;
    btnDVRGetTime: TButton;
    pnlNVRPlayBack: TPanel;
    procedure btnIPCCreateClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnIPCFreeClick(Sender: TObject);
    procedure btnIPCLoginClick(Sender: TObject);
    procedure btnIPCLogoutClick(Sender: TObject);
    procedure btnSavePicToFileClick(Sender: TObject);
    procedure btnIPCOpenPreviewClick(Sender: TObject);
    procedure btnIPCClosePreviewClick(Sender: TObject);
    procedure btnSaveRealPicToFileClick(Sender: TObject);
    procedure btnSavePicToJPEGClick(Sender: TObject);
    procedure btnPlayerCreateClick(Sender: TObject);
    procedure btnPlayerFreeClick(Sender: TObject);
    procedure btnPlayerOpenFileClick(Sender: TObject);
    procedure btnPlayerCloseFileClick(Sender: TObject);
    procedure btnPlayerPlayClick(Sender: TObject);
    procedure btnPlayerStopClick(Sender: TObject);
    procedure btnPlayerPauseClick(Sender: TObject);
    procedure trckbrPlayerVolumeChange(Sender: TObject);
    procedure btnDVRCreateClick(Sender: TObject);
    procedure btnDVRFreeClick(Sender: TObject);
    procedure btnDVRLoginClick(Sender: TObject);
    procedure btnLogoutClick(Sender: TObject);
    procedure btnDVRStartRecordClick(Sender: TObject);
    procedure btnDVRStopRecordClick(Sender: TObject);
    procedure tmrErrorTimer(Sender: TObject);
    procedure btnDVRStarPlayBackClick(Sender: TObject);
    procedure btnDVRPasuePlayBackClick(Sender: TObject);
    procedure btnDVRStopPlayBackClick(Sender: TObject);
    procedure btnDVRStartDownloadClick(Sender: TObject);
    procedure btnDVRStopDownloadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnIPCGetTimeClick(Sender: TObject);
    procedure btnDVRGetTimeClick(Sender: TObject);
  private
  public
  end;

var
  frmHIKTest: TfrmHIKTest;
  IPC: THIK_IPC_Device = nil;
  DVR: THIK_DVR_Device = nil;
  Player: THIK_Player = nil;

implementation

uses
  HCNetSDK;

{$R *.dfm}

/// <summary>异常消息的回调函数</summary>
/// <param name="dwType" IO="Out">异常或重连等消息的类型</param>
/// <param name="lUserID" IO="Out">登录ID</param>
/// <param name="lHandle" IO="Out">出现异常的相应类型的句柄</param>
/// <param name="pUser" IO="Out">用户数据</param>
procedure ExceptionCallBack_Demo(dwType: DWORD; lUserID, lHandle: LONG; pUser: PVOID); stdcall;
var
  tmpStr: string;
begin
  case dwType of
    EXCEPTION_EXCHANGE:
      tmpStr := '用户交互时异常';
    EXCEPTION_AUDIOEXCHANGE:
      tmpStr := '语音对讲异常';
    EXCEPTION_ALARM:
      tmpStr := '报警异常';
    EXCEPTION_PREVIEW:
      tmpStr := '网络预览异常';
    EXCEPTION_SERIAL:
      tmpStr := '透明通道异常';
    EXCEPTION_RECONNECT:
      tmpStr := '预览时重连';
    EXCEPTION_ALARMRECONNECT:
      tmpStr := '报警时重连';
    EXCEPTION_SERIALRECONNECT:
      tmpStr := '透明通道重连';
    SERIAL_RECONNECTSUCCESS:
      tmpStr := '透明通道重连成功';
    EXCEPTION_PLAYBACK:
      tmpStr := '回放异常';
    EXCEPTION_DISKFMT:
      tmpStr := '硬盘格式化';
    EXCEPTION_PASSIVEDECODE:
      tmpStr := '被动解码异常';
    EXCEPTION_EMAILTEST:
      tmpStr := '邮件测试异常';
    EXCEPTION_BACKUP:
      tmpStr := '备份异常';
    PREVIEW_RECONNECTSUCCESS:
      tmpStr := '预览时重连成功';
    ALARM_RECONNECTSUCCESS:
      tmpStr := '报警时重连成功';
    RESUME_EXCHANGE:
      tmpStr := '用户交互恢复';
    NETWORK_FLOWTEST_EXCEPTION:
      tmpStr := '网络流量检测异常';
    EXCEPTION_PICPREVIEWRECONNECT:
      tmpStr := '图片预览重连';
    PICPREVIEW_RECONNECTSUCCESS:
      tmpStr := '图片预览重连成功';
    EXCEPTION_PICPREVIEW:
      tmpStr := '图片预览异常';
    EXCEPTION_MAX_ALARM_INFO:
      tmpStr := '报警信息缓存已达上限';
    EXCEPTION_LOST_ALARM:
      tmpStr := '报警丢失';
    EXCEPTION_PASSIVETRANSRECONNECT:
      tmpStr := '被动转码重连';
    PASSIVETRANS_RECONNECTSUCCESS:
      tmpStr := '被动转码重连成功';
    EXCEPTION_PASSIVETRANS:
      tmpStr := '被动转码异常';
    EXCEPTION_RELOGIN:
      tmpStr := '用户重登录';
    RELOGIN_SUCCESS:
      tmpStr := '用户重登录成功';
    EXCEPTION_PASSIVEDECODE_RECONNNECT:
      tmpStr := '被动解码重连';
    EXCEPTION_RELOGIN_FAILED:
      tmpStr := '重登录失败，停止重登录';
    EXCEPTION_PREVIEW_RECONNECT_CLOSED:
      tmpStr := '关闭预览重连功能';
    EXCEPTION_ALARM_RECONNECT_CLOSED:
      tmpStr := '关闭报警重连功能';
    EXCEPTION_SERIAL_RECONNECT_CLOSED:
      tmpStr := '关闭透明通道重连功能';
    EXCEPTION_PIC_RECONNECT_CLOSED:
      tmpStr := '关闭回显重连功能';
    EXCEPTION_PASSIVE_DECODE_RECONNECT_CLOSED:
      tmpStr := '关闭被动解码重连功能';
    EXCEPTION_PASSIVE_TRANS_RECONNECT_CLOSED:
      tmpStr := '关闭被动转码重连功能';
  end;
  frmHIKTest.statIPC.Panels[0].Text := tmpStr;
end;

procedure TfrmHIKTest.btnDVRCreateClick(Sender: TObject);
begin
  if not Assigned(DVR) then
    DVR := THIK_DVR_Device.Create;
end;

procedure TfrmHIKTest.btnDVRFreeClick(Sender: TObject);
begin
  if Assigned(DVR) then
    FreeAndNil(DVR);
end;

procedure TfrmHIKTest.btnDVRGetTimeClick(Sender: TObject);
begin
  if Assigned(DVR) then
    ShowMessage(FormatDateTime('YYYY-MM-DD HH:NN:SS', DVR.GetDeviceTime));
end;

procedure TfrmHIKTest.btnDVRLoginClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.Login(lbledtDVRIP.Text, StrToInt(lbledtDVRPort.Text), lbledtDVRUser.Text, lbledtDVRPassword.Text);
end;

procedure TfrmHIKTest.btnDVRPasuePlayBackClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.PlayPause(cbbDVRChannel.ItemIndex + 1);
end;

procedure TfrmHIKTest.btnDVRStarPlayBackClick(Sender: TObject);
var
  B, E: TDateTime;
begin
  B := StrToDateTime(lbledtDVRBegin.Text);
  E := StrToDateTime(lbledtDVREnd.Text);
  if Assigned(DVR) then
    DVR.PlayStart(cbbDVRChannel.ItemIndex + 1, B, E, pnlNVRPlayBack.Handle);
end;

procedure TfrmHIKTest.btnDVRStartDownloadClick(Sender: TObject);
var
  B, E: TDateTime;
begin
  B := StrToDateTime(lbledtDVRBegin.Text);
  E := StrToDateTime(lbledtDVREnd.Text);
  if Assigned(DVR) then
    DVR.DownloadStart(cbbDVRChannel.ItemIndex + 1, B, E, ExtractFilePath(Application.ExeName) + FormatDateTime('YYYYMMDDHHNNSS', Now) + '.mp4');
end;

procedure TfrmHIKTest.btnDVRStartRecordClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.StartRecord(cbbDVRChannel.ItemIndex + 1);
end;

procedure TfrmHIKTest.btnDVRStopDownloadClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.DownloadStop(cbbDVRChannel.ItemIndex + 1);
end;

procedure TfrmHIKTest.btnDVRStopPlayBackClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.PlayStop(cbbDVRChannel.ItemIndex + 1);
end;

procedure TfrmHIKTest.btnDVRStopRecordClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.StopRecord(cbbDVRChannel.ItemIndex + 1);
end;

procedure TfrmHIKTest.btnIPCClosePreviewClick(Sender: TObject);
begin
  if Assigned(IPC) then
    IPC.ClosePreview;
end;

procedure TfrmHIKTest.btnIPCCreateClick(Sender: TObject);
begin
  if not Assigned(IPC) then
  begin
    IPC := THIK_IPC_Device.Create;
    IPC.OnExept := ExceptionCallBack_Demo;
  end;
end;

procedure TfrmHIKTest.btnIPCFreeClick(Sender: TObject);
begin
  if Assigned(IPC) then
    FreeAndNil(IPC);
end;

procedure TfrmHIKTest.btnIPCGetTimeClick(Sender: TObject);
begin
  if Assigned(IPC) then
    ShowMessage(FormatDateTime('YYYY-MM-DD HH:NN:SS', IPC.GetDeviceTime));
end;

procedure TfrmHIKTest.btnIPCLoginClick(Sender: TObject);
begin
  if Assigned(IPC) then
    IPC.Login(lbledtIPCIP.Text, StrToInt(lbledtIPCPort.Text), lbledtIPCUser.Text, lbledtIPCPassword.Text);
end;

procedure TfrmHIKTest.btnIPCLogoutClick(Sender: TObject);
begin
  if Assigned(IPC) then
    IPC.Logout;
end;

procedure TfrmHIKTest.btnIPCOpenPreviewClick(Sender: TObject);
begin
  if Assigned(IPC) then
    IPC.OpenPreview(pnlIPCPreview.Handle, True);
end;

procedure TfrmHIKTest.btnLogoutClick(Sender: TObject);
begin
  if Assigned(DVR) then
    DVR.Logout;
end;

procedure TfrmHIKTest.btnPlayerCloseFileClick(Sender: TObject);
begin
  if Assigned(Player) then
    Player.CloseVideo;
end;

procedure TfrmHIKTest.btnPlayerCreateClick(Sender: TObject);
begin
  if not Assigned(Player) then
    Player := THIK_Player.Create;
end;

procedure TfrmHIKTest.btnPlayerFreeClick(Sender: TObject);
begin
  if Assigned(Player) then
    FreeAndNil(Player);
end;

procedure TfrmHIKTest.btnPlayerOpenFileClick(Sender: TObject);
var
  FileName: string;
begin
  if Assigned(Player) then
  begin
    FileName := '';
    //海康播放库支持的视频格式目前已知的只有mp4
    dlgOpen1.Filter := '录像文件(*.mp4)|*.mp4';
    if dlgOpen1.Execute then
      FileName := dlgOpen1.FileName;
    if FileName <> '' then
      Player.OpenVideo(FileName);
  end;
end;

procedure TfrmHIKTest.btnPlayerPauseClick(Sender: TObject);
begin
  if Assigned(Player) then
    Player.Pause;
end;

procedure TfrmHIKTest.btnPlayerPlayClick(Sender: TObject);
begin
  if Assigned(Player) then
  begin
    Player.Play(pnlPlayer.Handle);
    trckbrPlayerVolume.Position := Player.Volume;
  end;
end;

procedure TfrmHIKTest.btnPlayerStopClick(Sender: TObject);
begin
  if Assigned(Player) then
    Player.Stop;
end;

procedure TfrmHIKTest.btnSavePicToFileClick(Sender: TObject);
var
  PicName: string;
begin
  if Assigned(IPC) then
  begin
    PicName := ExtractFilePath(Application.ExeName) + FormatDateTime('YYYYMMDDHHNNSS', Now) + '.jpg';
    if IPC.SavePic(PicName) then
      imgIPCPic.Picture.LoadFromFile(PicName);
  end;
end;

procedure TfrmHIKTest.btnSavePicToJPEGClick(Sender: TObject);
var
  jpg: TJPEGImage;
begin
  if Assigned(IPC) then
  begin
    jpg := TJPEGImage.Create;
    try
      if IPC.SavePic(jpg) then
      begin
//        imgIPCPic.Picture := nil;
        imgIPCPic.Picture.Assign(jpg);
//        imgIPCPic.Picture.Graphic := jpg;
      end;
    finally
      jpg.Free;
    end;
  end;
end;

procedure TfrmHIKTest.btnSaveRealPicToFileClick(Sender: TObject);
var
  PicName: string;
begin
  if Assigned(IPC) then
  begin
    PicName := ExtractFilePath(Application.ExeName) + FormatDateTime('YYYYMMDDHHNNSS', Now) + '.jpg';
    if IPC.SaveRealPic(PicName) then
      imgIPCPic.Picture.LoadFromFile(PicName);
  end;
end;

procedure TfrmHIKTest.FormDestroy(Sender: TObject);
begin
  btnIPCFree.Click;
  btnDVRFree.Click;
  btnPlayerFree.Click;
end;

procedure TfrmHIKTest.FormShow(Sender: TObject);
begin
  lbledtDVRBegin.Text := FormatDateTime('YYYY-MM-DD 00:00:00', Date);
  lbledtDVREnd.Text := FormatDateTime('YYYY-MM-DD 23:59:59', Date);
end;

procedure TfrmHIKTest.tmrErrorTimer(Sender: TObject);
begin
  if Assigned(IPC) then
    statIPC.Panels[0].Text := IPC.PubFunc.GetLastErrInfo;
  if Assigned(DVR) then
  begin
    statDVR.Panels[0].Text := DVR.PubFunc.GetLastErrInfo;
    if DVR.IsRecording[cbbDVRChannel.ItemIndex + 1] then
      shpDVR.Brush.Color := clRed
    else
      shpDVR.Brush.Color := clWhite;
  end;
  if Assigned(Player) then
    statPlayer.Panels[0].Text := Player.GetLastErrorInfo;
end;

procedure TfrmHIKTest.trckbrPlayerVolumeChange(Sender: TObject);
begin
  if Assigned(Player) then
    Player.Volume := trckbrPlayerVolume.Position;
end;

end.

